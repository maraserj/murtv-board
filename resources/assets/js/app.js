
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.config.productionTip = false;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('login-modal', require('./components/auth/LoginModal.vue'));
Vue.component('login-form', require('./components/auth/LoginForm.vue'));

Vue.component('admin-category-and-price', require('./components/selects/AdminCategoryAndPrice'));
Vue.component('admin-country-region-city', require('./components/selects/location/AdminCountryRegionCity'));
Vue.component('admin-breeds-and-breeds-type', require('./components/selects/AdminBreedsAndBreedsType'));

Vue.component('country-region-city', require('./components/selects/location/CountryRegionCity'));
Vue.component('breeds-and-breeds-type', require('./components/selects/BreedsAndBreedsType'));
Vue.component('category-and-price', require('./components/selects/CategoryAndPrice'));

Vue.component('favourites-button', require('./components/favourites/FavouritesButton'));
Vue.component('single-item-favourites-button', require('./components/favourites/SingleItemFavouritesButton'));
Vue.component('index-country-region-city', require('./components/index-posts-filter/IndexCountryRegionCity'));
Vue.component('index-breeds-and-breeds-type', require('./components/index-posts-filter/IndexBreedsAndBreedsType'));

const app = new Vue({
    el: '#app'
});
