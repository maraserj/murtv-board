<?php

return [
    'success_confirmation' => 'Success verified! Thanks',
    'error_confirmation'   => 'Maybe your token expired or this is wrong',
    'created'              => 'Success created!',
    'updated'              => 'Success updated!',
    'deleted'              => 'Success deleted!',
    'error'                => 'Something went wrong. Try again',
    'not_found'            => 'Not found',
];