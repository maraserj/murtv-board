<section class="search margin-bottom-50">
    <div class="container">
        <div class="row">
            <form action="{{ route('posts.index') }}" onsubmit="submitFilterForm(event, $(this));">
                <div class="col-md-12">
                <!-- Title -->
                <h3 class="search-title">Поиск объявление</h3>

                <!-- Form -->
                <div class="main-search-box no-shadow">
                    <div class="main-search-input">
                        <div class="col-sm-4">
                            <div class="form-group checkboxes">
                                <input id="check-13" type="checkbox" name="is_for_sale" value="1" {{ request('is_for_sale') == 1 ? 'checked' : '' }}>
                                <label for="check-13">Для покупки/продажи?</label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <select name="post_type_id" id="pets_type">
                                <option value="0">Выберите тип объявления</option>
                                @foreach($post_types as $post_type)
                                    <option {{ request('post_type_id') == $post_type->id ? 'selected' : '' }} value="{{ $post_type->id }}">{{ $post_type->title }}</option>
                                @endforeach
                            </select>
                            <select name="category_id" id="category_id">
                                <option value="0">Выберите категорию</option>
                                @foreach($categories as $category)
                                    <option {{ request('category_id') == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="button">Искать</button>
                        <a href="{{ route('posts.index') }}" class="button" style="min-width: inherit; background-color: #fe1821" title="Сбросить фильтр">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <div class="row with-forms">
                        <?php
                        $breed_data = [];
                        $breed_data['breed_type_id'] = request('breed_type_id', 0);
                        $breed_data['breed_id'] = request('breed_id', 0);
                        ?>
                        <index-breeds-and-breeds-type :post="{{ json_encode($breed_data) }}" :breed_types="{{ $breed_types }}"></index-breeds-and-breeds-type>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{--<label for="gender_s">Пол</label>--}}
                                <select class="" id="gender_s" name="gender" data-placeholder="Выберите пол">
                                    <option value="0">Выберите пол</option>
                                    <option {{ request('gender') == 'male' ? 'selected' : '' }} value="male">Мальчик</option>
                                    <option {{ request('gender') == 'female' ? 'selected' : '' }} value="female">Девочка</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- More Search Options -->
                    <a href="#" class="more-search-options-trigger margin-top-10" data-open-title="Уточнить детали" data-close-title="Уточнить детали"></a>

                    <div class="more-search-options relative">
                        <div class="more-search-options-container">

                            <!-- Row With Forms -->
                            <div class="row with-forms">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        {{--<label for="pets_color_id">Расцветка</label>--}}
                                        <select id="pets_color_id" name="color_id" class="" >
                                            <option value="0">Выберите расцветку</option>
                                            @foreach($colors as $color)
                                                <option {{ request('color_id') == $color->id ? 'selected' : '' }} value="{{ $color->id }}">{{ $color->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {{--<label for="pets_age">Возраст</label>--}}
                                        <select id="pets_age" name="age" class="" >
                                            <option value="0">Выберите возраст</option>
                                            <option {{ request('age') == '0-0.5' ? 'selected' : '' }} value="0-0.5">До 6 месяцев </option>
                                            <option {{ request('age') == '0.6-1' ? 'selected' : '' }} value="0.6-1">От 6 месяцев до 1 года </option>
                                            <option {{ request('age') == '1.1-2' ? 'selected' : '' }} value="1.1-2">1-2 года </option>
                                            <option {{ request('age') == '2.1-3' ? 'selected' : '' }} value="2.1-3">2-3 года </option>
                                            <option {{ request('age') == '3.1-4' ? 'selected' : '' }} value="3.1-4">3-4 года </option>
                                            <option {{ request('age') == '4.1-5' ? 'selected' : '' }} value="4.1-5">4-5 лет </option>
                                            <option {{ request('age') == '5' ? 'selected' : '' }} value="5+">Более 5 лет</option>
                                        </select>
                                    </div>
                                </div>
                                @if (!empty($organizations->count()))
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {{--<label for="pets_age">Возраст</label>--}}
                                            <select id="organizations" name="organization_id" class="" >
                                                <option value="0">Выберите организацию</option>
                                                <option value="9999" {{ request('organization_id') == '9999' ? 'selected' : '' }}>Частное лицо</option>
                                                @foreach($organizations as $organization)
                                                    <option {{ request('organization_id') == $organization->id ? 'selected' : '' }} value="{{ $organization->id }}">{{ $organization->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!-- Row With Forms / End -->


                            <!-- Checkboxes -->
                            <div class="checkboxes in-row" style="margin-bottom: 24px;">

                                <input id="check-2" type="checkbox" name="sterilized" value="1" {{ request('sterilized') == 1 ? 'checked' : '' }}>
                                <label for="check-2">Стерилизован</label>

                                <input id="check-3" type="checkbox" name="guarded" value="1" {{ request('guarded') == 1 ? 'checked' : '' }}>
                                <label for="check-3">Привит</label>

                                <input id="check-4" type="checkbox" name="ill" value="1" {{ request('ill') == 1 ? 'checked' : '' }}>
                                <label for="check-4">Требует лечения</label>

                                <input id="check-5" type="checkbox" name="deadly" value="1" {{ request('deadly') == 1 ? 'checked' : '' }}>
                                <label for="check-5">Тяжелое состояние</label>

                                <input id="check-6" type="checkbox" name="featured" value="1" {{ request('featured') == 1 ? 'checked' : '' }}>
                                <label for="check-6">Особенный</label>

                            </div>
                            <!-- Checkboxes / End -->

                            <div class="row with-forms">
                                <?php
                                $location_data = [];
                                $location_data['country_id'] = request('country_id', 0);
                                $location_data['region_id'] = request('region_id', 0);
                                $location_data['city_id'] = request('city_id', 0);
                                ?>
                                <index-country-region-city :post="{{ json_encode($location_data) }}" :countries="{{ $countries }}"></index-country-region-city>
                            </div>
                        </div>

                    </div>
                    <!-- More Search Options / End -->


                </div>
                <!-- Box / End -->
            </div>
            </form>
        </div>
    </div>
</section>

@push('js')
    <script>

        $(function () {
          toggleTypeSelect();
          $('#check-13').change(toggleTypeSelect);
        });

      function toggleTypeSelect() {
        if ($('#check-13').prop('checked')) {
          $('#pets_type').hide();
          $('#pets_type').val(0);
          $('#category_id').show();
        } else {
          $('#pets_type').show();
          $('#category_id').val(0);
          $('#category_id').hide();
        }
      }
    </script>
@endpush