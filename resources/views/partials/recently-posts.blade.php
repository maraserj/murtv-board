@if (!empty($posts) && $posts->count())
    <div class="container margin-bottom-55">
        <div class="row">

            <div class="col-md-12">
                <h3 class="headline margin-bottom-25 margin-top-65">Недавно добавленные</h3>
            </div>

            <!-- Carousel -->
            <div class="col-md-12">
                <div class="carousel">
                    <!-- Listing Item -->
                    @foreach($posts as $post)
                        <div class="carousel-item">
                            <div class="listing-item">

                                <a href="{{ route('posts.show', $post->slug) }}" class="listing-img-container">

                                    <div class="listing-badges">
                                        <span>{{ $post->postType ? $post->postType->title : 'Не известно' }}</span>
                                    </div>

                                    <div class="listing-img-content">
                                        <div class="date-time">{{ $post->created_at->format('d.m.Y') }}</div>
                                    </div>
                                    @if ($post->photos && $post->photos()->main()->count())
                                        <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;background:url('{{ asset($post->photos()->main()->first()->path) }}') center center; background-size:cover;"></div>
                                    @elseif ($post->photos && $post->photos()->count())
                                        <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;background:url('{{ asset($post->photos->first()->path) }}') center center; background-size:cover;"></div>
                                    @else
                                        <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;background:url('{{ asset('images/no-image.png') }}') center center; background-size:cover;"></div>
                                    @endif
                                </a>

                                <div class="listing-content">

                                    <div class="listing-title">
                                        <h4><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title or 'Неизвестно' }}</a></h4>
                                    </div>

                                    <ul class="listing-features">
                                        @if (!empty($post->breedType))
                                            <li>
                                                <span>
                                                @if(!empty($post->breedType->image))
                                                    <img src="{{ asset($post->breedType->image) }}" alt="{{ !empty($post->breedType) ? $post->breedType->title : '' }}" />
                                                @else
                                                    <img src="{{ asset($post->breedType->slug == 'koshka' ? 'images/cat.png' : 'images/dog.png') }}" alt="{{ !empty($post->breedType) ? $post->breedType->title : '' }}" />
                                                @endif
                                                </span>
                                            </li>
                                        @endif
                                        <li><span>
                                            @empty($post->gender)
                                                    Неизвестно
                                                @else
                                                    <img src="{{ $post->gender == 'male' ? 'images/male.png' : 'images/female.png' }}" alt="{{ $post->gender == 'male' ? 'Мальчик' : 'Девочка' }}" />
                                                @endempty
                                        </span></li>
                                        <li><span>{{ empty($post->breed) ? '' : $post->breed->title }}</span></li>
                                    </ul>

                                    <div class="listing-footer">
                                        <a href="https://maps.google.com/maps?q={{ (empty($post->country) ? '' : $post->country->name) . ', ' . (empty($post->region) ? '' : $post->region->name) . ', ' . (empty($post->city) ? '' : $post->city->name) }}" class="listing-address popup-gmaps">
                                            <i class="fa fa-map-marker"></i>
                                            {{ (empty($post->country) ? '' : $post->country->name) . ', ' . (empty($post->city) ? '' : $post->city->name) }}
                                        </a>
                                    </div>

                                </div>

                            </div>
                        </div>
                @endforeach
                <!-- Listing Item / End -->
                </div>
            </div>
            <!-- Carousel / End -->

        </div>
    </div>
@endif