<div class="parallax" data-background="{{ asset($main_settings['index_background_image'] ?? 'images/home-parallax.jpg') }}" data-color="#36383e" data-color-opacity="0.45" data-img-width="1920" data-img-height="1080">
    <div class="parallax-content">

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <!-- Main Search Container -->
                    <div class="main-search-container">
                        <h1>{!!  $main_settings['index_page_h1'] ?? 'Объявления о животных' !!}</h1>

                        <!-- Main Search -->
                        <form class="main-search-form" action="{{ route('posts.index') }}" onsubmit="submitFilterForm(event, $(this));">

                            <!-- Type -->
                            <div class="search-type">
                                @foreach($post_types as $post_type)
                                    <label class="{{ $loop->index == 0 ? 'active' : '' }}"><input class="{{ $loop->index == 0 ? 'first-tab' : '' }}" name="post_type_id" value="{{ $post_type->id }}" checked="checked" type="radio"> {{ $post_type->title }}</label>
                                @endforeach
                                <div class="search-type-arrow"></div>
                            </div>


                            <!-- Box -->
                            <div class="main-search-box">
                                <!-- Row -->
                                <div class="row with-forms">
                                <?php
                                $breed_data = [];
                                $breed_data['breed_type_id'] = request('breed_type_id', 0);
                                $breed_data['breed_id'] = request('breed_id', 0);
                                ?>

                                <!-- Row -->
                                        <index-breeds-and-breeds-type :post="{{ json_encode($breed_data) }}" :breed_types="{{ $breed_types }}"></index-breeds-and-breeds-type>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {{--<label for="gender_s">Пол</label>--}}
                                                <select class="" id="gender_s" name="gender" data-placeholder="Выберите пол">
                                                    <option value="0">Выберите пол</option>
                                                    <option value="male">Мальчик</option>
                                                    <option value="female">Девочка</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Row / End -->

                                    <!-- Checkboxes -->
                                    <div class="checkboxes in-row">

                                        <input id="check-2" type="checkbox" name="sterilized" value="1">
                                        <label for="check-2">Стерилизован</label>

                                        <input id="check-3" type="checkbox" name="guarded" value="1">
                                        <label for="check-3">Привит</label>

                                        <input id="check-4" type="checkbox" name="ill" value="1">
                                        <label for="check-4">Требует лечения</label>

                                        <input id="check-5" type="checkbox" name="deadly" value="1">
                                        <label for="check-5">Тяжелое состояние</label>

                                        <input id="check-6" type="checkbox" name="featured" value="1">
                                        <label for="check-6">Особенный</label>
                                    </div>
                                    <!-- Checkboxes / End -->

                                <!-- Row / End -->
                                <button type="submit" class="button pull-right">Искать</button>


                                <!-- More Искать Options -->
                                <a href="#" class="more-search-options-trigger" data-open-title="выбрать страну/город" data-close-title="выбрать страну/город"></a>

                                <div class="more-search-options">
                                    <div class="more-search-options-container">
                                        <div class="row with-forms">

                                        <?php
                                        $location_data = [];
                                        $location_data['country_id'] = request('country_id', 0);
                                        $location_data['region_id'] = request('region_id', 0);
                                        $location_data['city_id'] = request('city_id', 0);
                                        ?>
                                        <index-country-region-city :post="{{ json_encode($location_data) }}" :countries="{{ $countries }}"></index-country-region-city>
                                        </div>
                                    </div>
                                </div>
                                <!-- More Search Options / End -->


                            </div>
                            <!-- Box / End -->

                        </form>
                        <!-- Main Search -->

                    </div>
                    <!-- Main Search Container / End -->

                </div>
            </div>
        </div>

    </div>
</div>