<!-- Header Container
        ================================================== -->
<header id="header-container">
    <!-- Header -->
    <div id="header">
        <div class="container">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                    <a href="{{ route('index') }}">
                        <img src="{{ $main_settings['site_logo'] ?? asset('images/logo.png') }}" alt="{{ $seo_settings['index_page_title'] ?? '' }}">
                    </a>
                </div>


                <!-- Mobile Navigation -->
                <div class="mmenu-trigger">
                    <button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
                    </button>
                </div>


                <!-- Main Navigation -->
                <nav id="navigation" class="style-1">
                    <ul id="responsive">
                        <li><a class="{{ Route::is('posts.*') && !in_array(request('post_type_id'), [1,2,3,4]) ? 'current' : '' }}" href="{{ route('posts.index') }}">Взять</a></li>
                        <li><a class="{{ Route::is('posts.*') && request('post_type_id') == 2 ? 'current' : '' }}" href="{{ route('posts.index', ['post_type_id' => 2]) }}">Найденные</a></li>
{{--                        <li><a class="{{ Route::is('posts.*') && request('post_type_id') == 3 ? 'current' : '' }}" href="{{ route('posts.index', ['post_type_id' => 3]) }}">Пропавшие</a></li>--}}
                        <li class="create-btn"><a class="{{ Route::is('profile.posts.create') ? 'current' : '' }}" href="{{ route('profile.posts.create') }}">Добавить</a></li>

                        {{--<li><a class="{{ Route::is('index') ? 'current' : '' }}" href="{{ route('index') }}">Главная</a></li>--}}
                        {{--<li><a class="{{ Route::is('posts.*') ? 'current' : '' }}" href="{{ route('posts.index') }}">Питомцы</a></li>--}}

                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->

            <!-- Right Side Content / End -->
            <div class="right-side">
                <!-- Header Widget -->
                <div class="header-widget">
                    @guest
                        <a href="{{ route('login') }}" class="sign-in"><i class="fa fa-user"></i> Вход / Регистрация</a>
                    @else
                        <div class="user-menu">
                            <div class="user-name"><span><img src="{{ auth()->user()->avatar }}" alt=""></span>{{ auth()->user()->name }}</div>
                            <ul>
                                @can('admin')
                                    <li><a href="{{ route('admin.index') }}"><i class="fa fa-lock"></i> Админка</a></li>
                                @endcan
                                <li><a href="{{ route('profile.index') }}"><i class="sl sl-icon-user"></i> Мой профиль</a></li>
                                <li><a href="{{ route('profile.favourites.index') }}"><i class="sl sl-icon-star"></i> Избранное</a></li>
                                <li><a href="{{ route('profile.posts.index') }}"><i class="sl sl-icon-docs"></i> Мои объявления</a></li>
                                <li><a href="{{ route('profile.organizations.index') }}"><i class="im im-icon-Building"></i> Организации</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            <i class="fa sl sl-icon-power"></i> Выйти
                                        </a>
                                    </li>
                            </ul>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endguest
                    <!--<a href="{{ route('profile.posts.create') }}" class="button border">Приютить</a>-->
                </div>
                <!-- Header Widget / End -->
            </div>
            <!-- Right Side Content / End -->

        </div>
    </div>
    <!-- Header / End -->
</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
