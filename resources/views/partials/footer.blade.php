
<!-- Footer
        ================================================== -->
<div id="footer" class="{{--{{ Route::is('index') ? 'sticky-footer' : '' }}--}}">
    <!-- Main -->
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="widget-title text-center">Рубрики</h2>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <ul class="footer-links">
                            <li>
                                <a target="_blank" href="https://mur.tv/category/pets/" itemprop="url">Альбом ваших питомцев</a>
                            </li>
                            <li>
                                <a target="_blank" href="https://mur.tv/category/neobychnye-istorii/" itemprop="url">Необычные истории</a>
                            </li>
                            <li>
                                <a target="_blank" href="https://mur.tv/category/wow/" itemprop="url">Ух ты!</a>
                            </li>
                            <li>
                                <a target="_blank" href="https://mur.tv/category/spasenie-zhivotnyx/" itemprop="url">Спасение животных</a>
                            </li>
                            <li>
                                <a target="_blank" href="https://mur.tv/category/istorii-chitateley/" itemprop="url">Истории читателей</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="footer-links">
                        <li>
                            <a target="_blank" href="https://mur.tv/category/video/" itemprop="url">Интересное видео</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://mur.tv/sovety-eksperta/" itemprop="url">Консультация ветеринара</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://mur.tv/category/krasivye-kartinki/" itemprop="url">Веселые картинки</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://mur.tv/category/instagram-stars/" itemprop="url">Звезды Инстаграм</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://mur.tv/category/poleznosti/" itemprop="url">Лайфхаки и полезности</a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="widget-title text-center">Информация</h2>
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <ul class="footer-links">
                                    <li>
                                        <a target="_blank" href="https://mur.tv/dmca/" itemprop="url">Авторское право (DMCA)</a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://mur.tv/ads-on-mur-tv/" itemprop="url">Реклама на Mur.tv</a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://mur.tv/vakansii/" itemprop="url">Вакансии mur.tv</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="footer-links">
                                    <li>
                                        <a target="_blank" href="https://mur.tv/cookies/" itemprop="url">Использование cookies</a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://mur.tv/about-us/" itemprop="url">О проекте</a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://mur.tv/contact-us/" itemprop="url">Связаться с нами</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h2 class="widget-title text-center">ЧИТАЙ НАС ПЕРВЫМ!</h2>
            <div class="social circle text-center">
                <div class="text-center" style="float: none; display: inline-block;">
                    <a class="footer_social_btn f-soc_btn_facebook btn btn-rounded btn-default" href="https://www.facebook.com/mur.tv.mag/" title="Фейсбук" rel="nofollow" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                </div>
                <div class="text-center" style="float: none; display: inline-block;">
                    <a class="footer_social_btn f-soc_btn_twitter btn btn-rounded btn-default" href="https://twitter.com/mur_tv_love" title="Твиттер" rel="nofollow" target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>
                </div>
                <div class="text-center" style="float: none; display: inline-block;">
                    <a class="footer_social_btn f-soc_btn_vk btn btn-rounded btn-default" href="https://vk.com/murtv" title="Вконтактке" rel="nofollow" target="_blank">
                        <i class="fa fa-vk"></i>
                    </a>
                </div>
                <div class="text-center" style="float: none; display: inline-block;">
                    <a class="footer_social_btn f-soc_btn_odnoklassniki btn btn-rounded btn-default" href="https://ok.ru/group/54918347882496" title="Одноклассники" rel="nofollow" target="_blank">
                        <i class="fa fa-odnoklassniki"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- Copyright -->
        <div class="row">
            <div class="col-md-12">
                <div class="copyrights">© 2015 - {{ date('Y') }} Все права защищены</div>
            </div>
        </div>

    </div>

</div>
<!-- Footer / End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>