@extends('layouts.main')

@section('title', $page->seo_title ?? 'MUR TV')
@section('keywords', $page->seo_keywords ?? 'MUR TV')
@section('description', $page->seo_description ?? 'MUR TV')

@push('meta')
    <meta property="og:title" content="{{ $page->seo_title ?? 'MUR TV' }}" />
    <meta property="og:description" content="{{ $page->seo_description ?? 'MUR TV' }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="{{ env('FB_CLIENT_ID') }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image:width" content="640">
    <meta property="og:image:height" content="480">
    @if (!empty($main_settings['opengraph_main_image']))
        <meta property="og:image" content="{{ asset($main_settings['opengraph_main_image']) }}" />
    @endif
@endpush
@section('content')
    <section class="fullwidth margin-top-95 margin-bottom-90">
        <h3 class="headline-box">{!! $page->seo_h1 !!}</h3>

        <div class="container">
            <div class="row">
                {!! $page->description !!}
            </div>
        </div>
    </section>
@endsection
