@foreach($organizations as $organization)
    <div class="listing-item">

        <a href="{{ route('organizations.show', $organization->slug) }}" class="listing-img-container">

            <div class="listing-badges">
                {{--<span class="featured">Featured</span>--}}
                <span>{{ $organization->organizationType ? $organization->organizationType->title : 'Не известно' }}</span>
            </div>

            <div class="listing-img-content">
                <div class="date-time">{{--<i class="fa fa-calendar-o"></i> --}}{{ $organization->created_at->format('d.m.Y') }}</div>
            </div>

            @if ($organization->photos && $organization->photos->count())
                <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;
                background:url('{{ asset($organization->photos->first()->path) }}') center center; background-size:cover;"></div>
            @else
                <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;
                background:url('{{ asset('images/no-image.png') }}') center center; background-size:cover;"></div>
            @endif

        </a>

        <div class="listing-content">

            <div class="listing-title">
                <h4><a href="{{ route('organizations.show', $organization->slug) }}">{{ $organization->title or 'Неизвестно' }}</a></h4>
                <a href="{{ route('organizations.show', $organization->slug) }}" class="details button border">Подробнее</a>
            </div>

            <div class="listing-footer">
                <a href="https://maps.google.com/maps?q={{ $organization->country->name . ', ' . $organization->region->name . ', ' . $organization->city->name }}" class="listing-address popup-gmaps">
                    <i class="fa fa-map-marker"></i>
                    {{ $organization->country->name . ', ' . $organization->city->name }}
                </a>
                {{--<a><i class="fa fa-user"></i> {{ $organization->user->name }}</a>
                <span><i class="fa fa-calendar-o"></i> {{ $organization->created_at->format('d.m.Y') }}</span>--}}
            </div>

        </div>

    </div>
@endforeach