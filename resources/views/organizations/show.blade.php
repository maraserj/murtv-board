@extends('layouts.main')

@section('title', $organization->title)
@section('keywords', 'MUR TV')
@section('description', str_limit(strip_tags($organization->description), 250))

@push('meta')
    <meta property="og:title" content="{{ $organization->title ?? 'MUR TV' }}" />
    <meta property="og:description" content="{{ str_limit(strip_tags($organization->description), 250) ?? 'MUR TV' }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="{{ env('FB_CLIENT_ID') }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    @if ($organization->photos()->count())
        <meta property="og:image" content="{{ asset($organization->photos()->first()->path) }}">
    @elseif (!empty($main_settings['opengraph_main_image']))
        <meta property="og:image" content="{{ asset($main_settings['opengraph_main_image']) }}" />
    @endif
@endpush

@section('content')

    <div id="titlebar" class="property-titlebar margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <a href="{{ URL::previous() }}" class="back-to-listings"></a>
                    <div class="property-title">
                        <h2>{{ $organization->title }} <span
                                    class="property-badge">{{ $organization->organizationType ? $organization->organizationType->title : 'Не известно' }}</span>
                        </h2>
                        <span>
                            <a href="#location" class="listing-address">
                                <i class="fa fa-map-marker"></i>
                                {{ $organization->country->name . ', ' . $organization->region->name . ', ' . $organization->city->name }}
                            </a>
                        </span>
                    </div>
                </div>
                {{--<div class="property-pricing" style="margin-right: 85px;">--}}
                    {{--<div class="property-price">--}}
                        {{--<single-item-favourites-button :post_id="{{ $organization->id }}"--}}
                                                       {{--:is_in_favourites="{{ in_array($organization->id, $favourites_array) ? 'true' : 'false' }}"--}}
                                                       {{--:logged_user="{{ auth()->check() ? 'true' : 'false' }}"></single-item-favourites-button>--}}

                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>


    <!-- Content
    ================================================== -->
    @if ($organization->photos()->count())
        @php($images = $organization->photos)
        <div class="container">
            <div class="row margin-bottom-50">
                <div class="col-md-12">

                    <!-- Slider -->
                    <div class="property-slider default">
                        @foreach($images as $image)
                            <a href="{{ asset($image->path) }}" data-background-image="{{ asset($image->path) }}"
                               class="item mfp-gallery"></a>
                        @endforeach
                    </div>

                    <!-- Slider Thumbs -->
                    <div class="property-slider-nav">
                        @foreach($images as $image)
                            <div class="item"><img src="{{ asset($image->path) }}" alt="{{ $organization->title }}">
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    @endif


    <div class="container">
        <div class="row">

            <!-- Property Description -->
            <div class="col-lg-8 col-md-7">
                <div class="property-description">

                    <!-- Description -->
                    <h3 class="desc-headline">Описание</h3>
                    <div {{--class="show-more"--}}>
                        {!! strip_tags($organization->description, '<span><p><br>') !!}
                        {{--<a href="#" class="show-more-button">Show More <i class="fa fa-angle-down"></i></a>--}}
                    </div>

                    <!-- Details -->
                    <h3 class="desc-headline">Детали</h3>
                    <ul class="property-features margin-top-0">
                        <li>Страна: <span>{{ $organization->country->name }}</span></li>
                        <li>Регион: <span>{{ $organization->region->name }}</span></li>
                        <li>Город: <span>{{ $organization->city->name }}</span></li>
                        <li>Адрес: <span>{{ $organization->address or 'Не указан' }}</span></li>
                    </ul>

                </div>
                @if ($organization_posts_count)
                    <hr>
                    <div>
                        <h4>Питомцы данной организации <b>({{ $organization_posts_count }}):</b></h4>
                        <div class="listings-container grid-layout-three">
                            @include('posts.partials.list-item', ['posts' => $organization_posts, 'favourites_array' => $favourites_array])
                        </div>
                    </div>
                @endif
            </div>
            <!-- Property Description / End -->


            <!-- Sidebar -->
            <div class="col-lg-4 col-md-5">
                <div class="sidebar sticky right">

                    <!-- Widget -->
                    <div class="widget margin-bottom-30">
                        <div class="clearfix"></div>
                    </div>
                    <!-- Widget / End -->


                    <!-- Widget -->
                    <div class="widget">

                        <!-- Agent Widget -->
                        <div class="agent-widget">
                            <div class="agent-title">
                                <div class="agent-photo" style="display: flex;height: 72px;">
                                    <img src="{{ $organization->user->avatar }}" alt="{{ $organization->user->name }}"/>
                                </div>
                                <div class="agent-details">
                                    <h4><a href="#">{{ $organization->user->name }}</a></h4>
                                    @if ($organization->user->phone)
                                        <span><i class="sl sl-icon-call-in"></i>{{ $organization->user->phone }}</span>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <form action="{{ route('posts.send_message', $organization) }}">
                                <p>Все поля обязательны для заполнения</p>
                                <input type="email" name="email"
                                       value="{{ auth()->check() ? auth()->user()->email : old('email') }}"
                                       placeholder="Ваша почта"
                                       pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" {{ auth()->check() ? 'readonly' : '' }}>
                                <input type="text" name="phone" placeholder="Ваш номер телефона"
                                       value="{{ auth()->check() && !empty(auth()->user()->phone) ? auth()->user()->phone : old('phone') }}" {{ auth()->check() && !empty(auth()->user()->phone) ? 'readonly' : '' }}>
                                <textarea name="text" placeholder="Напишите сообщение">{{ old('text') }}</textarea>
                                <button class="button fullwidth margin-top-5">Отправить</button>
                            </form>
                        </div>
                        <!-- Agent Widget / End -->

                    </div>
                    <!-- Widget / End -->
                </div>
            </div>
            <!-- Sidebar / End -->

        </div>
    </div>
    <br>
    <br>
    <br>

@endsection
@push('js')
    <script>
      function updateURLParameter(param, value, url) {
        var newAdditionalURL = "";
        if (url === undefined) {
          url = window.location.href;
        }
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var requestURI = tempArray[1];
        var temp = "";
        if (requestURI) {
          tempArray = requestURI.split("&");
          for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] !== param) {
              newAdditionalURL += temp + tempArray[i];
              temp = "&";
            }
          }
        }
        var rows_txt = temp + "" + param + "=" + value;

        return baseURL + "?" + newAdditionalURL + rows_txt;
      }

      function changePostsSortOrder(el) {
        var urlValue = 'created_at';
        if (el.val() !== 'oldest' && el.val() !== 'newest') {
          urlValue = 'name';
        }
        var url = updateURLParameter(el.attr('name'), urlValue);
        window.location.href = updateURLParameter('order', el.find('option[value=' + el.val() + ']').data('order'), url);
      }

      function submitFilterForm(e, form) {
        e.preventDefault();
        var url = form.attr('action') + '?';
        var notSendArray = [
          '',
          '0',
          0,
          null,
          undefined,
        ];
        form.find('[name]').each(function () {
          var el = $(this);
          if (el.attr('type') !== 'hidden') {
            if (el.attr('type') === 'checkbox' || el.attr('type') === 'radio') {
              if (el.prop('checked')) {
                url += el.attr('name') + '=' + el.val() + '&';
              }
            } else if ($.inArray(el.val(), notSendArray) === -1) {
              url += el.attr('name') + '=' + el.val() + '&';
            }
          }
        });
        url = url.slice(0, -1);

        return window.location.href = url;
      }
    </script>
@endpush