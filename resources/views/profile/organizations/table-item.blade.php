<tr>
    <td class="title-container">
        @if ($organization->photos && $organization->photos->count())
            <img src="{{ asset($organization->photos->first()->path) }}" alt="{{ $organization->title }}">
        @else
            <img src="{{ asset('images/no-image.png') }}" alt="{{ $organization->title }}">
        @endif
        <div class="title">
            <h4><a href="{{--{{ route('organizations.show', $organization->slug) }}--}}">{{ $organization->title }}</a></h4>
            <span>{{ $organization->region->name }}, {{ $organization->city->name }}</span>
            <span class="table-property-price">{{ $organization->organizationType ? $organization->organizationType->title : 'Не известно' }}</span>
        </div>
    </td>
    <td class="expire-date">Добавлено: <span class="label label-info">{{ $organization->created_at->format('Y-m-d') }}</span></td>
    <td class="action">
        <a href="{{ route('profile.organizations.edit', $organization->id) }}"><i class="fa fa-pencil"></i> Редакт.</a>
        <a href="#" class="delete" onclick="if (confirm('Удалить вашу организацию навсегда?')) {$('#post-destroy-{{ $organization->id }}').submit()}"><i class="fa fa-remove"></i> Удалить</a>

        <form action="{{ route('profile.organizations.destroy', $organization) }}" id="post-destroy-{{ $organization->id }}" method="post" style="display: none;">
            {{ csrf_field() }}{{ method_field('DELETE') }}
        </form>
    </td>
</tr>
