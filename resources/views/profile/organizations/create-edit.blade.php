@extends('layouts.main')
@section('title', empty($organization->id) ? 'Добавление организации' : 'Редактирование организации')
@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <style>
        .holder.holder-textarea {
            position: absolute;
            margin: 20px 22px;
            color: #A3A3A3;
            cursor: auto;
            font-size: 11pt;
            z-index: 1;
        }
        .holder.holder-input {
            position: absolute;
            margin: 12px 21px;
            color: #A3A3A3;
            cursor: auto;
            font-size: 11pt;
            z-index: 1;
        }

    </style>
@endpush
@section('content')
    <!-- Titlebar
================================================== -->
    <div id="titlebar" class="submit-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><i class="fa fa-plus-circle"></i>{{ empty($organization->id) ? 'Добавить организацию' : 'Редактирование организации' }} </h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li><a href="{{ route('profile.index') }}">Мой профиль</a></li>
                            <li>Организации</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row">
            @if (!empty($organization->id))
                <form action="{{ route('profile.organizations.update', $organization->id) }}" method="post" enctype="multipart/form-data" id="post-form">
                {{ method_field('PUT') }}
            @else
                <form action="{{ route('profile.organizations.store') }}" method="post" enctype="multipart/form-data" id="post-form">
            @endif
            {{ csrf_field() }}
            <!-- Submit Page -->
                <div class="col-md-12">
                    <div class="submit-page">

                        <div class="notification notice large margin-bottom-20">
                            <ul>
                                <li>Внесите информацию об организации в соответствующие поля ниже.</li>
                                <li>Если вам непонятно, что нужно писать в поле — воспользуйтесь значком <i data-tip-content="Тут будет подсказка" class="tip"></i></li>
                                <li>Звездочкой <span class="required">*</span> помечены обязательные поля для заполнения</li>
                            </ul>
                        </div>
                        @if($errors->any())
                            <div class="notification error large">
                                <h4 class="text-center">Исправьте ошибки и повторите попытку</h4>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- Section -->
                        {{--<h3>Общая информация</h3>--}}
                        <div class="submit-section" style="padding-top: 0; border-top: 0;">

                            <!-- Row -->
                            <div class="row with-forms">

                                <div class="row col-sm-12">
                                    <div class="col-md-5 {{ $errors->has('organization_type_id') ? 'has-error' : '' }} form">
                                        {{--<label for="organization_type_id">Тип <span class="required">*</span><i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i></label>--}}
                                        <div class="disabled-first-option">
                                            <select name="organization_type_id" id="organization_type_id" required>
                                                <option value="0">Тип организации *</option>
                                                @foreach($organization_types as $organization_type)
                                                    <option {{ old('organization_type_id', $organization->organization_type_id) == $organization_type->id ? 'selected' : '' }} value="{{ $organization_type->id }}">{{ $organization_type->title }}</option>
                                                @endforeach
                                            </select>
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i>
                                            </div>
                                        </div>
                                        @if ($errors->has('organization_type_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('organization_type_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    <!-- Title -->
                                    <div class="col-md-9 margin-bottom-10 form-group {{ $errors->has('title') ? 'has-error' : '' }} form">
                                        {{--<label for="post_title">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                        <div class="holder holder-input" style="{{ old('title', $organization->title) ? 'display:none;' : '' }}">Название <span class="required">*</span></div>
                                        <input class="search-field" type="text" id="post_title" value="{{ old('title', $organization->title) }}" name="title" required minlength="10" maxlength="90" style="margin-bottom: 0;">
                                        <em>Осталось знаков: <b><span id="title_count_chars">{{ 90 - strlen(old('title', $organization->title)) }}</span></b></em>
                                        <br>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                        @endif
                                        <div class="info-tip">
                                            <i class="tip" data-tip-content="Например, Наши пушистые друзья"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    @php
                                        $location_data = $organization;
                                        if (empty($organization->id)) {
                                            $location_data->country_id = old('country_id');
                                            $location_data->region_id = old('region_id');
                                            $location_data->city_id = old('city_id');
                                        }
                                    @endphp
                                    <country-region-city :post="{{ $location_data }}" :countries="{{ $countries }}"></country-region-city>

                                    <div class="col-sm-12">
                                        @if ($errors->has('country_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('country_id') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('region_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('region_id') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('city_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('city_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }} form">
                                            <label for="post_address">Адрес</label>
                                            <input class="search-field" type="text" id="post_address" value="{{ old('address', $organization->address) }}" name="address" placeholder="Улица и т.д.">
                                            @if ($errors->has('address'))
                                                <span class="help-block text-danger">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-sm-9">
                                    <div class="col-md-12 form-group {{ $errors->has('description') ? 'has-error' : '' }} form">
                                        <div class="holder holder-textarea" style="{{ old('description', $organization->description) ? 'display:none;' : '' }}">Описание <span class="required">*</span></div>
                                        <textarea class="WYSIWYG" name="description" cols="40" rows="3" id="description" spellcheck="true" required minlength="20" maxlength="2500">{{ old('description', $organization->description) }}</textarea>
                                        <em>Осталось знаков: <b><span id="description_count_chars">{{ 2500 - strlen(old('title', $organization->title)) }}</span></b></em>
                                    @if ($errors->has('description'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row col-sm-9 margin-top-10 ">
                                    <!-- Title -->
                                    <div class="col-md-12 margin-bottom-10 form-group {{ $errors->has('youtube_link') ? 'has-error' : '' }} form">
                                        {{--<label for="post_youtube_link">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                        <div class="holder holder-input" style="{{ old('youtube_link', $organization->youtube_link) ? 'display:none;' : '' }}">Ссылка на видео в YouTube </div>
                                        <input class="search-field" type="text" id="post_youtube_link" value="{{ old('youtube_link', $organization->youtube_link) }}" name="youtube_link" style="margin-bottom: 0;">
                                        <br>
                                        @if ($errors->has('youtube_link'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('youtube_link') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    <div class="col-md-9">
                                        <h3><i class="fa fa-plus-circle" aria-hidden="true" style="color: #fa5b0f;"></i> Фото</h3>
                                        <em>К-тво фото должно быть не больше 10</em>
                                        <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                                            <input type="file" name="images[]" class="image-upload" multiple>
                                        </div>
                                    </div>
                                    <div class="row col-sm-9 margin-top-10 ">
                                        <div class="col-sm-12">
                                            <h4 class="">Контакты</h4>
                                        </div>
                                        <div class="col-md-6 margin-bottom-10 form-group {{ $errors->has('contact_name') ? 'has-error' : '' }} form">
                                            {{--<label for="post_contact_name">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                            <div class="holder holder-input" style="{{ old('contact_name', (empty($organization->contact_name) ? auth()->user()->name : $organization->contact_name)) ? 'display:none;' : '' }}">Имя <span class="required">*</span></div>
                                            <input class="search-field" type="text" id="post_contact_name" value="{{ old('contact_name', (empty($organization->contact_name) ? auth()->user()->name : $organization->contact_name)) }}" name="contact_name" required style="margin-bottom: 0;">
                                            <br>
                                            @if ($errors->has('contact_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contact_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 margin-bottom-10 form-group {{ $errors->has('contact_surname') ? 'has-error' : '' }} form">
                                            {{--<label for="post_contact_surname">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                            <div class="holder holder-input" style="{{ old('contact_surname', $organization->contact_surname) ? 'display:none;' : '' }}">Фамилия <span class="required">*</span> </div>
                                            <input class="search-field" type="text" id="post_contact_surname" value="{{ old('contact_surname', $organization->contact_surname) }}" name="contact_surname" required style="margin-bottom: 0;">
                                            <br>
                                            @if ($errors->has('contact_surname'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contact_surname') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 margin-bottom-10 form-group {{ $errors->has('contact_email') ? 'has-error' : '' }} form">
                                            {{--<label for="post_contact_email">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                            <div class="holder holder-input" style="{{ old('contact_email', (empty($organization->contact_email) ? auth()->user()->email : $organization->contact_email)) ? 'display:none;' : '' }}">Email <span class="required">*</span> </div>
                                            <input class="search-field" type="email" id="post_contact_email" value="{{ old('contact_email', (empty($organization->contact_email) ? auth()->user()->email : $organization->contact_email)) }}" name="contact_email" required style="margin-bottom: 0;">
                                            <br>
                                            @if ($errors->has('contact_email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contact_email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 margin-bottom-10 form-group {{ $errors->has('contact_phone') ? 'has-error' : '' }} form">
                                            {{--<label for="post_contact_phone">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                            <div class="holder holder-input" style="{{ old('contact_phone', (empty($organization->contact_phone) ? auth()->user()->phone : $organization->contact_phone)) ? 'display:none;' : '' }}">Телефон <span class="required">*</span> </div>
                                            <input class="search-field" type="text" id="post_contact_phone" value="{{ old('contact_phone', (empty($organization->contact_phone) ? auth()->user()->phone : $organization->contact_phone)) }}" name="contact_phone" required style="margin-bottom: 0;">
                                            <br>
                                            @if ($errors->has('contact_phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contact_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkboxes {{ $errors->has('accept_terms') ? 'has-error' : '' }} form">
                                            <input id="check-33" onchange="isTermsChecked()" type="checkbox" name="accept_terms" value="1"
                                                   @if (old('accept_terms') == 1)
                                                   checked
                                                   @elseif ($organization->id)
                                                   checked
                                                    @endif
                                            >
                                            <label for="check-33">Соглашаюсь с <a href="{{ route('page.show', ['slug' => 'terms']) }}" target="_blank">условиями использования</a></label>

                                            @if ($errors->has('accept_terms'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('accept_terms') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                    <button type="submit" id="submitFormButton" class="button btn_submit_post preview margin-bottom-35 create-pet orange-button" style="margin-top: 20px;"> Добавить </button>
                                    </div>
                                </div>

                            </div>
                            <!-- Row / End -->

                        </div>
                        <!-- Section / End -->

                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@push('js')
    {{--<script type="text/javascript" src="{{ asset('scripts/dropzone.js') }}"></script>--}}
    <script>
        function isTermsChecked() {
          var termsCheckbox = $('#check-33');

          if (!termsCheckbox.prop('checked')) {
            $('#submitFormButton').prop('disabled', true);
            $('#submitFormButton').addClass('disabled');
          } else {
            $('#submitFormButton').prop('disabled', false);
            $('#submitFormButton').removeClass('disabled');
          }
        }
        $(function () {
          isTermsChecked();
          $(".holder + input,.holder + textarea").keyup(function() {
            if($(this).val().length) {
              $(this).prev('.holder').hide();
            } else {
              $(this).prev('.holder').show();
            }
          });
          $('#description').keyup(function () {
            var descLength = $(this).next().find('#description_count_chars');
            descLength.text(2500 - $(this).val().length);
          });
          $('#post_title').keyup(function () {
            var titleLength = $(this).next().find('#title_count_chars');
            titleLength.text(90 - $(this).val().length);
          });
          $(".holder").click(function() {
            $(this).next().focus();
          });
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".image-upload").fileinput({
                language: "ru",
                previewFileType: 'any',
                showCaption: false,
                showZoom: false,
                validateInitialCount: true,
                maxFileCount: 10,
                allowedFileExtensions: ["jpg", "png", "gif"],
                allowedFileTypes: ["image"],
                @if ($organization->photos()->count())
                    initialPreview: [
                        @foreach ($organization->photos as $image)
                            '{{ asset($image->path) }}',
                        @endforeach
                    ],
                    initialPreviewAsData: true,
                    initialPreviewConfig: [
                        @foreach ($organization->photos as $image)
                            {
                                caption: "{{ $image->file_name }}",
                                width: "120px",
                                ajaxDeleteSettings: '',
                                key: '{{ $image->id }}',
                                removeClass: 'btn btn-default removePhoto',
                                showDelete: true
                            },
                        @endforeach
                    ],
                @endif
                fileActionSettings: {
                    showZoom: false,
                    showDrag: false,
                },
                deleteExtraData: {id: '{{ !empty($organization->id) ? $organization->id : null }}'},
                deleteUrl: "{{ route('profile.organizations.delete_image') }}",
                ajaxDeleteSettings: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                showUpload: false,
                showRemove: false,
                overwriteInitial: false,
                initialCaption: "Choose photos for apartment",
            });
        });
    </script>
@endpush