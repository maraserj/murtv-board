@extends('layouts.main')
@section('title', 'Список моих организаций')

@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Организации</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li><a href="{{ route('profile.index') }}">Мой профиль</a></li>
                            <li>Организации</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container padding-bottom-30 padding-top-30">
        <div class="row">
            <!-- Widget -->
            {{--<div class="col-md-3">--}}
                {{--@include('profile.sidebar')--}}
            {{--</div>--}}

            <div class="col-md-12">
                <div class="row">
                    @if ($organizations->count())
                        <table class="manage-table responsive-table">
                            @each('profile.organizations.table-item', $organizations, 'organization')
                        </table>
                        <br>
                        {{ $organizations->appends(request()->all())->render() }}
                        <br>
                    @else
                        <div class="alert alert-warning text-center" style="padding: 30px">
                            <strong>У вас пока нет организаций.</strong>
                        </div>
                    @endif
                        <a href="{{ route('profile.organizations.create') }}" class="margin-top-40 button text-center">Добавить организацию</a>
                </div>
            </div>

        </div>
    </div>
@endsection