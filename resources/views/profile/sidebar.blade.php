<div class="sidebar left">

    <div class="my-account-nav-container">

        <ul class="my-account-nav">
            <li class="sub-nav-title">Управление аккаунтом</li>
            <li>
                <a href="{{ route('profile.index') }}" class="{{ Route::is('profile.index') ? 'current' : '' }}">
                    <i class="sl sl-icon-user"></i> Мой профиль
                </a>
            </li>
            <li>
                <a href="{{ route('profile.favourites.index') }}" class="{{ Route::is('profile.favourites.index') ? 'current' : '' }}">
                    <i class="sl sl-icon-star"></i> Избранное
                </a>
            </li>
        </ul>

        <ul class="my-account-nav">
            <li class="sub-nav-title">
                Управление объявлениями
            </li>
            <li>
                <a href="{{ route('profile.posts.index') }}" class="{{ Route::is('profile.posts.index') ? 'current' : '' }}">
                    <i class="sl sl-icon-docs"></i> Мои объявления
                </a>
            </li>
            <li>
                <a href="{{ route('profile.posts.create') }}" class="{{ Route::is('profile.posts.create') ? 'current' : '' }}">
                    <i class="sl sl-icon-action-redo"></i> Добавить объявление
                </a>
            </li>
        </ul>

        <ul class="my-account-nav">
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="sl sl-icon-power"></i> Выход
                </a>
            </li>
        </ul>

    </div>

</div>