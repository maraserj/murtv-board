@extends('layouts.main')
@section('title', 'Избранные объявления')
@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Избранное</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li><a href="{{ route('profile.index') }}">Мой профиль</a></li>
                            <li>Избранное</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container padding-bottom-30 padding-top-30">
        <div class="row">
            {{--<!-- Widget -->--}}
            {{--<div class="col-md-3">--}}
                {{--@include('profile.sidebar')--}}
            {{--</div>--}}

            <div class="col-md-12">
                <div class="row">
                    @if ($favourites->count())
                    <table class="manage-table bookmarks-table responsive-table">
                        <tr>
                            <th><i class="fa fa-file-text"></i> Избранное</th>
                            <th></th>
                        </tr>
                        @foreach($favourites as $favourite)
                            <?php $post = null; ?>
                            <?php $post = $favourite->favourited; ?>
                            <tr>
                                <td class="title-container">
                                    @if ($post->photos && $post->photos->count())
                                        <img src="{{ asset($post->photos->first()->path) }}" alt="{{ $post->title }}">
                                    @else
                                        <img src="{{ asset('images/no-image.png') }}" alt="{{ $post->title }}">
                                    @endif
                                    <div class="title">
                                        <h4><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h4>
                                        <span><i class="fa fa-map-marker"></i> {{ $post->country->name . ', ' . $post->region->name . ', ' . $post->city->name }}</span>
                                        <span class="table-property-price">{{ $post->postType ? $post->postType->title : 'Не известно' }}</span>
                                    </div>
                                </td>
                                <td class="action">
                                    <a href="#" onclick="if (confirm('Удалить из избранного?')) {$('#post-fav-delete-{{ $post->id }}').submit()}" class="delete"><i class="fa fa-remove"></i> Удалить</a>
                                </td>
                                <form action="{{ route('profile.posts.remove_from_favourites', ['post_id' => $post->id]) }}" id="post-fav-delete-{{ $post->id }}" method="post" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            </tr>
                            <?php $post = null; ?>
                        @endforeach
                    </table>
                    @else
                        <div class="alert alert-warning text-center" style="padding: 30px">
                            <strong>У вас пока нет избранных объявлений.</strong>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection