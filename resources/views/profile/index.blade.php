@extends('layouts.main')
@section('title', 'Изменение моих персональных данных')
@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Мой профиль</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li>Мой профиль</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            {{--<!-- Widget -->--}}
            {{--<div class="col-md-3">--}}
            {{--@include('profile.sidebar')--}}
            {{--</div>--}}

            <div class="col-md-12">
                <div class="row">


                    <form action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data" id="form-profile-update">
                        <div class="col-md-3">
                            <!-- Avatar -->
                            <div class="edit-profile-photo">
                                <img src="{{ auth()->user()->avatar }}">
                                <div class="change-photo-btn">
                                    <div class="photoUpload">
                                        <span><i class="fa fa-upload"></i> Загрузить аватар</span>
                                        <input type="file" class="upload" name="avatar" onchange="submitProfileForm()">
                                    </div>
                                </div>
                            </div>
                            <em>Новое фото появится после сохранения</em>

                        </div>
                        <div class="col-md-9 my-profile">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label for="name">Имя</label>
                                        <input value="{{ old('name', auth()->user()->name) }}" name="name" id="name"
                                               type="text">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                        <label for="phone">Телефон</label>
                                        <input value="{{ old('phone', auth()->user()->phone) }}" name="phone" id="phone"
                                               type="text">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label for="email">Email</label>
                                        <input value="{{ old('email', auth()->user()->email) }}" name="email" id="email"
                                               class="disabled" disabled type="email" readonly>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                                        <label for="current_password">Пароль</label>
                                        <input value="" name="current_password" id="current_password" type="password">
                                        @if ($errors->has('current_password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label for="password">Новый пароль</label>
                                        <input value="" name="password" id="password" type="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="password_confirmation">Подтвердите новый пароль</label>
                                        <input value="" name="password_confirmation" id="password_confirmation" type="password">
                                    </div>
                                </div>
                            </div>


                            <button type="submit" class="button margin-top-20 margin-bottom-20">Сохранить <i class="fa fa-spinner hidden"></i></button>
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script>
        function submitProfileForm() {
          showLoader($('#form-profile-update button.button'));
          $('#form-profile-update').submit();
        }
    </script>
@endpush