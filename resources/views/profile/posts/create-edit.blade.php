@extends('layouts.main')
@section('title', !empty($post->id) ? 'Редактирование объявления - ' . $post->title : 'Создание нового объявления')
@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <style>
        .holder.holder-textarea {
            position: absolute;
            margin: 20px 22px;
            color: #A3A3A3;
            cursor: auto;
            font-size: 11pt;
            z-index: 1;
        }
        .holder.holder-input {
            position: absolute;
            margin: 12px 21px;
            color: #A3A3A3;
            cursor: auto;
            font-size: 11pt;
            z-index: 1;
        }

    </style>
@endpush
@section('content')
    <!-- Titlebar
================================================== -->
    <div id="titlebar" class="submit-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><i class="fa fa-plus-circle"></i>{{ !empty($post->id) ? 'Редактирование объявления - ' . $post->title : 'Создание нового объявления' }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li><a href="{{ route('profile.index') }}">Мой профиль</a></li>
                            <li><a href="{{ route('profile.posts.index') }}">Мои объвления</a></li>
                            <li>{{ !empty($post->id) ? 'Редактирование объявления - ' . $post->title : 'Создание нового объявления' }}</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row">
            @if (!empty($post->id))
                <form action="{{ route('profile.posts.update', $post->id) }}" method="post" enctype="multipart/form-data" id="post-form" onsubmit="submitPostForm(event, $(this))">
                {{ method_field('PUT') }}
            @else
                <form action="{{ route('profile.posts.store') }}" method="post" enctype="multipart/form-data" id="post-form" onsubmit="submitPostForm(event, $(this))">
            @endif
            {{ csrf_field() }}
            <!-- Submit Page -->
                <div class="col-md-12">
                    <div class="submit-page">

                        <div class="notification notice large margin-bottom-20">
                            <ul>
                                <li>Внесите информацию об объявлении в соответствующие поля ниже.</li>
                                <li>Если вам непонятно, что нужно писать в поле — воспользуйтесь значком <i data-tip-content="Тут будет подсказка" class="tip"></i></li>
                                <li>Звездочкой <span class="required">*</span> помечены обязательные поля для заполнения</li>
                            </ul>
                        </div>
                        @if($errors->any())
                            <div class="notification error large">
                                <h4 class="text-center">Исправьте ошибки и повторите попытку</h4>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- Section -->
                        {{--<h3>Общая информация</h3>--}}
                        <div class="submit-section" style="padding-top: 0; border-top: 0;">

                            <!-- Row -->
                            <div class="row with-forms">

                                <div class="row col-sm-12">
                                    <category-and-price :categories="{{ $categories }}" :post="{{ json_encode($post) }}"></category-and-price>
                                </div>
                                <div class="row col-sm-12">
                                    <div class="col-md-3 form-group {{ $errors->has('post_type_id') ? 'has-error' : '' }} form">
                                        {{--<label for="post_type_id">Тип <span class="required">*</span><i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i></label>--}}
                                        <div class="disabled-first-option">
                                            <select name="post_type_id" id="post_type_id">
                                                <option value="0">Тип *</option>
                                                @foreach($post_types as $post_type)
                                                    <option {{ old('post_type_id', $post->post_type_id) == $post_type->id ? 'selected' : '' }} value="{{ $post_type->id }}">{{ $post_type->title }}</option>
                                                @endforeach
                                            </select>
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i>
                                            </div>
                                        </div>
                                        @if ($errors->has('post_type_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('post_type_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    {{--<div class="col-md-3 form-group {{ $errors->has('animal_id') ? 'has-error' : '' }} form">--}}
                                        {{--<label for="type">Тип <span class="required">*</span><i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i></label>--}}
                                        {{--<div class="disabled-first-option">--}}
                                            {{--<input class="search-field {{ $post->animal_id ? 'disabled' : '' }}" type="text" id="animal_id" value="{{ old('animal_id', $post->animal_id) }}" name="animal_id" placeholder="Animal ID" {{ $post->animal_id ? 'disabled' : '' }}>--}}
                                            {{--<div class="info-tip">--}}
                                                {{--<i class="tip" data-tip-content="Введите Animal ID, если он есть. Если нету - он будет сгенерирован автоматически"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--@if ($errors->has('animal_id'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('animal_id') }}</strong>--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                </div>
                                @if($organizations->count())
                                    <div class="row col-sm-12">
                                        <div class="col-md-3 form-group {{ $errors->has('organization_id') ? 'has-error' : '' }} form">
                                            <label for="organization_id">Организация <i class="tip" data-tip-content="Если этот питомец из вашей организации - можете выбрать ее и в поиске люди смогут найти питомца по огранизации"></i></label>
                                            <div class="disabled-first-option">
                                                <select name="organization_id" id="organization_id" required>
                                                    <option value="0">Выберите из списка</option>
                                                    @foreach($organizations as $organization)
                                                        <option {{ old('organization_id', $post->organization_id) == $organization->id ? 'selected' : '' }} value="{{ $organization->id }}">{{ $organization->title . (empty($organization->organizationType) ? '' : ', ' . $organization->organizationType->title) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if ($errors->has('organization_id'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('organization_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                @endif

                                <div class="row col-sm-12">
                                    <!-- Title -->
                                    <div class="col-md-9 margin-bottom-10 form-group {{ $errors->has('title') ? 'has-error' : '' }} form">
                                        {{--<label for="post_title">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                        <div class="holder holder-input" style="{{ old('title', $post->title) ? 'display:none;' : '' }}">Краткое описание <span class="required">*</span></div>
                                        <input class="search-field" type="text" id="post_title" value="{{ old('title', $post->title) }}" name="title" required maxlength="90" style="margin-bottom: 0;">
                                        <em>Осталось знаков: <b><span id="title_count_chars">{{ 90 - strlen(old('title', $post->title)) }}</span></b></em>
                                        <br>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                        @endif
                                        <div class="info-tip">
                                            <i class="tip" data-tip-content="Краткое описание объявления"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-sm-12 toggled-visible">
                                    <div class="col-md-2 form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                                        {{--<label for="gender">Пол <span class="required">*</span></label>--}}
                                        <div class="disabled-first-option">
                                            <select name="gender" id="gender" required>
                                                <option value="0">Пол *</option>
                                                <option value="male" {{ old('gender', $post->gender) == 'male' ? 'selected' : '' }}>Мальчик</option>
                                                <option value="female" {{ old('gender', $post->gender) == 'female' ? 'selected' : '' }}>Девочка</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    @php
                                        $breed_data = $post;
                                        if (empty($post->id)) {
                                            $breed_data->breed_type_id = old('breed_type_id');
                                            $breed_data->breed_id = old('breed_id');
                                        }
                                    @endphp
                                    <breeds-and-breeds-type :post="{{ $breed_data }}" :breed_types="{{ $breed_types }}"></breeds-and-breeds-type>
                                    <div class="col-md-2">
                                        <div class="form-group {{ $errors->has('color_id') ? 'has-error' : '' }} form">
                                            {{--<label for="post_color_id">Окрас</label>--}}
                                            <select name="color_id" id="post_color_id" class="search-field">
                                                <option value="0">Окрас</option>
                                                @foreach($colors as $color)
                                                    <option value="{{ $color->id }}" {{ old('color_id', $post->color_id) == $color->id ? 'selected' : '' }}>{{ $color->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-sm-12 toggled-visible">
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} form">
                                            {{--<label for="post_name">Кличка</label>--}}
                                            <input class="search-field" type="text" id="post_name" value="{{ old('name', $post->name) }}" name="name" placeholder="Кличка">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('size') ? 'has-error' : '' }} form">
                                            {{--<label for="post_size">Рост в холке (в см)  <i class="tip" data-tip-content="Примерный рост питомца"></i></label>--}}
                                            <input class="search-field" type="text" id="post_size" value="{{ old('size', $post->size) }}" name="size" placeholder="Рост в холке (в см)">
                                            @if ($errors->has('size'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('size') }}</strong>
                                            </span>
                                            @endif
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Примерный рост питомца в холке (в см). Например: 25"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('birthday') ? 'has-error' : '' }} form">
                                            {{--<label for="post_birthday">Дата рождения <i class="tip" data-tip-content="Если вы нашли питомца и не знаете дату рождения, укажите приблизительный возраст."></i></label>--}}
                                            <select name="birthday" id="post_birthday" class="search-field">
                                                <option disabled>Возраст животного</option>
                                                <option value="0" {{ old('birthday', $post->birthday) == '0' || empty(old('birthday', $post->birthday)) ? 'selected' : '' }}> Возраст неизвестен</option>
                                                <option value="0-0.5" {{ old('birthday', $post->birthday) == '0-0.5' ? 'selected' : '' }}>До 6 месяцев </option>
                                                <option value="0.6-1" {{ old('birthday', $post->birthday) == '0.6-1' ? 'selected' : '' }}>От 6 месяцев до 1 года </option>
                                                <option value="1.1-2" {{ old('birthday', $post->birthday) == '1.1-2' ? 'selected' : '' }}>1-2 года </option>
                                                <option value="2.1-3" {{ old('birthday', $post->birthday) == '2.1-3' ? 'selected' : '' }}>2-3 года </option>
                                                <option value="3.1-4" {{ old('birthday', $post->birthday) == '3.1-4' ? 'selected' : '' }}>3-4 года </option>
                                                <option value="4.1-5" {{ old('birthday', $post->birthday) == '4.1-5' ? 'selected' : '' }}>4-5 лет </option>
                                                <option value="5" {{ old('birthday', $post->birthday) == '5' ? 'selected' : '' }}>Более 5 лет</option>
                                            </select>
                                            @if ($errors->has('birthday'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="row col-sm-12">
                                    <div class="col-sm-12">
                                        @if ($errors->has('breed_type_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('breed_type_id') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('breed_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('breed_id') }}</strong>
                                            </span>
                                        @endif

                                        @if ($errors->has('color_id'))
                                            <span class="help-block text-danger">
                                            <strong>{{ $errors->first('color_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    @php
                                        $location_data = $post;
                                        if (empty($post->id)) {
                                            $location_data->country_id = old('country_id');
                                            $location_data->region_id = old('region_id');
                                            $location_data->city_id = old('city_id');
                                        }
                                    @endphp
                                    <country-region-city :post="{{ $location_data }}" :countries="{{ $countries }}"></country-region-city>

                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }} form">--}}
                                            {{--<label for="post_address">Адрес</label>--}}
                                            {{--<input class="search-field" type="text" id="post_address" value="{{ old('address', $post->address) }}" name="address" placeholder="Улица и т.д.">--}}
                                            {{--@if ($errors->has('address'))--}}
                                                {{--<span class="help-block text-danger">--}}
                                                {{--<strong>{{ $errors->first('address') }}</strong>--}}
                                            {{--</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-sm-12">
                                        @if ($errors->has('country_id'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('region_id'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('region_id') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('city_id'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="row col-sm-12 toggled-visible">
                                    <!-- Checkboxes -->
                                    <div class="col-md-9 checkboxes in-row margin-bottom-20">

                                        <input id="check-2" type="checkbox" name="guarded" value="1" {{ old('guarded', $post->guarded) ? 'checked' : '' }}>
                                        <label for="check-2">Привит</label>

                                        <input id="check-3" type="checkbox" name="sterilized" value="1" {{ old('sterilized', $post->sterilized) ? 'checked' : ''  }}>
                                        <label for="check-3">Стерилизован</label>

                                        <input id="check-4" type="checkbox" name="ill" value="1" {{ old('ill', $post->ill) ? 'checked' : ''  }}>
                                        <label for="check-4">Требует лечения</label>

                                        <input id="check-5" type="checkbox" name="deadly" value="1" {{ old('deadly', $post->deadly) ? 'checked' : ''  }}>
                                        <label for="check-5">Тяжелое состояние</label>

                                    </div>
                                    <!-- Checkboxes / End -->
                                </div>

                                <div class="row col-sm-9">
                                    <div class="col-md-6 toggled-visible checkboxes in-row margin-bottom-20 form-group {{ $errors->has('featured') ? 'has-error' : '' }}">
                                        <input id="check-32" type="checkbox" name="featured" value="1" {{ old('featured', $post->featured) ? 'checked' : '' }}>
                                        <label for="check-32">Особенное животное <i class="tip" data-tip-content="Поставьте галочку, если у питомца есть особенности например косоглазие, хроническое заболевание или инвалидность."></i></label>
                                    </div>
                                    <div class="col-md-12 form-group {{ $errors->has('description') ? 'has-error' : '' }} form">
                                        <div class="holder holder-textarea" style="{{ old('description', $post->description) ? 'display:none;' : '' }}">Описание <span class="required">*</span></div>
                                        <textarea class="WYSIWYG" name="description" cols="40" rows="3" id="description" spellcheck="true" required minlength="20" maxlength="2500">{{ old('description', $post->description) }}</textarea>
                                        <em>Осталось знаков: <b><span id="description_count_chars">{{ 2500 - strlen(old('title', $post->title)) }}</span></b></em>
                                    @if ($errors->has('description'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    <div class="col-md-9">
                                        <h3><i class="fa fa-plus-circle" aria-hidden="true" style="color: #fa5b0f;"></i> Фото</h3>
                                        <em>К-тво фото должно быть не больше 10</em>
                                        <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                                            <input type="file" name="images[]" class="image-upload" multiple>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkboxes {{ $errors->has('accept_terms') ? 'has-error' : '' }} form">
                                            <input id="check-33" onchange="isTermsChecked()" type="checkbox" name="accept_terms" value="1"
                                                @if (old('accept_terms') == 1)
                                                    checked
                                                @elseif ($post->id)
                                                    checked
                                                @endif
                                            >
                                            <label for="check-33">Соглашаюсь с <a href="{{ route('page.show', ['slug' => 'terms']) }}" target="_blank">условиями использования</a></label>

                                            @if ($errors->has('accept_terms'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('accept_terms') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        </div>
                                    <div class="col-md-12">
                                    <button type="submit" id="submitFormButton" class="button btn_submit_post preview margin-bottom-35 create-pet orange-button" style="margin-top: 20px;"> Добавить <i class="fa fa-spinner hidden"></i></button>
                                    </div>
                                </div>

                            </div>
                            <!-- Row / End -->

                        </div>
                        <!-- Section / End -->

                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@push('js')
{{--<script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>--}}
{{--<script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>--}}
{{--<script type="text/javascript" src="{{ asset('scripts/dropzone.js') }}"></script>--}}
<script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var submitButton = $('#submitFormButton');
    $(function () {
      toggleTypeSelect();
      isTermsChecked();
      $('#check-13').change(function () {
        toggleTypeSelect();
      });
      // flatpickr.localize(flatpickr.l10ns.ru);
      // flatpickr("#post_birthday", {
      //   maxDate: new Date(),
      // });
      // flatpickr.l10ns.default.firstDayOfWeek = 1; // Monday
      var btns = '<button type="button" class="kv-cust-btn btn btn-kv btn-secondary btn-default pull-left" onclick="makeAsMain($(this))" title="Сделать фото главным"{dataKey}>' +
        '<i class="fa fa-floppy-o"></i>' +
        '</button>';
        $(".image-upload").fileinput({
            otherActionButtons: btns,
            language: "ru",
            previewFileType: 'any',
            showCaption: false,
            showZoom: false,
            validateInitialCount: true,
            maxFileCount: 10,
            allowedFileExtensions: ["jpg", "png", "gif"],
            allowedFileTypes: ["image"],
            @if ($post->photos()->count())
                initialPreview: [
                    @foreach ($post->photos as $image)
                        '{{ asset($image->path) }}',
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    @foreach ($post->photos as $image)
                        {
                            caption: "{{ $image->file_name }}",
                            width: "120px",
                            ajaxDeleteSettings: '',
                            key: '{{ $image->id }}',
                            removeClass: 'btn btn-default removePhoto',
                            showDelete: true,
                            @if ($image->is_main)
                            frameClass: 'is-main-photo',
                            @endif
                        },
                    @endforeach
                ],
            @endif
            fileActionSettings: {
                showZoom: false,
                showDrag: false,
            },
            deleteExtraData: {id: '{{ !empty($post->id) ? $post->id : null }}'},
            deleteUrl: "{{ route('profile.posts.delete_image') }}",
            ajaxDeleteSettings: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            showUpload: false,
            showRemove: false,
            overwriteInitial: false,
            initialCaption: "Choose photos for apartment",
        });
    });
    function toggleTypeSelect() {
      if ($('#check-13').prop('checked')) {
        $('#post_type_id').closest('.form-group').fadeOut();
        $('#animal_id').closest('.form-group').fadeOut();
        $('.toggled-visible').fadeOut();
      } else {
        $('#post_type_id').closest('.form-group').fadeIn();
        $('#animal_id').closest('.form-group').fadeIn();
        $('.toggled-visible').fadeIn();
      }
    }
    function submitPostForm(event, form) {
      event.preventDefault();
      var formData = form.serializeArray();
      formData.push({name: 'validate', value: true});
      showLoader(submitButton);

      $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: formData,
        beforeSend: function () {
          form.find('[style*="border-color: rgb(169, 68, 66);"]').each(function () {
            $(this).css('border-color', '#e0e0e0');
            removeHelpValidationBlock($(this));
          });
        },
        success: function (response) {
          showLoader(submitButton);
          form.attr('onsubmit', '');
          form.submit();
        },
        error: function (errors) {
          showLoader(submitButton);
          toastr.error('Исправьте ошибки и повторите попытку');
          errors = errors.responseJSON;
          var i = 0,
            scrollTo = null;
          _.forEach(errors.errors, function (value, key) {
            var el = form.find('[name='+key+']');
            i++;
            if (i > 0 && i <= 1) {
              scrollTo = el;
            }
            addHelpValidationBlock(el, value);
            if (scrollTo) {
              $('html, body').animate({
                scrollTop: scrollTo.offset().top - 250
              }, 200);
            }
          });
        }
      })
    }
    function makeAsMain(el) {
      console.log(el.data('key'));
      $.ajax({
        url: '{{ route('profile.posts.photos.mark_as_main', ['post_id' => $post->id]) }}',
        method: 'post',
        data: {
          id: el.data('key'),
        },
        success: function (response) {
          if (response.status === 'ok') {
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);
        }
      });
    }
    function removeHelpValidationBlock(el) {
        el.closest('.form-group').find('.help-block').remove();
      }
    function addHelpValidationBlock(el, text) {
        el.css('border-color', '#a94442');
        var html = '<span class="help-block text-danger">' +
          '<strong>'+text+'</strong>' +
          '</span>';

        el.closest('.form-group').append(html);
      }
</script>
@endpush