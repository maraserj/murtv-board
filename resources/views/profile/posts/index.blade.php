@extends('layouts.main')
@section('title', 'Все мои объявления')
@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Объявления</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li><a href="{{ route('profile.index') }}">Мой профиль</a></li>
                            <li>Объявления</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container padding-bottom-30 padding-top-30">
        <div class="row">
            <!-- Widget -->
            {{--<div class="col-md-3">--}}
                {{--@include('profile.sidebar')--}}
            {{--</div>--}}

            <div class="col-md-12">
                <div class="row">
                    @if ($posts->count() || $posts_has_home->count())
                        <!-- TAB NAVIGATION -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#active" role="tab" data-toggle="tab">Активные</a></li>
                                <li><a href="#has_home" role="tab" data-toggle="tab">Уже приютили</a></li>
                            </ul>
                            <!-- TAB CONTENT -->
                            <div class="tab-content">
                                <div class="active tab-pane fade in" id="active">
                                    @if ($posts->count())
                                        <table class="manage-table responsive-table">
                                            @each('profile.posts.table-item', $posts, 'post')
                                        </table>
                                        <br>
                                        {{ $posts->appends(request()->all())->render() }}
                                        <br>
                                    @else
                                        <div class="alert alert-warning text-center" style="padding: 30px">
                                            <strong>У вас пока активных нет объявлений.</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="tab-pane fade" id="has_home">
                                    @if ($posts_has_home->count())
                                        <table class="manage-table responsive-table">
                                            @each('profile.posts.table-item', $posts_has_home, 'post')
                                        </table>
                                        <br>
                                        {{ $posts_has_home->appends(request()->all())->render() }}
                                        <br>
                                    @else
                                        <div class="alert alert-warning text-center" style="padding: 30px">
                                            <strong>У вас пока никого не приютили.</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        @else
                        <div class="alert alert-warning text-center" style="padding: 30px">
                            <strong>У вас пока нет объявлений.</strong>
                        </div>
                    @endif
                        <a href="{{ route('profile.posts.create') }}" class="margin-top-40 button text-center">Добавить объявление</a>
                </div>
            </div>

        </div>
    </div>
@endsection