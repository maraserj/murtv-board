<tr>
    <td class="title-container">
        @if ($post->photos && $post->photos()->main()->count())
            <img src="{{ asset($post->photos()->main()->first()->path) }}" alt="{{ $post->title }}">
        @elseif ($post->photos && $post->photos()->count())
            <img src="{{ asset($post->photos->first()->path) }}" alt="{{ $post->title }}">
        @else
            <img src="{{ asset('images/no-image.png') }}" alt="{{ $post->title }}">
        @endif
        <div class="title">
            <h4><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h4>
            <span>{{ $post->region->name }}, {{ $post->city->name }}</span>
            <span class="table-property-price">{{ $post->postType ? $post->postType->title : 'Не известно' }}</span>
        </div>
    </td>
    <td class="expire-date">Добавлено: <span class="label label-info">{{ $post->created_at->format('Y-m-d') }}</span></td>
    <td class="action">
        @if (!$post->has_home)
            <a href="#" onclick="if (confirm('Питомец нашел дом?')) {$('#post-update-{{ $post->id }}').submit()}"><i class="fa fa-eye-slash"></i> Приютили</a>
            <form action="{{ route('profile.posts.change_home_status', $post->id) }}" id="post-update-{{ $post->id }}" method="post" style="display: none;">
                {{ csrf_field() }}{{ method_field('PUT') }}
                <input type="hidden" name="changeStatus" value="1">
                <input type="hidden" name="has_home" value="1">
            </form>
        @endif
        <a href="{{ route('profile.posts.edit', $post->id) }}"><i class="fa fa-pencil"></i> Редакт.</a>
        <a href="#" class="delete" onclick="if (confirm('Удалить ваше объявление навсегда?')) {$('#post-destroy-{{ $post->id }}').submit()}"><i class="fa fa-remove"></i> Удалить</a>

        <form action="{{ route('profile.posts.destroy', $post) }}" id="post-destroy-{{ $post->id }}" method="post" style="display: none;">
            {{ csrf_field() }}{{ method_field('DELETE') }}
        </form>
    </td>
</tr>
