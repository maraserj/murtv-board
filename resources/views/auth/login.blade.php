@extends('layouts.main')


@section('title', 'Вход и регистрация на сайте')
@section('keywords', 'Вход и регистрация на сайте')
@section('description', 'Вход и регистрация на сайте')

@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Вход и Регистрация</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li>Вход и Регистрация</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <!-- Container -->
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <!--Tab -->
                <div class="my-account style-1 margin-top-5 margin-bottom-40">

                    <ul class="tabs-nav">
                        <li class="{{ old('type', 'login') == 'login' ? 'active' : '' }}"><a href="#login">Авторизация</a></li>
                        <li class="{{ old('type', 'login') == 'register' ? 'active' : '' }}"><a href="#register">Регистрация</a></li>
                    </ul>

                    <div class="tabs-container alt">

                        <!-- Login -->
                        <div class="tab-content" id="login" style="{{ old('type', 'login') == 'login' ? '' : 'display: none;' }}">
                            <form method="post" class="login" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="type" value="login">

                                <p class="form-row form-row-wide {{ old('type', 'login') == 'login' && $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email"> Email:
                                        <i class="im im-icon-Male"></i>
                                        <input type="text" class="input-text" name="email" id="email" value="{{ old('type', 'login') == 'login' ? old('name') : '' }}" placeholder="Ваш Email" autofocus>
                                    </label>
                                    @if (old('type', 'login') == 'login' && $errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </p>

                                <p class="form-row form-row-wide {{ old('type', 'login') == 'login' && $errors->has('password') ? 'has-error' : '' }}">
                                    <label for="password">Пароль:
                                        <i class="im im-icon-Lock-2"></i>
                                        <input class="input-text" type="password" name="password" id="password" placeholder="Ваш пароль">
                                    </label>
                                    @if (old('type', 'login') == 'login' && $errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </p>
                                <div class="lost_password">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="rememberme" class="rememberme">
                                                <input name="remember" type="checkbox" id="rememberme" {{ old('remember') ? 'checked' : '' }}> Запомнить меня
                                            </label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="" class="rememberme"><a href="{{ route('password.request') }}" >Забыли пароль?</a></label>
                                        </div>
                                    </div>
                                </div>

                                <p class="form-row">
                                    <input type="submit" class="button border margin-top-10" name="login" style="width: 100%;" value="Войти">
                                </p>

                            </form>
                        </div>

                        <!-- Register -->
                        <div class="tab-content" id="register" style="{{ old('type', 'login') == 'register' ? '' : 'display: none;' }}">

                            <form method="post" class="register" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="type" value="register">

                                <p class="form-row form-row-wide {{ old('type', 'register') == 'register' && $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="username2">Имя:
                                        <i class="im im-icon-Male"></i>
                                        <input type="text" class="input-text" name="name" id="username2" value="{{ old('type', 'register') == 'register' ? old('name') : '' }}" required placeholder="Ваше имя">
                                    </label>
                                    @if (old('type', 'register') == 'register' && $errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </p>

                                <p class="form-row form-row-wide {{ old('type', 'register') == 'register' && $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email2">Ваш email:
                                        <i class="im im-icon-Mail"></i>
                                        <input type="email" class="input-text" name="email" id="email2" value="{{ old('type', 'register') == 'register' ? old('email') : '' }}" placeholder="Ваш email" required>
                                    </label>

                                    @if (old('type', 'register') == 'register' && $errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </p>

                                <p class="form-row form-row-wide {{ old('type', 'register') == 'register' && $errors->has('phone') ? 'has-error' : '' }}">
                                    <label for="phone2">Ваш телефон:
                                        <i class="im im-icon-Mail"></i>
                                        <input type="text" class="input-text" name="phone" id="phone2" value="{{ old('type', 'register') == 'register' ? old('phone') : '' }}" placeholder="Ваш телефон (не обязательно)" required>
                                    </label>

                                    @if (old('type', 'register') == 'register' && $errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </p>

                                <p class="form-row form-row-wide {{ old('type', 'register') == 'register' && $errors->has('password') ? 'has-error' : '' }}">
                                    <label for="password1">Пароль:
                                        <i class="im im-icon-Lock-2"></i>
                                        <input class="input-text" type="password" name="password" id="password1" placeholder="Ваш Пароль">
                                    </label>
                                    @if (old('type', 'register') == 'register' && $errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="password2">Повторите пароль:
                                        <i class="im im-icon-Lock-2"></i>
                                        <input class="input-text" type="password" name="password_confirmation" id="password2" placeholder="Повторите пароль">
                                    </label>
                                </p>

                                <p class="form-row">
                                    <input type="submit" class="button border fw margin-top-10" name="register" value="Зарегистрироваться" style="width: 100%;">
                                </p>

                            </form>
                        </div>

                        <hr>

                        <a href="{{ route('auth.social_sign', ['provider' => 'vkontakte']) }}" class="button social-login via-vk"><i class="fa fa-vk"></i> Войти через Вконтакте</a>
                        <a href="{{ route('auth.social_sign', ['provider' => 'google']) }}" class="button social-login via-gplus"><i class="fa fa-google-plus"></i> Войти через Google Plus</a>
                        <a href="{{ route('auth.social_sign', ['provider' => 'facebook']) }}" class="button social-login via-facebook"><i class="fa fa-facebook"></i> Войти через Facebook</a>
                        <a href="{{ route('auth.social_sign', ['provider' => 'odnoklassniki']) }}" class="button social-login via-odnoklassniki"><i class="fa fa-odnoklassniki"></i> Войти через Одноклассники</a>

                    </div>
                </div>



            </div>
        </div>

    </div>
    <!-- Container / End -->
@endsection
