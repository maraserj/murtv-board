@extends('layouts.main')

@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Сброс пароля</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li>Сброс пароля</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <!-- Container -->
    <div class="container" style="padding: 30px 0 40px 0;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-12 text-center control-label" style="text-align: center;">E-Mail</label>

                        <div class="col-md-10 col-md-offset-1">
                            <input id="email" type="email" class="form-control text-center" name="email" value="{{ old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <button type="submit" class="button text-center border margin-top-10" style="width: 100%;">
                                Отправить ссылку сброса
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
