<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('title', config('app.name', 'Laravel'))</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="@yield('keywords', config('app.name', 'Laravel'))">
    <meta name="description" content="@yield('description', config('app.name', 'Laravel'))">

    <meta name="theme-color" content="#fa5b0f">
    @stack('meta')

    @if (request()->ip() != '::1')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-K2CKPGK');</script>
    <!-- End Google Tag Manager -->
    @endif

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/orange.css') }}" id="colors">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    @stack('css')
</head>

<body>
@if (request()->ip() != '::1')
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K2CKPGK" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
@endif

<div id="app">
    <!-- Wrapper -->
    <div id="wrapper">

        @include('partials.header')

        @yield('content')

        @include('partials.footer')

    </div>
    <!-- Wrapper / End -->
</div>
<!-- Scripts
        ================================================== -->
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('scripts/chosen.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('scripts/magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/rangeSlider.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/sticky-kit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/masonry.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/mmenu.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/tooltips.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scripts/custom.js') }}"></script>
<script>
    @if (session()->has('success'))
        toastr.success('Успешно.', '{{ session()->get('success') }}');
    @elseif (session()->has('error'))
        toastr.error('Ошибка.', '{{ session()->get('error') }}');
    @endif
</script>

@stack('js')
</body>
</html>