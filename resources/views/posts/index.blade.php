@extends('layouts.main')

@section('title', $seo_settings['search_page_title'] ?? 'MUR TV')
@section('keywords', $seo_settings['search_page_keywords'] ?? 'MUR TV')
@section('description', $seo_settings['search_page_description'] ?? 'MUR TV')
@push('meta')
    <meta property="og:title" content="{{ $seo_settings['search_page_title'] ?? 'MUR TV' }}" />
    <meta property="og:description" content="{{ $seo_settings['search_page_description'] ?? 'MUR TV' }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="{{ env('FB_CLIENT_ID') }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image:width" content="640">
    <meta property="og:image:height" content="480">
    @if (!empty($main_settings['opengraph_main_image']))
        <meta property="og:image" content="{{ asset($main_settings['opengraph_main_image']) }}" />
    @endif
@endpush
@section('content')
    @include('partials.posts-search-filter')
    <div class="container">
        <div class="row fullwidth-layout">

            <div class="col-md-12">

                <!-- Sorting / Layout Switcher -->
                <div class="row margin-bottom-15">

                    <div class="col-md-4">
                        <!-- Sort by -->
                        <div class="sort-by">
                            <label>Сортировать по:</label>

                            <div class="sort-by-select">
                                <select data-placeholder="Сортировка" name="sortBy" class="chosen-select-no-single"
                                        onchange="changePostsSortOrder($(this))">
                                    <option {{ request('sortBy') == 'created_at' && request('order') == 'desc' ? 'selected' : '' }} value="newest"
                                            data-order="desc">От новых к старым
                                    </option>
                                    <option {{ request('sortBy') == 'created_at' && request('order') == 'asc' ? 'selected' : '' }} value="oldest"
                                            data-order="asc">От старых к новым
                                    </option>
                                    <option {{ request('sortBy') == 'name' && request('order') == 'asc' ? 'selected' : '' }} value="az"
                                            data-order="asc">От А до Я (по кличке)
                                    </option>
                                    <option {{ request('sortBy') == 'name'  && request('order') == 'desc' ? 'selected' : '' }} value="za"
                                            data-order="desc">От Я до А (по кличке)
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="sort-by">
                            <label>Найдено: <b>{{ $posts_count ?? 0 }}</b> шт.</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Layout Switcher -->
                        <div class="layout-switcher">
                            <a href="#" class="grid-three"><i class="fa fa-th"></i></a>
                            {{--<a href="#" class="grid"><i class="fa fa-th-large"></i></a>--}}
                            <a href="#" class="list"><i class="fa fa-th-list"></i></a>
                        </div>
                    </div>
                </div>
                
                @if (!empty($posts) && $posts->count())
                    <!-- Listings -->
                    <div class="listings-container grid-layout-three">
                        @include('posts.partials.list-item', ['posts' => $posts, 'favourites_array' => $favourites_array])
                    </div>
                    <!-- Listings Container / End -->
                @else
                    <div class="alert alert-info text-center" style="padding: 30px">
                        <strong>К сожалению, по Вашему запросу ничего не найдено. Попробуйте изменить параметры поиска или
                            <a href="{{ route('posts.index') }}"> сбросить фильтр</a></strong>
                    </div>
                @endif

                <div class="clearfix"></div>
                <!-- Pagination -->
                <div class="pagination-container margin-top-20">
                    {{ $posts->appends(request()->all())->render() }}
                </div>
                <!-- Pagination / End -->

            </div>

        </div>
    </div>
@endsection
@push('js')
    <script>
      function updateURLParameter(param, value, url) {
        var newAdditionalURL = "";
        if (url === undefined) {
          url = window.location.href;
        }
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var requestURI = tempArray[1];
        var temp = "";
        if (requestURI) {
          tempArray = requestURI.split("&");
          for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] !== param) {
              newAdditionalURL += temp + tempArray[i];
              temp = "&";
            }
          }
        }
        var rows_txt = temp + "" + param + "=" + value;

        return baseURL + "?" + newAdditionalURL + rows_txt;
      }

      function changePostsSortOrder(el) {
        var urlValue = 'created_at';
        if (el.val() !== 'oldest' && el.val() !== 'newest') {
          urlValue = 'name';
        }
        var url = updateURLParameter(el.attr('name'), urlValue);
        window.location.href = updateURLParameter('order', el.find('option[value=' + el.val() + ']').data('order'), url);
      }
      function submitFilterForm(e, form) {
        e.preventDefault();
        var url = form.attr('action') + '?';
        var notSendArray = [
          '',
          '0',
          0,
          null,
          undefined,
        ];
        form.find('[name]').each(function () {
          var el = $(this);
          // if (el.attr('type') !== 'hidden') {
            if (el.attr('type') === 'checkbox' || el.attr('type') === 'radio') {
              if (el.prop('checked')) {
                url += el.attr('name') + '=' + el.val() + '&';
              }
            } else if ($.inArray(el.val(), notSendArray) === -1) {
              url += el.attr('name') + '=' + el.val() + '&';
            }
          // }
        });
        url = url.slice(0, -1);

        return window.location.href = url;
      }
    </script>
@endpush