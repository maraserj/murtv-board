@foreach($posts as $post)
    <div class="listing-item">

        <a href="{{ route('posts.show', $post->slug) }}" class="listing-img-container">

            <div class="listing-badges">
                <span>{{ $post->postType ? $post->postType->title : 'Не известно' }}</span>
            </div>

            <div class="listing-img-content">
                <div class="date-time">{{--<i class="fa fa-calendar-o"></i> --}}{{ $post->created_at->format('d.m.Y') }}</div>
            </div>

            @if ($post->photos && $post->photos()->main()->count())
                <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;background:url('{{ asset($post->photos()->main()->first()->path) }}') center center; background-size:cover;"></div>
            @elseif ($post->photos && $post->photos()->count())
                <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;background:url('{{ asset($post->photos->first()->path) }}') center center; background-size:cover;"></div>
            @else
                <div class="magic-img" style="position: relative; height: 256px; width:100%; overflow:hidden; display: block;background:url('{{ asset('images/no-image.png') }}') center center; background-size:cover;"></div>
            @endif

        </a>

        <div class="listing-content">

            <div class="listing-title">
                <h4>
                    <a href="{{ route('posts.show', $post->slug) }}">{{ $post->title or 'Неизвестно' }}</a>
                </h4>
                <a href="{{ route('posts.show', $post->slug) }}" class="details button border">Подробнее</a>
            </div>

            <ul class="listing-details">
                @if (!empty($post->breedType))
                    <li>
                        <span>
                        @if(!empty($post->breedType->image))
                                <img src="{{ asset($post->breedType->image) }}" alt="{{ !empty($post->breedType) ? $post->breedType->title : '' }}" />
                            @else
                                <img src="{{ asset($post->breedType->slug == 'koshka' ? 'images/cat.png' : 'images/dog.png') }}" alt="{{ !empty($post->breedType) ? $post->breedType->title : '' }}" />
                            @endif
                        </span>
                    </li>
                @endif
                <li>
                    <span>
                        @empty($post->gender)
                            Неизвестно
                        @else
                            <img src="{{ $post->gender == 'male' ? asset('images/male.png') : asset('images/female.png') }}" alt="{{ $post->gender == 'male' ? 'Мальчик' : 'Девочка' }}" />
                        @endempty
                    </span>
                </li>
                <li>
                    <span>{{ $post->breed ? $post->breed->title : '' }}</span>
                </li>
            </ul>

            <div class="listing-footer">
                <a href="https://maps.google.com/maps?q={{ $post->country->name . ', ' . $post->region->name . ', ' . $post->city->name }}" class="listing-address popup-gmaps">
                    <i class="fa fa-map-marker"></i>
                    {{ $post->country->name . ', ' . $post->city->name }}
                </a>
            </div>

        </div>

    </div>
@endforeach