@extends('layouts.main')

@section('title', $post->title)
@section('keywords', 'MUR TV')
@section('description', str_limit(strip_tags($post->description), 250))
@push('meta')
    <meta property="og:title" content="{{ $post->title ?? 'MUR TV' }}" />
    <meta property="og:description" content="{{ str_limit(strip_tags($post->description), 250) ?? 'MUR TV' }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="{{ env('FB_CLIENT_ID') }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image:width" content="640">
    <meta property="og:image:height" content="480">
    @if ($post->photos()->main()->count())
        <meta property="og:image" content="{{ asset($post->photos()->main()->first()->path) }}">
    @elseif ($post->photos()->count())
        <meta property="og:image" content="{{ asset($post->photos()->first()->path) }}">
    @elseif (!empty($main_settings['opengraph_main_image']))
        <meta property="og:image" content="{{ asset($main_settings['opengraph_main_image']) }}">
    @endif
@endpush

@section('content')

    <div id="titlebar" class="property-titlebar margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <a href="{{ URL::previous() }}" class="back-to-listings"></a>
                    <div class="property-title">
                        <h2>{{ $post->title }} <span class="property-badge">{{ $post->postType ? $post->postType->title : 'Не известно' }}</span>
                        </h2>
                        <span>
                            <a href="#location" class="listing-address">
                                <i class="fa fa-map-marker"></i>
                                {{ $post->country->name . ', ' . $post->region->name . ', ' . $post->city->name }}
                            </a>
                        </span>
                    </div>
                </div>
                <div class="property-pricing" style="margin-right: 85px;">
                    <div class="property-price">
                        <single-item-favourites-button :post_id="{{ $post->id }}" :is_in_favourites="{{ in_array($post->id, $favourites_array) ? 'true' : 'false' }}" :logged_user="{{ auth()->check() ? 'true' : 'false' }}"></single-item-favourites-button>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row">
            @if ($post->photos()->count())
                @php
                    $images = $post->photos()->orderBy('is_main', 'desc')->get();
                @endphp
                <div class="margin-bottom-50 col-md-6">

                    <!-- Slider -->
                    <div class="property-slider default">
                        @foreach($images as $image)
                            <a href="{{ asset($image->path) }}" data-background-image="{{ asset($image->path) }}" class="item mfp-gallery"></a>
                        @endforeach
                    </div>

                    <!-- Slider Thumbs -->
                    <div class="property-slider-nav">
                        @foreach($images as $image)
                            <div class="item"><img src="{{ asset($image->path) }}" alt="{{ $post->title }}"></div>
                        @endforeach
                    </div>
                </div>
            @endif


            <!-- Property Description -->
            <div class="col-md-{{ $post->photos()->count() ? '6' : '10' }}">
                <div class="property-description">
                    <!-- Main Features -->
                    <ul class="property-main-features">
                        <li>Вид: <a href="{{ route('posts.index', ['breed_type_id' => $post->breed_type_id]) }}">{{ $post->breedType ? $post->breedType->title : '' }}</a></li>
                        <li>Порода: <a href="{{ route('posts.index', ['breed_id' => $post->breed_id]) }}">{{ $post->breed ? $post->breed->title : '' }}</a></li>
                        @if($post->color)
                            <li>Окрас: <a href="{{ route('posts.index', ['color_id' => $post->color_id]) }}">{{ $post->color->title }}</a></li>
                        @endif
                        <li>Пол: <a href="{{ route('posts.index', ['gender' => $post->gender]) }}">{{ !empty($post->gender) ? ($post->gender == 'male' ? 'Мальчик' : 'Девочка') : 'Неизвестно' }}</a></li>
                    </ul>


                    <!-- Description -->
                    <h3 class="desc-headline">Описание</h3>
                    <div class="{{--show-more--}} text-justify">
                        {!! strip_tags($post->description, '<span><p><br>') !!}
                        {{--<a href="#" class="show-more-button">Show More <i class="fa fa-angle-down"></i></a>--}}
                    </div>

                </div>
                <div class="row text-center">
                    <br>
                    <a class="button fullwidth margin-top-5" style="width: 50%;" data-toggle="modal" href="#send-author-message-modal">Написать автору</a>

                </div>
            </div>
            <!-- Property Description / End -->

            <div class="col-sm-12 property-description">
                <!-- Details -->
                <h3 class="desc-headline">Детали</h3>
                <ul class="property-features margin-top-0">
                    <li>Кличка: <span>{{ $post->name or 'Не указана' }}</span></li>
                    <li>Страна: <span>{{ $post->country->name }}</span></li>
                    <li>Регион: <span>{{ $post->region->name }}</span></li>
                    <li>Город: <span>{{ $post->city->name }}</span></li>
                    <li>Адрес: <span>{{ $post->address or 'Не указан' }}</span></li>
                    <li>Вид: <span>{{ $post->breedType ? $post->breedType->title : '' }}</span></li>
                    <li>Порода: <span>{{ $post->breed ? $post->breed->title : '' }}</span></li>
                    <li>Пол: <span>{{ !empty($post->gender) ? ($post->gender == 'male' ? 'Мальчик' : 'Девочка') : 'Неизвестно' }}</span></li>
                    @if ($post->color)
                        <li>Окрас: <span>{{ $post->color->title }}</span></li>
                    @endif
                    <li>Рост: <span>{{ !empty($post->size) ? $post->size . ' см' : 'Не указан' }}</span></li>
                    <li>Дата рождения: <span>{{ $post->birthday or 'Не указана' }}</span></li>
                </ul>


                @if ($post->guarder && $post->sterilized && $post->ill && $post->deadly && $post->featured)
                    <!-- Features -->
                    <h3 class="desc-headline">Особенности</h3>
                    <ul class="property-features checkboxes margin-top-0">
                        @if (!empty($post->guarder))
                            <li>Привит</li>
                        @endif
                        @if (!empty($post->sterilized))
                            <li>Стерилизован</li>
                        @endif
                        @if (!empty($post->ill))
                            <li>Требует лечения</li>
                        @endif
                        @if (!empty($post->deadly))
                            <li>Тяжелое состояние</li>
                        @endif
                        @if (!empty($post->featured))
                            <li>Особенный</li>
                        @endif
                    </ul>
                @endif


                <!-- Location -->
                {{--<h3 class="desc-headline no-border" id="location">Location</h3>--}}
                {{--<div id="propertyMap-container">--}}
                {{--<div id="propertyMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>--}}
                {{--<a href="#" id="streetView">Street View</a>--}}
                {{--</div>--}}

            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="modal fade" id="send-author-message-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">Связь с автором объявления</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('posts.send_message', ['post_id' => $post->id]) }}" method="post">
                        {{ csrf_field() }}
                        <p>Все поля обязательны для заполнения</p>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Испраьте ошибки и повторите попытку!</strong>
                            </div>
                        @endif
                        <div>
                            <input type="email" name="email" value="{{ auth()->check() ? auth()->user()->email : old('email') }}" placeholder="Ваша почта" {{ auth()->check() ? 'readonly' : '' }} required minlength="5" style="{{ $errors->has('email') ? 'border-color: #a94442' : '' }}">
                            @if ($errors->has('name'))
                                <div class="help-block">
                                    <strong>
                                        {{ $errors->first('name') }}
                                    </strong>
                                </div>
                            @endif
                        </div>
                        <div>
                            <input type="text" name="name" placeholder="Ваше имя" value="{{ auth()->check() && !empty(auth()->user()->name) ? auth()->user()->name : old('name') }}" {{ auth()->check() && !empty(auth()->user()->name) ? 'readonly' : '' }}
                        minlength="2" required style="{{ $errors->has('name') ? 'border-color: #a94442' : '' }}">

                            @if ($errors->has('name'))
                                <div class="help-block">
                                    <strong class="text-danger">
                                        {{ $errors->first('name') }}
                                    </strong>
                                </div>
                            @endif
                        </div>
                        <div>
                            <textarea name="text" placeholder="Напишите сообщение" required minlength="20" style="{{ $errors->has('text') ? 'border-color: #a94442' : '' }}">{{ old('text') }}</textarea>
                            @if ($errors->has('text'))
                                <div class="help-block">
                                    <strong class="text-danger">
                                        {{ $errors->first('text') }}
                                    </strong>
                                </div>
                            @endif
                        </div>
                        <button type="submit" class="button fullwidth margin-top-5">Отправить</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
@push('js')
    <script>
        $(document).ready(function () {
          @if ($errors->any())
            $('#send-author-message-modal').modal('show');
          @endif
        });
      function updateURLParameter(param, value, url) {
        var newAdditionalURL = "";
        if (url === undefined) {
          url = window.location.href;
        }
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var requestURI = tempArray[1];
        var temp = "";
        if (requestURI) {
          tempArray = requestURI.split("&");
          for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] !== param) {
              newAdditionalURL += temp + tempArray[i];
              temp = "&";
            }
          }
        }
        var rows_txt = temp + "" + param + "=" + value;

        return baseURL + "?" + newAdditionalURL + rows_txt;
      }

      function changePostsSortOrder(el) {
        var urlValue = 'created_at';
        if (el.val() !== 'oldest' && el.val() !== 'newest') {
          urlValue = 'name';
        }
        var url = updateURLParameter(el.attr('name'), urlValue);
        window.location.href = updateURLParameter('order', el.find('option[value=' + el.val() + ']').data('order'), url);
      }
      function submitFilterForm(e, form) {
        e.preventDefault();
        var url = form.attr('action') + '?';
        var notSendArray = [
          '',
          '0',
          0,
          null,
          undefined,
        ];
        form.find('[name]').each(function () {
          var el = $(this);
          if (el.attr('type') !== 'hidden') {
            if (el.attr('type') === 'checkbox' || el.attr('type') === 'radio') {
              if (el.prop('checked')) {
                url += el.attr('name') + '=' + el.val() + '&';
              }
            } else if ($.inArray(el.val(), notSendArray) === -1) {
              url += el.attr('name') + '=' + el.val() + '&';
            }
          }
        });
        url = url.slice(0, -1);

        return window.location.href = url;
      }
    </script>
@endpush