@extends('layouts.main')

@section('title', $seo_settings['index_page_title'] ?? 'MUR TV')
@section('keywords', $seo_settings['index_page_keywords'] ?? 'MUR TV')
@section('description', $seo_settings['index_page_description'] ?? 'MUR TV')
@push('meta')
    <meta property="og:title" content="{{ $seo_settings['index_page_title'] ?? 'MUR TV' }}" />
    <meta property="og:description" content="{{ $seo_settings['index_page_description'] ?? 'MUR TV' }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="{{ env('FB_CLIENT_ID') }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image:width" content="640">
    <meta property="og:image:height" content="480">
    @if (!empty($main_settings['opengraph_main_image']))
        <meta property="og:image" content="{{ asset($main_settings['opengraph_main_image']) }}" />
    @endif
@endpush

@section('content')

    @include('partials.top-filter-banner')

    @include('partials.recently-posts', ['posts' => $posts])

    <section class="fullwidth margin-bottom-0 fullwidth-50">
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <p><strong>Добро пожаловать на самую удобную межгосударственную бесплатную доску объявлений МУР.тв!</strong></p>
                    <p>На нашем сайте вы не только сможете <a href="https://mur.tv/" target="_blank">почитать интересные истории</a> о животных, узнать <a href="https://mur.tv/category/poleznosti/" target="_blank" rel="noopener">много полезного</a> об их содержании, получить заряд позитива от их<a href="https://mur.tv/category/krasivye-kartinki/" target="_blank" rel="noopener"> фото-приключений</a>, задать <a href="https://mur.tv/sovety-eksperta/" target="_blank" rel="noopener">вопрос ветеринару</a>, и даже разместить <a href="https://mur.tv/category/pets/" target="_blank" rel="noopener">собственную статью</a>, но и:</p>
                    <ol>
                        <li>Дать объявление о пристрое питомца в добрые руки (особенно актуально для приютских или найденных животных).</li>
                        <li>Выставить на продажу любые товары для животных.</li>
                        <li>Продать породистых питомцев.</li>
                        <li>Подать объявление о тех или иных событиях/акциях/ярмарках/выставках/благотворительных мероприятиях и прочем – для максимального привлечения людей.</li>
                        <li>Ознакомиться с объявлениями и бесплатно выбрать себе друга в семью.</li>
                        <li>Купить все необходимые вещи для ваших любимчиков (от корма и наполнителя – до одежды и домиков).</li>
                        <li>Написать объявление о потере или находке животного в вашем городе.</li>
                        <li>Приобрести понравившееся животное (однако обязательно уточняйте у заводчика все детали сделки и наличие необходимых бумаг/разрешений на подобную деятельность!).</li>
                    </ol>
                    <p>Пользоваться нашей Доской очень удобно – вы можете ограничить поиск нужной страной и областью, а при подаче объявлений потребуется самый минимум времени и усилий!</p>
                    <p><strong>Всё-в-одном для любителей животных – это о нашей Доске Мур.тв!</strong></p>
                    <p><strong>Присоединяйтесь к нашей большой семье - вместе мы сильнее! :)</strong></p>

                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <script>
      function submitFilterForm(e, form) {
        e.preventDefault();
        var url = form.attr('action') + '?';
        var notSendArray = [
          '',
          '0',
          0,
          null,
          undefined,
        ];
        form.find('[name]').each(function () {
          var el = $(this);
          // if (el.attr('type') !== 'hidden') {
            if (el.attr('type') === 'checkbox' || el.attr('type') === 'radio') {
              if (el.prop('checked')) {
                url += el.attr('name') + '=' + el.val() + '&';
              }
            } else if ($.inArray(el.val(), notSendArray) === -1) {
                url += el.attr('name') + '=' + el.val() + '&';
            }
          // }
        });
        url = url.slice(0, -1);

        return window.location.href = url;
      }
    </script>
@endpush