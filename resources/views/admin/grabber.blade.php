@extends('admin.layouts.common')

@section('title')
    Парсер постов из файла
@endsection

@section('content')
    @push('active_breadcrumb', 'парсер постов')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">Парсер постов</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                <form id="form" action="{{ route('admin.grabber.run') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="btn-group">
                            <button onclick="submitGrabberForm(event, $(this), 1)" type="button" class="btn btn-info">Спарсить 1 новый пост <i class="fa fa-spinner hidden"></i></button>
                            <button onclick="submitGrabberForm(event, $(this), 10)" type="button" class="btn btn-primary">Спарсить 10 новых постов <i class="fa fa-spinner hidden"></i></button>
                            <button onclick="submitGrabberForm(event, $(this), 50)" type="button" class="btn btn-warning">Спарсить 50 новых постов <i class="fa fa-spinner hidden"></i></button>
                        </div>
                    </div>
                    <div id="response-data"></div>

                </form>
            </div>
        </div>
    </div>
@stop
@push('scripts')
    <script>
        function submitGrabberForm(e, button, limit) {
          e.preventDefault();
          var form = $('#form');
          if (limit === undefined) {
            limit = 0;
          }
          $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: {
              limit: limit,
            },
            beforeSend: function () {
              showLoader(button);
              $('#response-data').html('');
            },
            success: function (response) {
              console.log(response);
              $('#response-data').html(response);
              showLoader(button);
            },
            error: function (errors) {
              console.log(errors);
              showLoader(button);
            },
          });
        }
    </script>
@endpush