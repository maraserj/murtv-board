@extends('admin.layouts.common')
@section('title') {{ isset($organizationType->id) ? 'Редактирование типа организации - ' . $organizationType->title : 'Содание типа организации' }} @endsection
@section('css')
    @parent
@stop

@section('scripts')
    @parent
@stop
@push('breadcrumbs')
    <li>
        <a href="{{ route('admin.organization-types.index') }}">Типы организаций</a>
    </li>
@endpush
@push('active_breadcrumb')
    {{ $organizationType->title ? $organizationType->title : 'Содание типа организации'}}
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($organizationType->id) ? 'Редактирование типа организации - ' . $organizationType->title : 'Содание типа организации' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($organizationType->id))
                    <form action="{{ route('admin.organization-types.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                @else
                    <form action="{{ route('admin.organization-types.update', ['id' => $organizationType->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                    {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <div class="form-group row @if($errors->has('title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Название</label>
                            <div class="col-md-10">
                                <input type="text" name="title" class="form-control" value="{{ old('title', $organizationType->title) }}">
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('title')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.organization-types.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

            </form>
            </div>
        </div>
    </div>
@stop