@extends('admin.layouts.common')

@section('title', 'Типы организаций')
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список типов организаций
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Список типов организаций</h6>
                    <div class="row">
                        <a href="{{ route('admin.organization-types.create') }}" class="pull-right btn btn-success">Добавить ноый тип</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <thead>
                            <tr class="active">
                                <th class="text-center">ID</th>
                                <th>Название</th>
                                <th>Создано</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($organizationTypes as $organizationType)
                                <tr>
                                    <td class="text-center">
                                        {{ $organizationType->id }}
                                    </td>
                                    <td>
                                        <b>{{ $organizationType->title }}</b>
                                    </td>
                                    <td>
                                        <b>{{ $organizationType->created_at->format('Y.m.d H:i') }}</b>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.organization-types.edit', ['id' => $organizationType->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('admin.organization-types.destroy', ['id' => $organizationType->id]) }}" method="post">
                                                {{ csrf_field() }}{{  method_field('DELETE') }}
                                                <button onclick="if(!confirm('Удалить?')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
