<ul class="navigation navigation-main navigation-accordion">
    <li class="navigation-header"><span>Пользователи</span></li>
    <li class="{{ Route::is('admin.user*') ? 'active' : '' }}">
        <a href="{{ route('admin.users.index') }}"><i class="icon-users2"></i> <span>Пользователи</span></a>
    </li>
    <li class="navigation-header"><span>Объявления</span></li>
    <li class="{{ Route::is('admin.posts*') ? 'active' : '' }}">
        <a href="{{ route('admin.posts.index') }}"><i class="icon-users2"></i> <span>Объявления</span></a>
    </li>
    <li class="{{ Route::is('admin.breed-types*') ? 'active' : '' }}">
        <a href="{{ route('admin.breed-types.index') }}"><i class="icon-bug2"></i> <span>Животные</span></a>
    </li>
    <li class="{{ Route::is('admin.breeds*') ? 'active' : '' }}">
        <a href="{{ route('admin.breeds.index') }}"><i class="icon-tux"></i> <span>Породы</span></a>
    </li>
    <li class="{{ Route::is('admin.colors*') ? 'active' : '' }}">
        <a href="{{ route('admin.colors.index') }}"><i class="icon-color-sampler"></i> <span>Расцветки</span></a>
    </li>
    <li class="{{ Route::is('admin.post-types*') ? 'active' : '' }}">
        <a href="{{ route('admin.post-types.index') }}"><i class="icon-database2"></i> <span>Типы объяв</span></a>
    </li>

    <li class="{{ Route::is('admin.categories*') ? 'active' : '' }}">
        <a href="{{ route('admin.categories.index') }}"><i class="icon-magazine"></i> <span>Категории продажи</span></a>
    </li>

    <li class="navigation-header"><span>Организации</span></li>
    <li class="{{ Route::is('admin.organization-types*') ? 'active' : '' }}">
        <a href="{{ route('admin.organization-types.index') }}"><i class="icon-home2"></i> <span>Типы организаций</span></a>
    </li>
    <li class="{{ Route::is('admin.organizations*') ? 'active' : '' }}">
        <a href="{{ route('admin.organizations.index') }}"><i class="icon-home"></i> <span>Организации</span></a>
    </li>
    {{--<li class="navigation-header"><span>Categories</span></li>--}}
    {{--<li class="{{ Route::is('admin.categories*') ? 'active' : '' }}">--}}
        {{--<a href="{{ route('admin.categories.index') }}"><i class="icon-color-sampler"></i> <span>Categories</span></a>--}}
    {{--</li>--}}

    <li class="navigation-header"><span>Локация</span></li>
    <li class="{{ Route::is('admin.countries*') ? 'active' : '' }}">
        <a href="{{ route('admin.countries.index') }}"><i class="icon-map"></i> <span>Страны</span></a>
    </li>
    <li class="{{ Route::is('admin.regions*') ? 'active' : '' }}">
        <a href="{{ route('admin.regions.index') }}"><i class="icon-map"></i> <span>Регионы</span></a>
    </li>
    <li class="{{ Route::is('admin.cities*') ? 'active' : '' }}">
        <a href="{{ route('admin.cities.index') }}"><i class="icon-map"></i> <span>Города</span></a>
    </li>

    <li class="navigation-header"><span>Статические страницы</span></li>
    <li class="{{ Route::is('admin.pages*') ? 'active' : '' }}">
        <a href="{{ route('admin.pages.index') }}"><i class="icon-newspaper"></i> <span>Статические страницы</span></a>
    </li>

    <li class="navigation-header"><span>Система</span></li>
    <li class="{{ Route::is('admin.robots*') ? 'active' : '' }}">
        <a href="{{ route('admin.robots.index') }}"><i class="icon-file-eye2"></i> <span>Robots.txt</span></a>
    </li>
    <li class="{{ Route::is('admin.settings*') ? 'active' : '' }}">
        <a href="{{ route('admin.settings.index') }}"><i class="icon-cogs"></i> <span>Настройки</span></a>
    </li>
    <li class="{{ Route::is('admin.grabber*') ? 'active' : '' }}">
        <a href="{{ route('admin.grabber.index') }}"><i class="icon-grab"></i> <span>Парсер постов</span></a>
    </li>
</ul>