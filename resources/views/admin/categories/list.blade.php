@extends('admin.layouts.common')

@section('title')Список категорий @endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script>
        $(function() {
            $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
                var obj = $(this),
                        data = state ? 1 : 0;
                $.ajax({
                    url: $(this).data('url'),
                    method: 'put',
                    data: {
                        active: data
                    },
                    dataType: 'json',
                    success: function(response) {
                        new PNotify({
                            text: response.message,
                            type: response.status
                        });
                    }
                });
            });
        });
    </script>
@endpush
@section('content')

    @push('active_breadcrumb')
        Список категорий
    @endpush
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Categories list</h6>
                    <a href="{{ route('admin.categories.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Create</a>
                </div>
                <div class="panel-body">Список категорий</div>
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr class="active">
                            <th class="text-center">#</th>
                            <th>Name</th>
                            <th>Image</th>
                            {{--<th>On main?</th>--}}
                            <th class="text-center">Status</th>
                            <th class="text-center">Created At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin"><small class="display-block text-size-small no-margin">{{$category->id}}</small></h6>
                                </td>
                                <td>
                                    <a href="{{ route('admin.categories.edit', ['id' => $category->id]) }}" class="display-inline-block text-default text-semibold letter-icon-title">{{$category->title}}</a>
                                </td>
                                <td>
                                    @if ($category->image)
                                        <img src="{{ asset($category->image) }}" width="120">
                                    @endif
                                </td>
                                {{--<td>--}}
                                    {{--@if ($category->show_on_main)--}}
                                        {{--<span class="label label-warning">Yes</span>--}}
                                    {{--@else--}}
                                        {{--<span class="label label-primary">No</span>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                <td class="text-center">
                                    <div class="checkbox checkbox-switch checkbox-switch-xs">
                                        <label>
                                            <input type="checkbox" class="switch" data-id="{{$category->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.categories.update', ['id' => $category->id]) }}" data-size="small" @if($category->active)checked="checked"@endif>
                                        </label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    {{$category->created_at}}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        {{--<a href="{{ route('admin.categories.show', ['category' => $category->slug]) }}" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>--}}
                                        <a href="{{ route('admin.categories.edit', ['id' => $category->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection