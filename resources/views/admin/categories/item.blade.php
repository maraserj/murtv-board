@extends('admin.layouts.common')

@section('title'){{ $item->id ? 'Редактирование категории' : 'Создание категории' }} @endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script>
        $(function() {
                $(".touchspin-no-mousewheel").TouchSpin({
                    mousewheel: false
                });
                $('.multiselect').multiselect({
                    enableFiltering: true,
                    templates: {
                        filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text" placeholder="Введите название"></li>'
                    },
                    onChange: function() {
                        $.uniform.update();
                    }
                });
                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });
                $(".file-styled-primary").uniform({
                    fileButtonClass: 'action btn bg-blue'
                });
                $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
                    var obj = $(this),
                        data = state ? 1 : 0;
                    $.ajax({
                        url: '{{ route('admin.categories.update', ['id' => $item->id]) }}',
                        method: 'put',
                        data: {
                            active: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            new PNotify({
                                text: response.message,
                                type: response.status
                            });
                        }
                    });
                });
            });
    </script>
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            @push('breadcrumbs')
                <li><a href="{{ route('admin.categories.index') }}">Список категорий</a></li>
            @endpush
            @push('active_breadcrumb')
                {{ $item->id ? $item->title : 'Создание категории' }}
            @endpush
                <div class="row">
                <div class="col-md-6 text-left">
                    <div class="checkbox checkbox-switch">
                        <label>
                            <input type="checkbox" class="switch" data-id="{{$item->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-size="small" @if($item->active == 1)checked="checked"@elseif(!isset($item->id)) checked="checked" @endif>
                        </label>
                    </div>
                </div>
                @if(isset($item->id))
                <div class="col-md-6 text-right">
                    <form action="{{ route('admin.categories.destroy', ['id' => $item->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit"><i class="fa fa-remove"></i></button>
                    </form>
                </div>
                    @endif
                </div>
            <h3 class="panel-title">{{isset($item->id)?$item->title:'Создание категории'}}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if (isset($item->id))
                    <form id="form" action="{{ route('admin.categories.update', ['id' => $item->id]) }}" method="post"  enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                @else
                    <form id="form" action="{{ route('admin.categories.store') }}" method="post"  enctype="multipart/form-data">
                @endif
                    {{ csrf_field() }}
                    @if(isset($item->id))<input type="hidden" name="id" value="{{$item->id}}">@endif
                    <fieldset class="content-group">
                        <legend class="text-bold">Общая информация</legend>
                        <div class="form-group row @if($errors->has('show_on_main'))has-error has-feedback @endif">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <label class="control-label col-md-4 text-semibold">Для покупки/продажи?</label>
                                    <div class="col-md-2">
                                        <div class="col-md-2 col-sm-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="is_for_sale" class="styled" value="1" {{old('is_for_sale', $item->is_for_sale) == 1?'checked':''}} @if(!isset($item->id) && !old('is_for_sale', $item->is_for_sale)) checked="checked" @endif>
                                                </label>
                                            </div>
                                            @if($errors->has('is_for_sale'))
                                                <div class="form-control-feedback">
                                                    <i class="icon-cancel-circle2"></i>
                                                </div>
                                                <span class="help-block">{{$errors->first('is_for_sale')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">--}}
                                    {{--<label class="control-label col-md-4 text-semibold">Show on main page?</label>--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<div class="col-md-2 col-sm-6">--}}
                                            {{--<div class="checkbox">--}}
                                                {{--<label>--}}
                                                    {{--<input type="checkbox" name="show_on_main" class="styled" value="1" {{old('show_on_main', $item->show_on_main) == 1?'checked':''}}>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                            {{--@if($errors->has('show_on_main'))--}}
                                                {{--<div class="form-control-feedback">--}}
                                                    {{--<i class="icon-cancel-circle2"></i>--}}
                                                {{--</div>--}}
                                                {{--<span class="help-block">{{$errors->first('show_on_main')}}</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">--}}
                                    {{--<label class="control-label col-md-4 text-semibold">Show in top menu?</label>--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<div class="col-md-2 col-sm-6">--}}
                                            {{--<div class="checkbox">--}}
                                                {{--<label>--}}
                                                    {{--<input type="checkbox" name="show_in_menu" class="styled" value="1" {{old('show_in_menu', $item->show_in_menu) == 1?'checked':''}}>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                            {{--@if($errors->has('show_in_menu'))--}}
                                                {{--<div class="form-control-feedback">--}}
                                                    {{--<i class="icon-cancel-circle2"></i>--}}
                                                {{--</div>--}}
                                                {{--<span class="help-block">{{$errors->first('show_in_menu')}}</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">--}}
                                    {{--<label class="control-label col-md-4 text-semibold">Big image on main?</label>--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<div class="col-md-2 col-sm-6">--}}
                                            {{--<div class="checkbox">--}}
                                                {{--<label>--}}
                                                    {{--<input type="checkbox" name="big_caption" class="styled" value="1" {{old('big_caption', $item->big_caption) == 1?'checked':''}}>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                            {{--@if($errors->has('big_caption'))--}}
                                                {{--<div class="form-control-feedback">--}}
                                                    {{--<i class="icon-cancel-circle2"></i>--}}
                                                {{--</div>--}}
                                                {{--<span class="help-block">{{$errors->first('big_caption')}}</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('title'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">Название</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="title" placeholder="Название категории" value="{{old('title', $item->title) }}" required>
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('parent_id'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">Родительская категория</label>
                            <div class="col-lg-10">
                                <div class="multi-select-full">
                                    <select class="form-control multiselect" name="parent_id">
                                        <option value="0" {{old('parent_id', 0) ? 'selected="selected"' : ''}}>Главная категория</option>
                                        @foreach($categories as $cat)
                                            <option value="{{ $cat->id }}" {{$cat->id == old('parent_id', $item->parent_id) ? 'selected="selected"' : ''}}>{{ $cat->title }}</option>
                                            @if(count($cat->child))
                                                @foreach($cat->child as $level_2)
                                                    <option value="{{ $level_2->id }}" {{$level_2->id == old('parent_id', $item->parent_id) ? 'selected="selected"' : ''}}>{{ $cat->title }} > {{ $level_2->title }}</option>
                                                    @if(count($level_2->child))
                                                        @foreach($level_2->child as $level_3)
                                                            <option value="{{ $level_3->id }}" {{$level_3->id == old('parent_id', $item->parent_id) ? 'selected="selected"' : ''}}>{{ $cat->title }} > {{ $level_2->title }} > {{ $level_3->title }}</option>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('parent_id'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('parent_id')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('slug'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">ЧПУ <span class="help-block">(link)</span></label>
                            <div class="col-lg-10">
                                    <input type="text" class="form-control" name="slug" placeholder="оставьте пустым, для автогенерации" value="{{old('slug', $item->slug) }}">
                                @if($errors->has('slug'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('slug')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('image'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">Картинка </label>
                            <div class="col-lg-10">
                                <input type="file" id="image" data-url="{{ route('admin.categories.delete_img') }}" data-src="{{ $item->image }}" data-id="{{ $item->id }}" class="form-control" name="image" value="{{old('image', $item->image) }}">
                                @if($errors->has('image'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('image')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('image'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">Или ссылка на картинку</label>
                            <div class="col-lg-10">
                                <input type="text" id="image_link" class="form-control" name="image" value="">
                                @if($errors->has('image'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('image')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="content-group">
                        <legend class="text-bold row">СЕО данные</legend>
                        <div class="form-group row @if($errors->has('seo_title'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">СЕО тайтл</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo_title" value="{{old('seo_title', $item->seo_title) }}">
                                @if($errors->has('seo_title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('seo_h1'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">СЕО H1</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo_h1" placeholder="" value="{{old('seo_h1', $item->seo_h1) }}">
                                @if($errors->has('seo_h1'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_h1')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('seo_description'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">СЕО дескрипшн</label>
                            <div class="col-lg-10">
                                <textarea rows="5" cols="5" name="seo_description" class="form-control" placeholder="">{{old('seo_description', $item->seo_description) }}</textarea>
                                @if($errors->has('seo_description'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_description')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('seo_keywords'))has-error has-feedback @endif">
                            <label class="control-label col-lg-2 text-semibold">СЕО кейвордс</label>
                            <div class="col-lg-10">
                                <textarea rows="5" cols="5" name="seo_keywords" class="form-control" placeholder="">{{old('seo_keywords', $item->seo_keywords) }}</textarea>
                                @if($errors->has('seo_keywords'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_keywords')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{route('admin.categories.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>

@stop