@extends('admin.layouts.common')
@section('title') {{ isset($breed->id) ? 'Редактирование породы' : 'Содание породы' }} @endsection
@section('css')
    @parent
@stop

@section('scripts')
    @parent
@stop
@push('breadcrumbs')
    <li>
        <a href="{{ route('admin.breeds.index') }}">Породы</a>
    </li>
@endpush
@push('active_breadcrumb')
    {{ $breed->title ? $breed->title : 'Содание породы'}}
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($breed->id) ? 'Редактирование породы' : 'Содание породы' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($breed->id))
                    <form action="{{ route('admin.breeds.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                @else
                    <form action="{{ route('admin.breeds.update', ['id' => $breed->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                    {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <legend class="text-bold">Main info</legend>
                        <div class="form-group row @if($errors->has('breed_type_id'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Вид</label>
                            <div class="col-md-10">
                                <select name="breed_type_id" class="form-control">
                                    @foreach($breedTypes as $breedType)
                                        <option value="{{ $breedType->id }}" {{old('breed_type_id', $breed->breed_type_id) == $breedType->id ? 'selected' : '' }}>{{ $breedType->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('breed_type_id'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('breed_type_id')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('order'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Порядок сортировки (Чем больше - тем выше в списке)</label>
                            <div class="col-md-10">
                                <input type="number" name="order" class="form-control" value="{{ old('order', $breed->order) }}">
                                @if($errors->has('order'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('order')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Название</label>
                            <div class="col-md-10">
                                <input type="text" name="title" class="form-control" value="{{ old('title', $breed->title) }}">
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('title')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.breeds.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

            </form>
            </div>
        </div>
    </div>
@stop