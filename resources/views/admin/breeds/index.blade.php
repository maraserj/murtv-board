@extends('admin.layouts.common')

@section('title', 'Породы')
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список пород
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Список пород</h6>
                    <a href="{{ route('admin.breeds.create') }}" class="pull-right btn btn-success">Добавить породу</a>
                </div>
                <div class="panel-body">
                    <!-- TAB NAVIGATION -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($breedTypes as $breedType)
                            <li class="{{ $loop->index == 0 ? 'active' : '' }}">
                                <a href="#tab{{ $breedType->id }}" role="tab" data-toggle="tab">{{ $breedType->title }} ({{ $breedType->breeds()->count() }})</a>
                            </li>
                        @endforeach
                    </ul>
                    <!-- TAB CONTENT -->
                    <div class="tab-content">
                        @foreach($breedTypes as $breedType)
                            <div class="{{ $loop->index == 0 ? 'active' : '' }} tab-pane fade in" id="tab{{ $breedType->id }}">
                                <div class="table-responsive">
                                    <table class="table text-nowrap">
                                        <thead>
                                        <tr class="active">
                                            <th class="text-center">ID</th>
                                            <th>Название</th>
                                            <th>К-тво объяв</th>
                                            <th>Создано</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($breedType->breeds as $breed)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $breed->id }}
                                                </td>
                                                <td>
                                                    <b>{{ $breed->title }}</b>
                                                </td>
                                                <td>
                                                    <b><span class="label label-info">{{ $breed->posts()->count() }}</span></b>
                                                </td>
                                                <td>
                                                    <b>{{ $breed->created_at->format('Y.m.d H:i') }}</b>
                                                </td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.breeds.edit', ['id' => $breed->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                                        <form action="{{ route('admin.breeds.destroy', ['id' => $breed->id]) }}" method="post">
                                                            {{ csrf_field() }}{{  method_field('DELETE') }}
                                                            <button onclick="if(!confirm('Удалить?')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
