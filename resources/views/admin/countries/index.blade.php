@extends('admin.layouts.common')

@section('title', 'Страны')
@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script>
      $(function() {
        $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
          var obj = $(this),
            data = state ? 1 : 0;
          $.ajax({
            url: $(this).data('url'),
            method: 'put',
            data: {
              status: data,
              changeStatus: true
            },
            dataType: 'json',
            // success: function(response) {
            //     new PNotify({
            //         text: response.message,
            //         type: response.status
            //     });
            // }
          });
        });
      });
    </script>
@endpush
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список стран
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="row">
                    <div class="panel-heading">
                        <h6 class="panel-title">Список стран</h6>
                        <a href="{{ route('admin.countries.create') }}" class="pull-right btn btn-success">Добавить страну</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                <tr class="active">
                                    <th class="text-center">ID</th>
                                    <th>Название</th>
                                    <th>Статус</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($countries as $country)
                                    <tr>
                                        <td class="text-center">
                                            {{ $country->id }}
                                        </td>
                                        <td>
                                            <b>{{ $country->name }}</b>
                                        </td>
                                        <td>
                                            <div class="checkbox checkbox-switch checkbox-switch-xs">
                                                <label>
                                                    <input type="checkbox" class="switch" data-id="{{$country->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.countries.update', ['id' => $country->id]) }}" data-size="small" @if($country->status)checked="checked"@endif>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.countries.edit', ['id' => $country->id]) }}"
                                                   class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                                <form action="{{ route('admin.countries.destroy', ['id' => $country->id]) }}"
                                                      method="post">
                                                    {{ csrf_field() }}{{  method_field('DELETE') }}
                                                    <button onclick="if(!confirm('Удалить? Внимание! Будут удалены все регионы и города')){return false;}"
                                                            class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            {{ $countries->appends(request()->all())->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
