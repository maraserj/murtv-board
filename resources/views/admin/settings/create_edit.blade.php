@extends('admin.layouts.common')

@section('title')
@if ($setting->id)
    Editing setting - {{ $setting->name }}
@else
    Creating new setting
@endif
@endsection
@push('breadcrumbs')
    <li><a href="{{ route('admin.settings.index') }}">Settings</a></li>
@endpush
@push('active_breadcrumb')
    @if ($setting->id)
        Editing setting - {{ $setting->name }}
    @else
        Creating new setting
    @endif
@endpush

@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/editors/summernote/summernote.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
    <script>
        function changeInputTypeSetting(selector) {
            var hiddenText = $('#hiddenText');
            var hiddenTextArea = $('#hiddenTextArea');
            var hiddenFile = $('#hiddenFile');
            var hiddenImage = $('#hiddenImage');
            if (selector.val() == '0') {
                hiddenFile.hide();
                hiddenText.hide();
                hiddenTextArea.hide();
                hiddenImage.hide();
            } else if (selector.val() == 'file') {
                hiddenFile.show();
                hiddenText.hide();
                hiddenTextArea.hide();
                hiddenImage.hide();
            } else if (selector.val() == 'image') {
                hiddenFile.hide();
                hiddenText.hide();
                hiddenTextArea.hide();
                hiddenImage.show();
            } else if (selector.val() == 'text') {
                hiddenFile.hide();
                hiddenText.show();
                hiddenTextArea.hide();
                hiddenImage.hide();
            } else if (selector.val() == 'textarea') {
                hiddenFile.hide();
                hiddenText.hide();
                hiddenTextArea.show();
                hiddenImage.hide();
            }
        }
        $(function() {
            $('#textarea').summernote();
            changeInputTypeSetting($('#type'));
            $('#type').change(function () {
                changeInputTypeSetting($('#type'));
            });

            $("#image").fileinput({
                browseIcon: "<i class=\"icon-image2\"></i> ",
                removeIcon: "<i class=\"icon-trash\"></i> ",
                uploadIcon: "<i class=\"icon-upload2\"></i> ",
                showCaption: false,
                showUpload: false,
                showRemove: false,
                @if (isset($setting->value) && $setting->type == 'image' && $setting->value != '')
                initialPreview: [
                    '{{ $setting->value }}'
                ],
                @endif
                initialPreviewAsData: true,
                allowedFileTypes: [
                    'image'
                ],
                allowedFileExtensions: [
                    'jpg', 'gif', 'png'
                ],
                initialPreviewConfig: [
                    {
                        showUpload: false,
                        showDelete: false,
                        showZoom: false
                    }
                ],
                maxFileSize: 2000,

            });
            $('.summernote').summernote({lang: 'ru-RU'});
            $(".touchspin-no-mousewheel").TouchSpin({
                mousewheel: false
            });
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });
            $(".file-styled-primary").uniform({
                fileButtonClass: 'action btn bg-blue'
            });
            $(function() {
                $(window).scroll(function() {
                    if($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
                    }
                    else {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
                    }
                });
            });
        });
    </script>
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if (isset($setting->id))
                <form id="form" action="{{ route('admin.settings.update', ['id' => $setting->id]) }}" method="post"  enctype="multipart/form-data">
                {{ method_field('PUT') }}
                @else
                <form id="form" action="{{ route('admin.settings.store') }}" method="post"  enctype="multipart/form-data">
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <legend class="text-bold">Main Info</legend>
                        <div class="form-group row">
                            <label class="control-label col-lg-2" for="group_id">Group of settings</label>
                            <div class="col-lg-10">
                                <select name="group_id" id="group_id" class="form-control">
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}" {{ old('group_id', $setting->group_id) == $group->id ? 'selected' : '' }}>{{ $group->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="control-label col-lg-2" for="name">Name</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $setting->name) }}">
                                @if ($errors->has('name'))
                                    <div class="help-block"><span>{{ $errors->first('name') }}</span></div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row {{ $errors->has('code') ? 'has-error' : '' }}">
                            <label class="control-label col-lg-2" for="code">Code</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="code" name="code" value="{{ old('code', $setting->code) }}">
                                @if ($errors->has('code'))
                                    <div class="help-block"><span>{{ $errors->first('code') }}</span></div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row {{ $errors->has('type') ? 'has-error' : '' }}">
                            <label class="control-label col-lg-2" for="type">Type</label>
                            <div class="col-lg-10">
                                <select name="type" id="type" class="form-control">
                                    <option value="0">Choose type</option>
                                    <option value="file" {{ old('type', $setting->type) == 'file' ? 'selected' : '' }}>File</option>
                                    <option value="image" {{ old('type', $setting->type) == 'image' ? 'selected' : '' }}>Image</option>
                                    <option value="text" {{ old('type', $setting->type) == 'text' ? 'selected' : '' }}>Text</option>
                                    <option value="textarea" {{ old('type', $setting->type) == 'textarea' ? 'selected' : '' }}>Text editor</option>
                                </select>
                                @if ($errors->has('type'))
                                    <div class="help-block"><span>{{ $errors->first('type') }}</span></div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="hiddenFile" style="display: none;">
                            <label class="control-label col-lg-2" for="file">File</label>
                            <div class="col-lg-10">
                                <input type="file" id="file" class="form-control" name="file" value="{{ (isset($setting->type) && $setting->type == 'file') ? (isset($setting->value) ? $setting->value : '') : '' }}">
                            </div>
                        </div>
                        <div class="form-group row" id="hiddenImage" style="display: none;">
                            <label class="control-label col-lg-2" for="image">Image</label>
                            <div class="col-lg-10">
                                <input type="file" id="image" class="form-control" name="image" accept="image/jpeg,image/png,image/jpg" value="{{ (isset($setting->type) && $setting->type == 'image') ? (isset($setting->value) ? $setting->value : '') : '' }}">

                            </div>
                            <label class="control-label col-lg-2" for="size">Size</label>
                            <div class="col-lg-2">
                                <input type="text" id="width" placeholder="width" class="form-control" name="width" value="{{ old('width') }}">
                            </div>
                            <div class="col-lg-2">
                                <input type="text" id="height" placeholder="height" class="form-control" name="height" value="{{ old('height') }}">
                            </div>

                        </div>

                        <div class="form-group row" id="hiddenText" style="display: none;">
                            <label class="control-label col-lg-2" for="text">Text</label>
                            <div class="col-lg-10">
                                <input type="text" id="text" class="form-control"
                                       name="text" value="{{ old('type', $setting->type) == 'text' ? old('value', $setting->value) : '' }}">
                            </div>
                        </div>
                        <div class="form-group row" id="hiddenTextArea" style="display: none;">
                            <label class="control-label col-lg-2" for="textarea">Text editor</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="textarea" rows="8" id="{{ request('group', $setting->group) == 'banners' ? '' : 'textarea' }}">{{ (isset($setting->type) && $setting->type == 'textarea') ? (isset($setting->value) ? $setting->value : '') : '' }}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{route('admin.categories.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>

@stop