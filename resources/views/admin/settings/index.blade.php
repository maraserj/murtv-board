@extends('admin.layouts.common')

@section('title')Settings list @endsection
@push('active_breadcrumb')
    Settings list
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/editable/editable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script>
        $(function() {

            $('.edit').editable({
                url: $(this).data('url'),
                ajaxOptions: {
                    type: 'put'
                },
                success: function(response, newValue) {
                    if(response.status == 'error') return response.msg;
                },
                error: function(data) {
                    var errors = data.responseJSON;
                    console.log(errors);
                    if(errors.value[0]) return errors.value[0];
                }
            });

            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function(html) {
                var switchery = new Switchery(html);
                html.onclick = function() {
                    change_status($(html).data('id'), html.checked);
                };
            });
        });
        function change_status(id, status) {
            var data = status ? 1 : 0;
            $.ajax({
{{--                url: '{{ route('admin.page_active') }}',--}}
                method: 'post',
                data: 'active=' + data + '&id=' + id,
                dataType: 'json',
                success: function(response) {
                    new PNotify({
                        text: response.message,
                        addclass: 'bg-' + response.status
                    });
                }
            });
        }
        function deleteSetting(e, form) {
            e.preventDefault();
            swal({
                title: 'Delete?',
                text: "Setting will delete forever",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete!',
                cancelButtonText: 'Cancel'
            }, function () {
                $.ajax({
                    url: form.attr('action'),
                    method: 'DELETE'
                }).done(function (data) {
                    swal(
                        data.message,
                        data.type
                    );
                    $('#setting-'+form.data('id')).html('');
                }).fail(function (data) {
                    swal(
                        data.message,
                        data.type
                    )

                });
            });
        }
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Settings<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <a href="{{ route('admin.settings.create') }}" class="btn bg-teal"><i class="fa fa-plus"></i> Create</a>
                </div>
            </div>
            <div class="panel-body">Settings management</div>

            <!-- TAB NAVIGATION -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach($groups as $group)
                    <li class="{{ $loop->first ? 'active' : '' }}"><a href="#tab{{ $group->id }}" role="tab" data-toggle="tab">{{ $group->name }}</a></li>
                @endforeach
            </ul>
            <!-- TAB CONTENT -->
            <div class="tab-content">
                @foreach($groups as $group)
                    <div class="{{ $loop->first ? 'active' : '' }} tab-pane fade in" id="tab{{ $group->id }}">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                <tr class="active">
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th class="text-center">Value</th>
                                    <th class="text-center">Date</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group->settings as $setting)
                                    <tr id="setting-{{ $setting->id }}">
                                        <td><h6 class="no-margin"><small class="display-block text-size-small no-margin">{{$setting->id}}</small></h6></td>
                                        <td>{{ $setting->name }}</td>
                                        <td><code>{{ $setting->code }}</code></td>
                                        <td>
                                            @if ($setting->type == 'image' && $setting->value != '')
                                                <img src="{{ asset($setting->value) }}" width="150">
                                            @elseif ($setting->type == 'textarea' && $setting->value != '')
                                                <span>{{ str_limit($setting->value, 150) }}</span>
                                            @else
                                                <span id="setting-edit-{{ $setting->id }}" class="edit" data-type="text" data-pk="{{ $setting->id }}" data-url="{{ route('admin.settings.update', ['id' => $setting->id]) }}">{{ (strlen(strip_tags($setting->value)) > 200) ? mb_substr(strip_tags($setting->value), 0, 200, 'UTF-8') : strip_tags($setting->value) }}</span>

                                            @endif
                                        </td>
                                        <td>{{ $setting->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.settings.edit', ['id' => $setting->id]) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('admin.settings.destroy', ['id' => $setting->id]) }}" id="delete-setting-{{ $setting->id }}" method="post" data-id="{{ $setting->id }}" onsubmit="deleteSetting(event, $('#delete-setting-{{ $setting->id }}'))">
                                                {{ csrf_field() }}{{ method_field('DELETE') }}
                                                <button type="submit" class="btn bst-sm btn-danger" data-toggle="tooltip" data-placement="top"
                                                        title="Удалить"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>
        function changeInputTypeSetting(selector) {
            var hiddenText = $('#hiddenText');
            var hiddenTextArea = $('#hiddenTextArea');
            var hiddenFile = $('#hiddenFile');
            var hiddenImage = $('#hiddenImage');
            if (selector.val() == '0') {
                hiddenFile.hide();
                hiddenText.hide();
                hiddenTextArea.hide();
                hiddenImage.hide();
            } else if (selector.val() == 'file') {
                hiddenFile.show();
                hiddenText.hide();
                hiddenTextArea.hide();
                hiddenImage.hide();
            } else if (selector.val() == 'image') {
                hiddenFile.hide();
                hiddenText.hide();
                hiddenTextArea.hide();
                hiddenImage.show();
            } else if (selector.val() == 'text') {
                hiddenFile.hide();
                hiddenText.show();
                hiddenTextArea.hide();
                hiddenImage.hide();
            } else if (selector.val() == 'textarea') {
                hiddenFile.hide();
                hiddenText.hide();
                hiddenTextArea.show();
                hiddenImage.hide();
            }
        }

    </script>
@endpush