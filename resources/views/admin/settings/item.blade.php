@extends('admin.layouts.common')

@section('title'){{ isset($post->id) ? 'Editing' : 'Creating' }} page @endsection
@section('description'){{ isset($post->id) ? 'Editing' : 'Creating' }} page @endsection

@section('styles')

@endsection
@section('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('back/assets/js/plugins/editors/summernote/summernote.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/ui/fab.min.js') !!}"></script>
    <script>
        function uploadImage(image) {
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                data: data,
                type: "POST",
                url: "{!! route('admin.blog_upload') !!}",
                cache: false,
                contentType: false,
                processData: false,
                success: function (url) {
                    var image = $('<img>').attr('src', url);
                    $('.summernote').summernote("insertNode", image[0]);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
        $(function () {
            $('.summernote').summernote({
                height: 600,
                callbacks: {
                    onImageUpload: function (image) {
                        uploadImage(image[0]);
                    }
                }
            });
            $(".touchspin-no-mousewheel").TouchSpin({
                mousewheel: false
            });
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });
            $(".file-styled-primary").uniform({
                fileButtonClass: 'action btn bg-blue'
            });
            $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
                var obj = $(this),
                    data = state ? 1 : 0;
                $.ajax({
                    url: '{!! route('admin.page_active') !!}',
                    method: 'post',
                    data: 'active=' + data + '&id=' + $(this).attr('data-id'),
                    dataType: 'json',
                    success: function (response) {
                        new PNotify({
                            text: response.message,
                            addclass: 'bg-' + response.status
                        });
                    }
                });
            });
            $(function () {
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
                    }
                    else {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
                    }
                });
            });
        });
    </script>
@stop
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            @if(isset($page->id))
            <div class="row">
                <div class="col-md-6 text-left">
                    <div class="checkbox checkbox-switch">
                        <label>
                            <input type="checkbox" class="switch" data-id="{{$page->id}}" data-on-text="Опубликовано"
                                   data-off-text="Выключено" data-on-color="success" data-off-color="default"
                                   data-size="small" @if($page->active == 1)checked="checked"@endif>
                        </label>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <form action="{!! route('admin.page_delete') !!}" method="post" id="delete">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="{{$page->id}}">
                    </form>
                </div>
            </div>
            @endif
            <h3 class="panel-title">{{ isset($post->id) ? 'Editing' : 'Creating' }} page</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                <form id="form" action="{!! route('admin.page_store') !!}" method="post"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    @if(isset($page->id))<input type="hidden" name="id" value="{{$page->id}}">@endif
                    <fieldset class="content-group">
                        <legend class="text-bold">Main info</legend>
                        <div class="form-group row @if($errors->has('base.footer'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Show in footer?</label>
                            <div class="col-md-5">
                                <div class="col-md-2 col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="base[footer]" class="styled"
                                                   value="1" {{old('base.footer')||$page->footer==1?'checked':''}}>
                                        </label>
                                    </div>
                                    @if($errors->has('base.footer'))
                                        <div class="form-control-feedback">
                                            <i class="icon-cancel-circle2"></i>
                                        </div>
                                        <span class="help-block">{{$errors->first('base.footer')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('info.title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="info[title]" placeholder=""
                                       value="{!! old('info.title') ? old('info.title') : $page->info->title !!}"
                                       required>
                                @if($errors->has('info.title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('info.title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('base.slug'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">URL <span
                                        class="help-block">(link)</span></label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="base[slug]"
                                       placeholder="auto generate if empty"
                                       value="{!! old('base.slug') ? old('base.slug') : $page->slug !!}">
                                @if($errors->has('base.slug'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('base.slug')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('template'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Template <span class="help-block">(pages/{name}.blade.php)</span></label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="base[template]" placeholder="custom"
                                       value="{!! old('base.template') ? old('base.template') : $page->template !!}">
                                @if($errors->has('template'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('template')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('info.description'))has-error has-feedback @endif">
                            <div class="col-md-12">
                                <textarea rows="5" cols="5" name="info[description]" class="form-control summernote"
                                          placeholder="">{!! old('info.description') ? old('info.description') : $page->info->description !!}</textarea>
                                @if($errors->has('info.description'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('info.description')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="content-group">
                        <legend class="text-bold row">Seo Params</legend>
                        <div class="form-group row @if($errors->has('info.seo_title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Seo Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="info[seo_title]" placeholder=""
                                       value="{!! old('info.seo_title') ? old('info.seo_title') : $page->info->seo_title !!}">
                                @if($errors->has('info.seo_title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('info.seo_title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('info.seo_h1'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Seo H1</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="info[seo_h1]" placeholder=""
                                       value="{!! old('info.seo_h1') ? old('info.seo_h1') : $page->info->seo_h1 !!}">
                                @if($errors->has('info.seo_h1'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('info.seo_h1')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('info.seo_description'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Seo Description</label>
                            <div class="col-md-10">
                                <textarea rows="5" cols="5" name="info[seo_description]" class="form-control"
                                          placeholder="">{!! old('info.seo_description') ? old('info.seo_description') : $page->info->seo_description !!}</textarea>
                                @if($errors->has('info.seo_description'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('info.seo_description')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('info.seo_keywords'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Seo Keywords</label>
                            <div class="col-md-10">
                                <textarea rows="5" cols="5" name="info[seo_keywords]" class="form-control"
                                          placeholder="">{!! old('info.seo_keywords') ? old('info.seo_keywords') : $page->info->seo_keywords !!}</textarea>
                                @if($errors->has('info.seo_keywords'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('info.seo_keywords')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon"
                               href="{!! route('admin.pages') !!}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon"
                               onclick="if(confirm('Уверены, что хотите удалить страницу?')){$('#delete').submit();}">
                                <i class="fab-icon-open icon-cross2"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon"
                               onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>

@stop