@extends('admin.layouts.common')

@section('title', 'Список организаций')
@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script>
        $(function() {
            $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
                var obj = $(this),
                    data = state ? 1 : 0;
                $.ajax({
                    url: $(this).data('url'),
                    method: 'put',
                    data: {
                        status: data,
                        changeStatus: true
                    },
                    dataType: 'json',
                    // success: function(response) {
                    //     new PNotify({
                    //         text: response.message,
                    //         type: response.status
                    //     });
                    // }
                });
            });
        });
    </script>
@endpush
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список организаций
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title"></h6>
                    <a href="{{ route('admin.organizations.create') }}" class="pull-right btn btn-success">Создать новую</a>
                </div>
                <div class="panel-body">Список организаций</div>
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr class="active">
                            <th class="text-center">#</th>
                            <th>Фото</th>
                            <th>Название</th>
                            <th>Автор</th>
                            <th>Создано</th>
                            <th class="text-center">Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($organizations as $organization)
                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin"><small class="display-block text-size-small no-margin">{{$organization->id}}</small></h6>
                                </td>
                                <td>
                                    @if ($organization->photos && $organization->photos->count())
                                        <img src="{{ asset($organization->photos->first()->path) }}" alt="{{ $organization->title }}" width="100">
                                    @else
                                        <img src="{{ asset('images/no-image.png') }}" alt="{{ $organization->title }}" width="100">
                                    @endif
                                </td>
                                <td>
                                    <a href="{{--{{ route('organizations.show', $organization->slug) }}--}}" target="_blank" class="display-inline-block text-default text-semibold letter-icon-title"><i class="icon-eye"></i> {{$organization->title}}</a>
                                </td>
                                <td>
                                    <div class="text-muted text-size-small"><a href="{{ route('admin.users.edit', $organization->user) }}">{{$organization->user->name}} <span class="icon-pencil"></span></a></div>
                                </td>
                                <td>
                                    <b>{{ $organization->created_at->format('Y.m.d H:i') }}</b>
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-switch checkbox-switch-xs">
                                        <label>
                                            <input type="checkbox" class="switch" data-id="{{$organization->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.organizations.update', ['id' => $organization->id]) }}" data-size="small" @if($organization->status)checked="checked"@endif>
                                        </label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.organizations.edit', ['id' => $organization->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                        <form action="{{ route('admin.organizations.destroy', ['id' => $organization->id]) }}" method="post">
                                            {{ csrf_field() }}{{  method_field('DELETE') }}
                                            <button onclick="if(!confirm('Удалить?')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $organizations->appends(request()->all())->render() }}
        </div>
    </div>
@endsection
