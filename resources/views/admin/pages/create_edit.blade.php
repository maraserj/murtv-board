@extends('admin.layouts.common')

@section('title')
    @if (isset($page->id))
        Editing page - {{ $page->title }}
    @else
        Creating page
    @endif
@endsection
@section('description')
    @if (isset($page->id))
        Editing page - {{ $page->title }}
    @else
        Creating page
    @endif
@endsection
@push('breadcrumbs')
    <li><a href="{{ route('admin.pages.index') }}">Static pages</a></li>
@endpush
@push('active_breadcrumb')
    @if (isset($page->id))
        Editing page - {{ $page->title }}
    @else
        Creating page
    @endif
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/ui/fab.min.js') }}"></script>
    <script>
        $(function() {
            $('.summernote').summernote({lang: 'ru-RU'});
            $(".touchspin-no-mousewheel").TouchSpin({
                mousewheel: false
            });
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });
            $(".file-styled-primary").uniform({
                fileButtonClass: 'action btn bg-blue'
            });
            $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
                var obj = $(this),
                    data = state ? 1 : 0;
                $.ajax({
                    url: $(this).data('url'),
                    method: 'put',
                    data: 'active=' + data,
                    dataType: 'json',
                    success: function (response) {
                        new PNotify({
                            text: response.message,
                            type: response.type
                        });
                    }
                });
            });
            $(function() {
                $(window).scroll(function() {
                    if($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
                    }
                    else {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
                    }
                });
            });
        });
    </script>
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            @if(isset($page->id))
                <div class="row">
                    <div class="col-md-6 text-left">
                        <div class="checkbox checkbox-switch">
                            <label>
                                <input type="checkbox" class="switch" data-id="{{$page->id}}" data-on-text="Опубликовано"
                                       data-off-text="Выключено" data-on-color="success" data-off-color="default" data-url="{{ route('admin.pages.update', ['id' => $page->id]) }}"
                                       data-size="small" {{ old('active', $page->active) == 1 ? 'checked' : '' }}>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <form action="{{ route('admin.pages.destroy', ['id' => $page->id]) }}" method="post" id="delete">
                            {{ csrf_field() }}{{ method_field('DELETE') }}
                            <input type="hidden" name="id" value="{{$page->id}}">
                        </form>
                    </div>
                </div>
            @endif
            <h3 class="panel-title">@yield('title')</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if (isset($page->id))
                    <form id="form" action="{{ route('admin.pages.update', ['id' => $page->id]) }}" method="post"  enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                @else
                    <form id="form" action="{{ route('admin.pages.store') }}" method="post"  enctype="multipart/form-data">
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <legend class="text-bold">Main info</legend>
                        <div class="form-group row @if($errors->has('footer'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Show in footer?</label>
                            <div class="col-md-5">
                                <div class="col-md-2 col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="footer" class="styled" value="1" {{old('footer', $page->footer) == 1?'checked':''}}>
                                        </label>
                                    </div>
                                    @if($errors->has('footer'))
                                        <div class="form-control-feedback">
                                            <i class="icon-cancel-circle2"></i>
                                        </div>
                                        <span class="help-block">{{$errors->first('footer')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="title" placeholder="" value="{{ old('title', $page->title) }}" required>
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('slug'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Pretty url <span class="help-block">(link)</span></label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="slug" placeholder="Autogenerate if empty" value="{{ old('slug', $page->slug) }}">
                                @if($errors->has('slug'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('slug')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('description'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Description</label>
                            <div class="col-md-10">
                                <textarea rows="5" cols="5" name="description" class="form-control summernote" placeholder="">{!! old('description', $page->description) !!}</textarea>
                                @if($errors->has('description'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('description')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="content-group">
                        <legend class="text-bold row">СЕО параметры</legend>
                        <div class="form-group row @if($errors->has('seo_title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">SEO title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="seo_title" placeholder="" value="{{ old('seo_title', $page->seo_title) }}">
                                @if($errors->has('seo_title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('seo_h1'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">SEO h1</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="seo_h1" placeholder="" value="{{ old('seo_h1', $page->seo_h1) }}">
                                @if($errors->has('seo_h1'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_h1')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('seo_description'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">SEO description</label>
                            <div class="col-md-10">
                                <textarea rows="5" cols="5" name="seo_description" class="form-control" placeholder="">{{ old('seo_description', $page->seo_description) }}</textarea>
                                @if($errors->has('seo_description'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_description')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('seo_keywords'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">SEO keywords</label>
                            <div class="col-md-10">
                                <textarea rows="5" cols="5" name="seo_keywords" class="form-control" placeholder="">{{ old('seo_keywords', $page->seo_keywords) }}</textarea>
                                @if($errors->has('seo_keywords'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('seo_keywords')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.pages.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>

@stop