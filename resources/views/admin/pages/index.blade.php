@extends('admin.layouts.common')

@section('title')Pages list @endsection

@section('styles')

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script>
        $(function() {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function(html) {
                var switchery = new Switchery(html);
                html.onclick = function() {
                    change_status($(html).data('id'), html.checked, $(html).data('url'));
                };
            });
        });
        function change_status(id, status, url) {
            var data = status ? 1 : 0;
            $.ajax({
                url: url,
                method: 'put',
                data: 'active=' + data,
                dataType: 'json',
                success: function(response) {
                    new PNotify({
                        text: response.message,
                        type: response.type
                    });
                }
            });
        }
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Page list<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <a href="{{ route('admin.pages.create') }}" class="btn bg-teal"><i class="fa fa-plus"></i> Create</a>
                </div>
            </div>
            <div class="panel-body">Pages management</div>
            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                        <tr class="active">
                            <th class="text-center">#</th>
                            <th>Name</th>
                            <th>Short text</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $page)
                    <tr>
                        <td class="text-center">
                            <h6 class="no-margin"><small class="display-block text-size-small no-margin">{{$page->id}}</small></h6>
                        </td>
                        <td>
                            <a href="{{ route('admin.pages.edit', ['id' => $page->id]) }}" target="_blank" class="display-inline-block text-default text-semibold letter-icon-title">{{$page->title}}</a>
                        </td>
                        <td>
                            <div class="text-muted text-size-small"><span class="display-block text-muted">{{ str_limit(strip_tags($page->description), 50) }}</span></div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-switchery switchery-sm">
                                <label>
                                    <input type="checkbox" class="switchery" data-url="{{ route('admin.pages.update', ['id' => $page->id]) }}" data-id="{{$page->id}}" @if($page->active)checked="checked"@endif>
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            {{$page->created_at}}
                        </td>
                        <td class="text-center">
                            <a href="{{ route('page.show', ['slug' => $page->slug]) }}" target="_blank" class="btn bg-slate btn-xs"><i class="fa fa-eye"></i><span class="hidden-sm hidden-xs"> Show</span></a>
                            <a href="{{ route('admin.pages.edit', ['id' => $page->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i><span class="hidden-sm hidden-xs"> Edit</span></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection