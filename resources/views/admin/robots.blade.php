@extends('admin.layouts.common')

@section('title')
    Editing robots.txt file
@endsection

    @push('scripts')
    <script type="text/javascript" src="{{ asset('back/assets/js/plugins/ui/fab.min.js') }}"></script>
    <script>
        $(function() {

            $(function() {
                $(window).scroll(function() {
                    if($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
                    }
                    else {
                        $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
                    }
                });
            });
        });
    </script>
@endpush
@section('content')
    @push('active_breadcrumb', 'Robots file editing')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">Robots file editing</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                <form id="form" action="{{ route('admin.robots.store') }}" method="post"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <legend class="text-bold row">Robots content</legend>
                        <div class="form-group row @if($errors->has('robots'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Put your code</label>
                            <div class="col-md-10">
                                <textarea rows="15" cols="5" name="robots" class="form-control" placeholder="" style="background-color: #272822;color: #F8F8F2;">{{ old('robots') ? old('robots') : $robots }}</textarea>
                                @if($errors->has('robots'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('robots')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-danger-400 btn-float btn-rounded btn-icon" onclick="if(confirm('Are you sure? This action will clear robots.txt')){$('textarea').html('');}">
                                <i class="fab-icon-open icon-cross2"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
@stop