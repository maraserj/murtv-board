@extends('admin.layouts.common')

@section('title', 'Города')
@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script>
      $(function() {
        $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
          var obj = $(this),
            data = state ? 1 : 0;
          $.ajax({
            url: $(this).data('url'),
            method: 'put',
            data: {
              status: data,
              changeStatus: true
            },
            dataType: 'json',
            // success: function(response) {
            //     new PNotify({
            //         text: response.message,
            //         type: response.status
            //     });
            // }
          });
        });
      });
    </script>
@endpush
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список городов
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="row">
                    <div class="panel-heading">
                        <h6 class="panel-title">Список городов</h6>
                    </div>
                    <div class="col-sm-12">
                        <form action="{{ route('admin.cities.index') }}" method="get" role="form">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select name="region_id" id="region_id" class="form-control">
                                        <option value="0"> Выбрать регион</option>
                                        @foreach($regions as $region)
                                            <option value="{{ $region->id }}" {{ request('region_id') == $region->id ? 'selected' : '' }}>{{ $region->name . ', ' . $region->country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" value="{{ request('q') }}" id="search_query" placeholder="Введите название города">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary">Искать</button>
                                <a href="{{ route('admin.cities.index') }}" class="btn btn-flat text-danger-800 btn-danger">Сбросить <i class="icon-close2"></i></a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ route('admin.cities.create') }}" class="pull-right btn btn-success">Добавить город</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                <tr class="active">
                                    <th class="text-center">ID</th>
                                    <th>Название</th>
                                    <th>Страна</th>
                                    <th>Область</th>
                                    <th>Статус</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cities as $city)
                                    <tr>
                                        <td class="text-center">
                                            {{ $city->id }}
                                        </td>
                                        <td>
                                            <b>{{ $city->name }}</b>
                                        </td>
                                        <td>
                                            <b>{!! $city->region && $city->region->country ? '<a href="'.route('admin.countries.edit', $city->region->country->id).'" target="_blank">'.$city->region->country->name.' <i class="icon-pencil"></i></a>' : 'Не указано' !!}</b>
                                        </td>
                                        <td>
                                            <b>{!! $city->region ? '<a href="'.route('admin.regions.edit', $city->region->id).'" target="_blank">'.$city->region->name.' <i class="icon-pencil"></i></a>' : 'Не указано' !!}</b>
                                        </td>
                                        <td>
                                            <div class="checkbox checkbox-switch checkbox-switch-xs">
                                                <label>
                                                    <input type="checkbox" class="switch" data-id="{{$city->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.cities.update', ['id' => $city->id]) }}" data-size="small" @if($city->status)checked="checked"@endif>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.cities.edit', ['id' => $city->id]) }}"
                                                   class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                                <form action="{{ route('admin.cities.destroy', ['id' => $city->id]) }}"
                                                      method="post">
                                                    {{ csrf_field() }}{{  method_field('DELETE') }}
                                                    <button onclick="if(!confirm('Удалить?')){return false;}"
                                                            class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            {{ $cities->appends(request()->all())->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
