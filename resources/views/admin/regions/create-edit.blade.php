@extends('admin.layouts.common')
@section('title') {{ isset($region->id) ? 'Редактирование региона' : 'Содание региона' }} @endsection
@section('css')
    @parent
@stop

@section('scripts')
    @parent
@stop
@push('breadcrumbs')
    <li>
        <a href="{{ route('admin.regions.index') }}">Страны</a>
    </li>
@endpush
@push('active_breadcrumb')
    {{ $region->name ? $region->name : 'Содание региона'}}
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($region->id) ? 'Редактирование региона' : 'Содание региона' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($region->id))
                    <form action="{{ route('admin.regions.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                @else
                    <form action="{{ route('admin.regions.update', ['id' => $region->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <div class="form-group row @if($errors->has('name'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Название</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class="form-control" value="{{ old('name', $region->name) }}">
                                @if($errors->has('name'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('country_id'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Страна</label>
                            <div class="col-md-10">
                                <select name="country_id" class="form-control">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{old('country_id', $region->country_id) == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('country_id'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('country_id')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('slug'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Транслит (оставьте пустым для автогенерации)</label>
                            <div class="col-md-10">
                                <input type="text" name="slug" class="form-control" value="{{ old('slug', $region->slug) }}">
                                @if($errors->has('slug'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('slug')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('status'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Опубликовано?</label>
                            <div class="col-md-10">
                                <input type="checkbox" name="status" class="styled" value="1" {{ old('status', $region->status) == 1 ? 'checked' : '' }}>
                                @if($errors->has('status'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('status')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.regions.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

                </form>
            </div>
        </div>
    </div>
@stop