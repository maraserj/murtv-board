@extends('admin.layouts.common')

@section('title', 'Регионы')
@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script>
      $(function() {
        $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
          var obj = $(this),
            data = state ? 1 : 0;
          $.ajax({
            url: $(this).data('url'),
            method: 'put',
            data: {
              status: data,
              changeStatus: true
            },
            dataType: 'json',
            // success: function(response) {
            //     new PNotify({
            //         text: response.message,
            //         type: response.status
            //     });
            // }
          });
        });
      });
    </script>
@endpush
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список регионов
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="row">
                    <div class="panel-heading">
                        <h6 class="panel-title">Список регионов</h6>
                    </div>
                    <div class="col-sm-12">
                        <form action="{{ route('admin.regions.index') }}" method="get" role="form">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select name="country_id" id="country_id" class="form-control">
                                        <option value="0"> Выбрать страну</option>
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}" {{ request('country_id') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" value="{{ request('q') }}" id="search_query" placeholder="Введите название страны">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary">Искать</button>
                                <a href="{{ route('admin.regions.index') }}" class="btn btn-flat text-danger-800 btn-danger">Сбросить <i class="icon-close2"></i></a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ route('admin.regions.create') }}" class="pull-right btn btn-success">Добавить регион</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                <tr class="active">
                                    <th class="text-center">ID</th>
                                    <th>Название</th>
                                    <th>Страна</th>
                                    <th>Статус</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($regions as $region)
                                    <tr>
                                        <td class="text-center">
                                            {{ $region->id }}
                                        </td>
                                        <td>
                                            <b>{{ $region->name }}</b>
                                        </td>
                                        <td>
                                            <b>{!! $region->country ? '<a href="'.route('admin.countries.edit', $region->country->id).'" target="_blank">'.$region->country->name.' <i class="icon-pencil"></i></a>' : 'Не указано' !!}</b>
                                        </td>
                                        <td>
                                            <div class="checkbox checkbox-switch checkbox-switch-xs">
                                                <label>
                                                    <input type="checkbox" class="switch" data-id="{{$region->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.regions.update', ['id' => $region->id]) }}" data-size="small" @if($region->status)checked="checked"@endif>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.regions.edit', ['id' => $region->id]) }}"
                                                   class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                                <form action="{{ route('admin.regions.destroy', ['id' => $region->id]) }}"
                                                      method="post">
                                                    {{ csrf_field() }}{{  method_field('DELETE') }}
                                                    <button onclick="if(!confirm('Удалить? Внимание! Будут удалены города')){return false;}"
                                                            class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            {{ $regions->appends(request()->all())->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
