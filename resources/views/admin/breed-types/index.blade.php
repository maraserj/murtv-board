@extends('admin.layouts.common')

@section('title', 'Животные')
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список животных
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Список животных</h6>
                    <div class="row">
                        <a href="{{ route('admin.breed-types.create') }}" class="pull-right btn btn-success">Добавить животное</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <thead>
                            <tr class="active">
                                <th class="text-center">ID</th>
                                <th>Название</th>
                                <th>Картинка</th>
                                <th>Кт-во пород</th>
                                <th>Создано</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($breedTypes as $breedType)
                                <tr>
                                    <td class="text-center">
                                        {{ $breedType->id }}
                                    </td>
                                    <td>
                                        <b>{{ $breedType->title }}</b>
                                    </td>
                                    <td>
                                        <img src="{{ asset($breedType->image ? $breedType->image : 'images/noavatar.png') }}" width="80">
                                    </td>
                                    <td>
                                        <b><span class="label label-info">{{ $breedType->breeds_count }}</span></b>
                                    </td>

                                    <td>
                                        <b>{{ $breedType->created_at->format('Y.m.d H:i') }}</b>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.breed-types.edit', ['id' => $breedType->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('admin.breed-types.destroy', ['id' => $breedType->id]) }}" method="post">
                                                {{ csrf_field() }}{{  method_field('DELETE') }}
                                                <button onclick="if(!confirm('Удалить? Удалятся все породы данного вида.')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
