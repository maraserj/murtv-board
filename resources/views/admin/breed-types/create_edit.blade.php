@extends('admin.layouts.common')
@section('title') {{ isset($breedType->id) ? 'Редактирование животного - ' . $breedType->title : 'Содание животного' }} @endsection
@section('css')
    @parent
@stop

@section('scripts')
    @parent
@stop
@push('breadcrumbs')
    <li>
        <a href="{{ route('admin.breed-types.index') }}">Животные</a>
    </li>
@endpush
@push('active_breadcrumb')
    {{ $breedType->title ? $breedType->title : 'Содание животного'}}
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($breedType->id) ? 'Редактирование животного - ' . $breedType->title : 'Содание животного' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($breedType->id))
                    <form action="{{ route('admin.breed-types.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                @else
                    <form action="{{ route('admin.breed-types.update', ['id' => $breedType->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                    {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <div class="form-group row @if($errors->has('title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Название</label>
                            <div class="col-md-10">
                                <input type="text" name="title" class="form-control" value="{{ old('title', $breedType->title) }}">
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('order'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Порядок сортировки (Чем больше - тем выше в списке)</label>
                            <div class="col-md-10">
                                <input type="number" name="order" class="form-control" value="{{ old('order', $breedType->order) }}">
                                @if($errors->has('order'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('order')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('image'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Изображение</label>
                            <div class="col-md-10">
                                <input type="file" name="image" class="form-control" data-src="{{ old('image', $breedType->image) }}" id="image" value="{{ old('image', $breedType->image) }}">
                                @if($errors->has('image'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('image')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.breed-types.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

            </form>
            </div>
        </div>
    </div>
@stop