<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Admin-panel')</title>
    <meta name="description" content="@yield('title')">

    <link href="//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900"  rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/icons/icomoon/styles.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/bootstrap.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/core.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/components.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('back/assets/css/colors.css') }}"  rel="stylesheet" type="text/css">
    @stack('css')

</head>
<body>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{route('admin.index')}}" >
            <img src="{{ asset('back/assets/images/logo_light.png') }}"  alt="">
            <div> </div>
        </a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        <div class="navbar-right">
            <p class="navbar-text"><a href="{{ route('index') }}" class="bg-success-400 label">Главная</a></p>
            <p class="navbar-text"><a class="bg-danger-400 label" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Выход</a></p>
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ auth()->user()->avatar }}"  alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</div>
<div class="page-container">
    <div class="page-content">
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="{{ auth()->user()->avatar }}" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">{{Auth::user()->name}}</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;{{Auth::user()->email}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        @include('admin.includes.menu')
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">@yield('title')</span></h4>
                    </div>
                </div>
                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('admin.index') }}" ><i class="icon-home2 position-left"></i> Главная</a></li>
                        @stack('breadcrumbs')
                        <li class="active">@stack('active_breadcrumb')</li>
                    </ul>
                </div>
            </div>
            <div class="content">

                <div id="app">
                    @yield('content')
                </div>

                <div class="footer text-muted">
                    &copy; {{date('Y')}}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ mix('js/app.js') }}" ></script>

<script type="text/javascript" src="{{ asset('back/assets/js/core/app.js') }}" ></script>

<script type="text/javascript" src="{{ asset('back/assets/js/core/libraries/jquery.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('back/assets/js/core/libraries/bootstrap.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('back/assets/js/plugins/loaders/pace.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('back/assets/js/plugins/loaders/blockui.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('back/assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('back/assets/js/plugins/uploaders/fileinput.min.js') }}"></script>


<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $(function() {
      @if (session('success'))
      new PNotify({
        text: '{{ session('success') }}',
        type: 'success'
      });
      @endif
      @if (session('error'))
      new PNotify({
        text: '{{ session('error') }}',
        type: 'danger'
      });
      @endif

      @if (!request()->is('*admin/settings*'))
      $('#image').fileinput({
        showUpload: false,
        showDelete: false,
        previewFileType: 'any',
        overwriteInitial: true,
        initialPreviewAsData: true, // defaults markup
          @if (request()->is('*/edit'))
          initialPreview: [
            '{{ env('APP_URL') }}'+$('#image').data('src'),
          ],
        initialPreviewConfig: [
          {url: $('#image').data('url'), key: $('#image').data('id')},
        ],
          @endif

      });
      @endif
  });

  function showLoader(selector) {
    selector.children('.fa').toggleClass('fa-spin');
    selector.children('.fa').toggleClass('hidden');
    selector.toggleClass('disabled');
  }

</script>

@stack('scripts')

</body>
</html>
