@extends('admin.layouts.common')
@section('title') {{ isset($user->id) ? 'Редактирование пользователя' : 'Содание пользователя' }} @endsection
@section('css')
    @parent
@stop

@section('scripts')
    @parent
@stop
@section('breadcrumbs')
    <li>
        <a href="{{ route('admin.users.index') }}">Пользователи</a>
    </li>
    <li class="active">
        {{ $user->name ? $user->name : 'Содание пользователя'}}
    </li>
@stop
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($user->id) ? 'Редактирование пользователя' : 'Содание пользователя' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($user->id))
                    <form action="{{ route('admin.users.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                @else
                    <form action="{{ route('admin.users.update', ['id' => $user->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                    {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <legend class="text-bold">Main info</legend>
                        <div class="form-group row @if($errors->has('role'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Role</label>
                            <div class="col-md-10">
                                <select name="role" class="form-control">
                                    <option value="admin" {{old('role', $user->role) == 'admin' ? 'selected' : '' }}>Admin</option>
                                    <option value="user" {{old('role', $user->role) == 'user' ? 'selected' : '' }}>User</option>
                                </select>
                                @if($errors->has('role'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('role')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('name'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Name</label>
                            <div class="col-md-10">
                                <input type="text" name="name" class="form-control" value="{{ old('name', $user->name) }}">
                                @if($errors->has('name'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('email'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Email</label>
                            <div class="col-md-10">
                                <input type="email" name="email" class="form-control" value="{{ old('email', $user->email) }}">
                                @if($errors->has('email'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('phone'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Phone</label>
                            <div class="col-md-10">
                                <input type="text" name="phone" class="form-control" value="{{ old('phone', $user->phone) }}">
                                @if($errors->has('phone'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('password'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Password</label>
                            <div class="col-md-10">
                                <input type="text" name="password" class="form-control" value="" placeholder="leave empty if not need change">
                                @if($errors->has('password'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('password')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.users.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

            </form>
            </div>
        </div>
    </div>
@stop