@extends('admin.layouts.common')

@section('title', 'User list')
@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script>
        $(function() {
            $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
                var obj = $(this),
                    data = state ? 1 : 0;
                $.ajax({
                    url: $(this).data('url'),
                    method: 'put',
                    data: {
                        is_confirmed: data,
                        changeStatus: true
                    },
                    dataType: 'json',
                    success: function(response) {
                        new PNotify({
                            text: response.message,
                            type: response.status
                        });
                    }
                });
            });
        });
    </script>
@endpush
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            User list
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">User list</h6>
                    <a href="{{ route('admin.users.create') }}" class="pull-right btn btn-success">Create</a>
                </div>
                <div class="panel-body">Management User list</div>
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr class="active">
                            <th class="text-center">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th class="text-center">Confirmed</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin"><small class="display-block text-size-small no-margin">{{$user->id}}</small></h6>
                                </td>
                                <td>
                                    <a class="display-inline-block text-default text-semibold letter-icon-title">{{$user->name}}</a>
                                </td>
                                <td>
                                    <div class="text-muted text-size-small"><span class="display-block text-muted"><i class="fa fa-envelope"></i> {{$user->email}}</span></div>
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-switch checkbox-switch-xs">
                                        <label>
                                            <input type="checkbox" class="switch" data-id="{{$user->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.users.update', ['id' => $user->id]) }}" data-size="small" @if($user->is_confirmed)checked="checked"@endif>
                                        </label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                        <form action="{{ route('admin.users.destroy', ['id' => $user->id]) }}" method="post">
                                            {{ csrf_field() }}{{  method_field('DELETE') }}
                                            <button onclick="if(!confirm('Are you sure?')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
