@extends('admin.layouts.common')

@section('title', 'Расцветки животных')
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список расцветок
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Список расцветок</h6>
                    <div class="row">
                        <a href="{{ route('admin.colors.create') }}" class="pull-right btn btn-success">Добавить расцветку</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <thead>
                            <tr class="active">
                                <th class="text-center">ID</th>
                                <th>Название</th>
                                <th>Создано</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($colors as $color)
                                <tr>
                                    <td class="text-center">
                                        {{ $color->id }}
                                    </td>
                                    <td>
                                        <b>{{ $color->title }}</b>
                                    </td>
                                    <td>
                                        <b>{{ $color->created_at->format('Y.m.d H:i') }}</b>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.colors.edit', ['id' => $color->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('admin.colors.destroy', ['id' => $color->id]) }}" method="post">
                                                {{ csrf_field() }}{{  method_field('DELETE') }}
                                                <button onclick="if(!confirm('Удалить?')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
