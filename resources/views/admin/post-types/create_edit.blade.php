@extends('admin.layouts.common')
@section('title') {{ isset($postType->id) ? 'Тип объявления - ' . $postType->title : 'Содание типа объявления' }} @endsection
@section('css')
    @parent
@stop

@section('scripts')
    @parent
@stop
@push('breadcrumbs')
    <li>
        <a href="{{ route('admin.post-types.index') }}">Типы объявлений</a>
    </li>
@endpush
@push('active_breadcrumb')
    {{ $postType->title ? $postType->title : 'Содание типа объявления'}}
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($postType->id) ? 'Тип объявления - ' . $postType->title : 'Содание типа объявления' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($postType->id))
                    <form action="{{ route('admin.post-types.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                @else
                    <form action="{{ route('admin.post-types.update', ['id' => $postType->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="form">
                    {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        <div class="form-group row @if($errors->has('title'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Название</label>
                            <div class="col-md-10">
                                <input type="text" name="title" class="form-control" value="{{ old('title', $postType->title) }}">
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('title')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('code'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Код, например <code>founded</code> - <code>Найденный</code></label>
                            <div class="col-md-10">
                                <input type="text" name="code" class="form-control" value="{{ old('code', $postType->code) }}">
                                @if($errors->has('code'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('code')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row @if($errors->has('description'))has-error has-feedback @endif">
                            <label class="control-label col-md-2 text-semibold">Описание</label>
                            <div class="col-md-10">
                                <input type="text" name="description" class="form-control" value="{{ old('description', $postType->description) }}">
                                @if($errors->has('description'))
                                    <div class="form-control-feedback">
                                        <i class="icon-cancel-circle2"></i>
                                    </div>
                                    <span class="help-block">{{$errors->first('description')}}</span>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.post-types.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

            </form>
            </div>
        </div>
    </div>
@stop