@extends('admin.layouts.common')

@section('title', 'Типы объявлений')
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Типы объявлений
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Типы объявлений</h6>
                    <div class="row">
                        <a href="{{ route('admin.post-types.create') }}" class="pull-right btn btn-success">Добавить тип объявления</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <thead>
                            <tr class="active">
                                <th class="text-center">ID</th>
                                <th>Название</th>
                                <th>К-тво объяв</th>
                                <th>Создано</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($postTypes as $postType)
                                <tr>
                                    <td class="text-center">
                                        {{ $postType->id }}
                                    </td>
                                    <td>
                                        <b>{{ $postType->title }}</b>
                                    </td>
                                    <td>
                                        <b><span class="label label-info"> {{ $postType->posts_count }}</span></b>
                                    </td>
                                    <td>
                                        <b>{{ $postType->created_at->format('Y.m.d H:i') }}</b>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.post-types.edit', ['id' => $postType->id]) }}" class="btn bg-slate btn-xs"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('admin.post-types.destroy', ['id' => $postType->id]) }}" method="post">
                                                {{ csrf_field() }}{{  method_field('DELETE') }}
                                                <button onclick="if(!confirm('Удалить?')){return false;}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
