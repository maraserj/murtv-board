@extends('admin.layouts.common')
@section('title') {{ isset($post->id) ? 'Редактирование объявления' : 'Создание объявления' }} @endsection
@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <style>
        .holder.holder-textarea {
            position: absolute;
            margin: 20px 22px;
            color: #A3A3A3;
            cursor: auto;
            font-size: 11pt;
            z-index: 1;
        }
        .holder.holder-input {
            position: absolute;
            margin: 12px 21px;
            color: #A3A3A3;
            cursor: auto;
            font-size: 11pt;
            z-index: 1;
        }

    </style>
@endpush
@push('breadcrumbs')
    <li>
        <a href="{{ route('admin.posts.index') }}">Объявления</a>
    </li>
@endpush
@push('active_breadcrumb')
    {{ $post->title ? $post->title : 'Создание объявления'}}
@endpush
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h3 class="panel-title">{{ isset($post->id) ? 'Редактирование объявления' : 'Создание объявления' }}</h3>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                @if(empty($post->id))
                    <form action="{{ route('admin.posts.store') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" onsubmit="submitPostForm(event, $(this))" id="form">
                @else
                    <form action="{{ route('admin.posts.update', ['id' => $post->id]) }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" onsubmit="submitPostForm(event, $(this))" id="form">
                    {{ method_field('PUT') }}
                @endif
                    {{ csrf_field() }}
                    <fieldset class="content-group">
                        @if($errors->any())
                            <div class="notification error large">
                                <h4 class="text-center">Исправьте ошибки и повторите попытку</h4>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <!-- Section -->
                        {{--<h3>Общая информация</h3>--}}
                        <div class="submit-section" style="padding-top: 0; border-top: 0;">

                            <!-- Row -->
                            <div class="row with-forms">

                                <div class="row col-sm-12">
                                    <admin-category-and-price :categories="{{ $categories }}" :post="{{ json_encode($post) }}"></admin-category-and-price>
                                </div>
                                <div class="row col-sm-12">
                                    <div class="col-md-3 form-group {{ $errors->has('post_type_id') ? 'has-error' : '' }} form">
                                        {{--<label for="post_type_id">Тип <span class="required">*</span><i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i></label>--}}
                                        <div class="disabled-first-option">
                                            <select name="post_type_id" class="form-control"  id="post_type_id">
                                                <option value="0">Тип *</option>
                                                @foreach($post_types as $post_type)
                                                    <option {{ old('post_type_id', $post->post_type_id) == $post_type->id ? 'selected' : '' }} value="{{ $post_type->id }}">{{ $post_type->title }}</option>
                                                @endforeach
                                            </select>
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i>
                                            </div>
                                        </div>
                                        @if ($errors->has('post_type_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('post_type_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 form-group {{ $errors->has('animal_id') ? 'has-error' : '' }} form">
                                        {{--<label for="type">Тип <span class="required">*</span><i class="tip" data-tip-content="Если вы нашли питомца и хотите его пристроить — выберите «Ищет дом». Если это ваш домашний питомец и вы не хотите его пристраивать — выберите «Мой домашний»."></i></label>--}}
                                        <div class="disabled-first-option">
                                            <input class="search-field form-control {{ $post->animal_id ? 'disabled' : '' }}" type="text" id="animal_id" value="{{ old('animal_id', $post->animal_id) }}" name="animal_id" placeholder="Animal ID" {{ $post->animal_id ? 'disabled' : '' }}>
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Введите Animal ID, если он есть. Если нету - он будет сгенерирован автоматически"></i>
                                            </div>
                                        </div>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                @if($organizations->count())
                                    <div class="row col-sm-12">
                                        <div class="col-md-3 form-group {{ $errors->has('organization_id') ? 'has-error' : '' }} form">
                                            <label for="organization_id">Организация <i class="tip" data-tip-content="Если этот питомец из вашей организации - можете выбрать ее и в поиске люди смогут найти питомца по огранизации"></i></label>
                                            <div class="disabled-first-option">
                                                <select name="organization_id" id="organization_id" class="form-control">
                                                    <option value="0">Выберите из списка</option>
                                                    @foreach($organizations as $organization)
                                                        <option {{ old('organization_id', $post->organization_id) == $organization->id ? 'selected' : '' }} value="{{ $organization->id }}">{{ $organization->title . (empty($organization->organizationType) ? '' : ', ' . $organization->organizationType->title) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if ($errors->has('organization_id'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('organization_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                @endif

                                <div class="row col-sm-12">
                                    <!-- Title -->
                                    <div class="col-md-9 margin-bottom-10 form-group {{ $errors->has('title') ? 'has-error' : '' }} form">
                                        {{--<label for="post_title">Заголовок <span class="required">*</span> <i class="tip" data-tip-content="Краткое описание питомца"></i></label>--}}
                                        <div class="holder holder-input" style="{{ old('title', $post->title) ? 'display:none;' : '' }}">Краткое описание <span class="required">*</span></div>
                                        <input class="search-field form-control" type="text" id="post_title" value="{{ old('title', $post->title) }}" name="title" required maxlength="90" style="margin-bottom: 0;">
                                        <em>Осталось знаков: <b><span id="title_count_chars">{{ 90 - strlen(old('title', $post->title)) }}</span></b></em>
                                        <br>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                        @endif
                                        <div class="info-tip">
                                            <i class="tip" data-tip-content="Краткое описание объявления"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-sm-12 toggled-visible">
                                    <div class="col-md-2 form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                                        <label for="gender">Пол</label>
                                        <div class="disabled-first-option">
                                            <select name="gender" class="form-control" id="gender" required>
                                                <option value="0">Пол *</option>
                                                <option value="male" {{ old('gender', $post->gender) == 'male' ? 'selected' : '' }}>Мальчик</option>
                                                <option value="female" {{ old('gender', $post->gender) == 'female' ? 'selected' : '' }}>Девочка</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    @php
                                        $breed_data = $post;
                                        if (empty($post->id)) {
                                            $breed_data->breed_type_id = old('breed_type_id');
                                            $breed_data->breed_id = old('breed_id');
                                        }
                                    @endphp
                                    <admin-breeds-and-breeds-type :post="{{ $breed_data }}" :breed_types="{{ $breed_types }}"></admin-breeds-and-breeds-type>
                                    <div class="col-md-2">
                                        <div class="form-group {{ $errors->has('color_id') ? 'has-error' : '' }} form">
                                            <label for="post_color_id">Окрас</label>
                                            <select name="color_id" id="post_color_id" class="search-field form-control">
                                                <option value="0">Окрас</option>
                                                @foreach($colors as $color)
                                                    <option value="{{ $color->id }}" {{ old('color_id', $post->color_id) == $color->id ? 'selected' : '' }}>{{ $color->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-sm-12 toggled-visible">
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} form">
                                            {{--<label for="post_name">Кличка</label>--}}
                                            <input class="search-field form-control" type="text" id="post_name" value="{{ old('name', $post->name) }}" name="name" placeholder="Кличка">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('size') ? 'has-error' : '' }} form">
                                            {{--<label for="post_size">Рост в холке (в см)  <i class="tip" data-tip-content="Примерный рост питомца"></i></label>--}}
                                            <input class="search-field form-control" type="text" id="post_size" value="{{ old('size', $post->size) }}" name="size" placeholder="Рост в холке (в см)">
                                            @if ($errors->has('size'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('size') }}</strong>
                                            </span>
                                            @endif
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Примерный рост питомца в холке (в см). Например: 25"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('birthday') ? 'has-error' : '' }} form">
                                            {{--<label for="post_birthday">Дата рождения <i class="tip" data-tip-content="Если вы нашли питомца и не знаете дату рождения, укажите приблизительный возраст."></i></label>--}}
                                            <input class="search-field form-control" type="date" id="post_birthday" max="{{ date('Y-m-d') }}" value="{{ old('birthday', $post->birthday) }}" name="birthday" placeholder="Дата рождения">
                                            @if ($errors->has('birthday'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                            @endif
                                            <div class="info-tip">
                                                <i class="tip" data-tip-content="Если вы нашли питомца и не знаете дату рождения, укажите приблизительный возраст."></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row col-sm-12">
                                    <div class="col-sm-12">
                                        @if ($errors->has('breed_type_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('breed_type_id') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('breed_id'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('breed_id') }}</strong>
                                            </span>
                                        @endif

                                        @if ($errors->has('color_id'))
                                            <span class="help-block text-danger">
                                            <strong>{{ $errors->first('color_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    @php
                                        $location_data = $post;
                                        if (empty($post->id)) {
                                            $location_data->country_id = old('country_id');
                                            $location_data->region_id = old('region_id');
                                            $location_data->city_id = old('city_id');
                                        }
                                    @endphp
                                    <country-region-city :post="{{ $location_data }}" :countries="{{ $countries }}"></country-region-city>

                                    {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }} form">--}}
                                    {{--<label for="post_address">Адрес</label>--}}
                                    {{--<input class="search-field" type="text" id="post_address" value="{{ old('address', $post->address) }}" name="address" placeholder="Улица и т.д.">--}}
                                    {{--@if ($errors->has('address'))--}}
                                    {{--<span class="help-block text-danger">--}}
                                    {{--<strong>{{ $errors->first('address') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-sm-12">
                                        @if ($errors->has('country_id'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('region_id'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('region_id') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('city_id'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="row col-sm-12 toggled-visible">
                                    <!-- Checkboxes -->
                                    <div class="col-md-9 checkboxes in-row margin-bottom-20">

                                        <input id="check-2" type="checkbox" name="guarded" value="1" {{ old('guarded', $post->guarded) ? 'checked' : '' }}>
                                        <label for="check-2">Привит</label>

                                        <input id="check-3" type="checkbox" name="sterilized" value="1" {{ old('sterilized', $post->sterilized) ? 'checked' : ''  }}>
                                        <label for="check-3">Стерилизован</label>

                                        <input id="check-4" type="checkbox" name="ill" value="1" {{ old('ill', $post->ill) ? 'checked' : ''  }}>
                                        <label for="check-4">Требует лечения</label>

                                        <input id="check-5" type="checkbox" name="deadly" value="1" {{ old('deadly', $post->deadly) ? 'checked' : ''  }}>
                                        <label for="check-5">Тяжелое состояние</label>

                                    </div>
                                    <!-- Checkboxes / End -->
                                </div>

                                <div class="row col-sm-9">
                                    <div class="col-md-6 toggled-visible checkboxes in-row margin-bottom-20 form-group {{ $errors->has('featured') ? 'has-error' : '' }}">
                                        <input id="check-32" type="checkbox" name="featured" value="1" {{ old('featured', $post->featured) ? 'checked' : '' }}>
                                        <label for="check-32">Особенное животное <i class="tip" data-tip-content="Поставьте галочку, если у питомца есть особенности например косоглазие, хроническое заболевание или инвалидность."></i></label>
                                    </div>
                                    <div class="col-md-12 form-group {{ $errors->has('description') ? 'has-error' : '' }} form">
                                        <div class="holder holder-textarea" style="{{ old('description', $post->description) ? 'display:none;' : '' }}">Описание <span class="required">*</span></div>
                                        <textarea class="WYSIWYG form-control" name="description" cols="40" rows="3" id="description" spellcheck="true" required minlength="20" maxlength="2500">{{ old('description', $post->description) }}</textarea>
                                        <em>Осталось знаков: <b><span id="description_count_chars">{{ 2500 - strlen(old('title', $post->title)) }}</span></b></em>
                                        @if ($errors->has('description'))
                                            <span class="help-block text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row col-sm-12">
                                    <div class="col-md-9">
                                        <h3><i class="fa fa-plus-circle" aria-hidden="true" style="color: #fa5b0f;"></i> Фото</h3>
                                        <em>К-тво фото должно быть не больше 10</em>
                                        <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                                            <input type="file" name="images[]" class="image-upload" multiple>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- Row / End -->

                        </div>
                        <!-- Section / End -->
                    </fieldset>

                    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-left">
                        <li style="display: inline">
                            <a class="fab-menu-btn btn btn-default btn-float btn-rounded btn-icon" href="{{ route('admin.posts.index') }}">
                                <i class="fab-icon-open icon-arrow-left8"></i>
                            </a>
                        </li>
                        <li style="display: inline">
                            <a class="fab-menu-btn btn bg-primary-400 btn-float btn-rounded btn-icon" id="submitFormButton" onclick="$('#form').submit();">
                                <i class="fab-icon-open icon-floppy-disk"></i>
                            </a>
                        </li>
                    </ul>

            </form>
            </div>
        </div>
    </div>
@stop
@push('scripts')

    <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>

    <script type="text/javascript" src="{{ asset('scripts/magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/rangeSlider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/sticky-kit.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/masonry.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/mmenu.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/tooltips.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/chosen.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('scripts/dropzone.js') }}"></script>--}}
    <script>
      var submitButton = $('#submitFormButton');
      function toggleTypeSelect() {
        if ($('#check-13').prop('checked')) {
          $('#post_type_id').closest('.form-group').fadeOut();
          $('#animal_id').closest('.form-group').fadeOut();
          $('.toggled-visible').fadeOut();
        } else {
          $('#post_type_id').closest('.form-group').fadeIn();
          $('#animal_id').closest('.form-group').fadeIn();
          $('.toggled-visible').fadeIn();
        }
      }

      $(function () {

        flatpickr.localize(flatpickr.l10ns.ru);
        flatpickr("#post_birthday", {
          maxDate: new Date(),
        });
        flatpickr.l10ns.default.firstDayOfWeek = 1; // Monday
        toggleTypeSelect();
        $('#check-13').change(function () {
          toggleTypeSelect();
        });
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        var btns = '<button type="button" class="kv-cust-btn btn btn-kv btn-secondary btn-default pull-left" onclick="makeAsMain($(this))" title="Сделать фото главным"{dataKey}>' +
          '<i class="fa fa-floppy-o"></i>' +
          '</button>';
        $(".image-upload").fileinput({
          otherActionButtons: btns,
          language: "ru",
          previewFileType: 'any',
          showCaption: false,
          showZoom: false,
          validateInitialCount: true,
          maxFileCount: 10,
          allowedFileExtensions: ["jpg", "png", "gif"],
          allowedFileTypes: ["image"],
            @if ($post->photos()->count())
            initialPreview: [
                @foreach ($post->photos as $image)
                  '{{ asset($image->path) }}',
                @endforeach
            ],
          initialPreviewAsData: true,
          initialPreviewConfig: [
          @foreach ($post->photos as $image)
            {
              caption: "{{ $image->file_name }}",
              width: "120px",
              ajaxDeleteSettings: '',
              key: '{{ $image->id }}',
              removeClass: 'btn btn-default removePhoto',
              showDelete: true,
                  @if ($image->is_main)
                  frameClass: 'is-main-photo',
                  @endif
            },
          @endforeach
          ],
            @endif
            fileActionSettings: {
              showZoom: false,
              showDrag: false,
            },
          deleteExtraData: {id: '{{ !empty($post->id) ? $post->id : null }}'},
          deleteUrl: "{{ route('profile.posts.delete_image') }}",
          ajaxDeleteSettings: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          showUpload: false,
          showRemove: false,
          overwriteInitial: false,
          initialCaption: "Choose photos for apartment",
        });
      });
      function submitPostForm(event, form) {
        event.preventDefault();
        var formData = form.serializeArray();
        formData.push({name: 'validate', value: true});
        showLoader(submitButton);

        $.ajax({
          url: form.attr('action'),
          method: form.attr('method'),
          data: formData,
          beforeSend: function () {
            form.find('[style*="border-color: rgb(169, 68, 66);"]').each(function () {
              $(this).css('border-color', '#e0e0e0');
              removeHelpValidationBlock($(this));
            });
          },
          success: function (response) {
            showLoader(submitButton);
            form.attr('onsubmit', '');
            form.submit();
          },
          error: function (errors) {
            showLoader(submitButton);
            toastr.error('Исправьте ошибки и повторите попытку');
            errors = errors.responseJSON;
            var i = 0,
              scrollTo = null;
            _.forEach(errors.errors, function (value, key) {
              var el = form.find('[name='+key+']');
              i++;
              if (i > 0 && i <= 1) {
                scrollTo = el;
              }
              addHelpValidationBlock(el, value);
              if (scrollTo) {
                $('html, body').animate({
                  scrollTop: scrollTo.offset().top - 250
                }, 200);
              }
            });
          }
        })
      }
      function makeAsMain(el) {
        console.log(el.data('key'));
        $.ajax({
          url: '{{ route('profile.posts.photos.mark_as_main', ['post_id' => $post->id]) }}',
          method: 'post',
          data: {
            id: el.data('key'),
          },
          success: function (response) {
            if (response.status === 'ok') {
              location.reload();
            }
          },
          error: function (error) {
            console.log(error);
          }
        });
      }
      function removeHelpValidationBlock(el) {
        el.closest('.form-group').find('.help-block').remove();
      }
      function addHelpValidationBlock(el, text) {
        el.css('border-color', '#a94442');
        var html = '<span class="help-block text-danger">' +
          '<strong>'+text+'</strong>' +
          '</span>';

        el.closest('.form-group').append(html);
      }

      function showLoader(selector) {
        selector.children('.fa').toggleClass('fa-spin');
        selector.children('.fa').toggleClass('hidden');
        selector.toggleClass('disabled');
      }
    </script>
@endpush