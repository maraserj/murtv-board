@extends('admin.layouts.common')

@section('title', 'Список объявлений')
@push('scripts')
    <script type="text/javascript" src="{!! asset('back/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
    <script>
        $(function() {
            $('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
                var obj = $(this),
                    data = state ? 1 : 0;
                $.ajax({
                    url: $(this).data('url'),
                    method: 'put',
                    data: {
                        status: data,
                        changeStatus: true
                    },
                    dataType: 'json',
                    // success: function(response) {
                    //     new PNotify({
                    //         text: response.message,
                    //         type: response.status
                    //     });
                    // }
                });
            });
        });
    </script>
@endpush
@section('content')
    <div class="row">
        @push('active_breadcrumb')
            Список объявлений
        @endpush
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title"></h6>
                    <a href="{{ route('admin.posts.create') }}" class="pull-right btn btn-success">Создать новое</a>
                    @if (request('is_from_parser'))
                        <a href="{{ route('admin.posts.index') }}" class="btn btn-info">Все объявления</a>
                    @else
                        <a href="{{ route('admin.posts.index', ['is_from_parser' => true]) }}" class=" btn btn-warning">Объявления из парсера</a>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="row">
                        Всего найдено объявлений: <b>{{ $posts->total() }}</b>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr class="active">
                            <th width="5%" class="text-center">#</th>
                            <th width="10%">Фото</th>
                            <th>Заголовок</th>
                            <th>Автор</th>
                            <th width="10%">Создано</th>
                            <th width="10%" class="text-center">Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr class="{{ !$post->status ? 'danger' : '' }}">
                                <td class="text-center">
                                    <h6 class="no-margin"><small class="display-block text-size-small no-margin">{{$post->id}}</small></h6>
                                </td>
                                <td>
                                    @if ($post->photos && $post->photos->count())
                                        <img src="{{ asset($post->photos->first()->path) }}" alt="{{ $post->title }}" width="70">
                                    @else
                                        <img src="{{ asset('images/no-image.png') }}" alt="{{ $post->title }}" width="70">
                                    @endif
                                </td>
                                <td  width="10%">
                                    <a href="{{ route('posts.show', $post->slug) }}" title="{{ $post->title }}" target="_blank" class="display-inline-block text-default text-semibold letter-icon-title"><i class="icon-eye"></i> {{ str_limit($post->title, 20) }}</a>
                                </td>
                                <td>
                                    <div class="text-muted text-size-small"><a href="{{ route('admin.users.edit', $post->user) }}">{{$post->user->name}} <span class="icon-pencil"></span></a></div>
                                </td>
                                <td>
                                    <b>{{ $post->created_at->format('Y.m.d H:i') }}</b>
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-switch checkbox-switch-xs">
                                        <label>
                                            <input type="checkbox" class="switch" data-id="{{$post->id}}" data-on-text="On" data-off-text="Off" data-on-color="success" data-off-color="default" data-url="{{ route('admin.posts.update', ['id' => $post->id]) }}" data-size="small" @if($post->status)checked="checked"@endif>
                                        </label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.posts.edit', ['id' => $post->id]) }}" class="btn bg-slate"><i class="fa fa-pencil"></i></a>
                                        <button onclick="if(confirm('Удалить?')){$('#delete-form-{{ $post->id }}').submit()}" class="btn btn-danger"><i class="fa fa-trash"></i></button>

                                        <form id="delete-form-{{ $post->id }}" action="{{ route('admin.posts.destroy', ['id' => $post->id]) }}" style="display:none;" method="post">
                                            {{ csrf_field() }}{{  method_field('DELETE') }}
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $posts->appends(request()->all())->render() }}
        </div>
    </div>
@endsection
