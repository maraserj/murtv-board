<?php

/**
 * @var \Illuminate\Routing\Router $router
 */
$router->auth();
$router->get('auth/{provider}/sign', 'Auth\SocialSignController@redirectToProvider')->name('auth.social_sign');
$router->get('auth/{provider}/callback', 'Auth\SocialSignController@handleProviderCallback')->name('auth.social_sign_callback');

$router->get('/', 'HomeController@index')->name('index');
$router->get('home', 'HomeController@index')->name('home');
$router->get('countries/regions', 'RegionController@getRegionsByCountry')->name('countries.get_regions');
$router->get('regions/cities', 'CityController@getRegionsByCountry')->name('regions.get_cities');
$router->get('cities', 'CityController@index')->name('cities.index');
$router->get('cities/{id}', 'CityController@show')->name('city.show');
$router->get('breeds', 'BreedController@index')->name('breeds.index');

$router->get('pets', 'PostController@index')->name('posts.index');
$router->get('pets/{slug}', 'PostController@show')->name('posts.show');


//$router->get('shop', 'PostController@productsIndex')->name('posts.index');
//$router->get('shop/{category}/{slug}', 'PostController@showProduct')->name('posts.show');

$router->post('pets/send-message', 'PostController@submitMessage')->name('posts.send_message');

$router->get('organizations', 'OrganizationController@index')->name('organizations.index');
$router->get('organizations/{slug}', 'OrganizationController@show')->name('organizations.show');

Route::get('sitemap', function () {
    return redirect()->route('sitemap_xml', 302);
});
Route::get('sitemap.xml', 'HomeController@sitemap')->name('sitemap_xml');


$router->group(['middleware' => 'auth', 'prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Profile'], function () use ($router) {
    $router->get('/', 'IndexController@index')->name('index');
    $router->put('update', 'IndexController@update')->name('update');
    $router->get('favourites', 'FavouritesController@index')->name('favourites.index');
    $router->post('posts/add-to-favourites', 'PostController@addToFavourites')->name('posts.add_to_favourites');
    $router->delete('posts/remove-from-favourites', 'PostController@removeFromFavourites')->name('posts.remove_from_favourites');
    $router->post('posts/delete-image', 'PostController@deleteImage')->name('posts.delete_image');
    $router->post('posts/photos/mark-as-main', 'PostController@markPhotoAsMain')->name('posts.photos.mark_as_main');
    $router->put('posts/change-home-status/{id}', 'PostController@changeHomeStatus')->name('posts.change_home_status');
    $router->resource('posts', 'PostController');

    $router->post('organizations/delete-image', 'OrganizationController@deleteImage')->name('organizations.delete_image');
    $router->resource('organizations', 'OrganizationController');
});
$router->group(['middleware' => 'can:admin', 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () use ($router) {
    $router->get('/', 'IndexController@index')->name('index');
    $router->get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    $router->get('grabber', 'IndexController@grabber')->name('grabber.index');
    $router->post('grabber', 'IndexController@grabberRun')->name('grabber.run');

    $router->get('robots', 'IndexController@robots')->name('robots.index');
    $router->post('robots', 'IndexController@storeRobots')->name('robots.store');

    $router->post('categories/delete-img', 'CategoriesController@deleteImg')->name('categories.delete_img');
    $router->resource('categories', 'CategoriesController');
    $router->resource('users', 'UsersController');
    $router->resource('settings', 'SettingsController');
    $router->resource('pages', 'PageController');
    $router->resource('posts', 'PostController');
    $router->resource('breeds', 'BreedController');
    $router->resource('breed-types', 'BreedTypeController');
    $router->resource('post-types', 'PostTypeController');
    $router->resource('colors', 'ColorController');
    $router->resource('countries', 'CountryController');
    $router->resource('regions', 'RegionController');
    $router->resource('cities', 'CityController');
    $router->resource('organizations', 'OrganizationController');
    $router->resource('organization-types', 'OrganizationTypeController');

});

$router->get('{slug}', 'HomeController@showStaticPage')->name('page.show');
