<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $http = new \GuzzleHttp\Client(['base_uri' => '//mur.tv']);
//    $file = Storage::disk('public')->get('foundpets-history.dat');
    $file = $http->get('/assets/foundpets-history.dat')->getBody()->getContents();
    $articles = explode("\n", trim($file));
    dump(count($articles));
    foreach ($articles as $article) {
        dd(explode('|', $article));
    }
    dd();
})->describe('Display an inspiring quote');
