<?php
namespace App\Services;
use App\Models\Setting;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;
class ImageUpload
{
    protected $settingPath = 'settings';
    protected $categoriesPath = 'categories';
    protected $channelsPath = 'channels';
    protected $avatarsPath = 'avatars';

    public $baseFolder = 'other';
    public $withWatermark = false;

    /**
     * @param UploadedFile $uploadedImage
     * @param null $width
     * @param null $height
     * @param string $method
     * @param string $oldImage
     * @return null|string
     */
    public function save($uploadedImage = null, $width = null, $height = null, $method = 'save', $oldImage = '')
    {
        if ($method == 'update') {
            Storage::disk('public')->delete(str_replace('/storage', '', $oldImage));
        }
        $image = null;

        if ($uploadedImage instanceof UploadedFile) {
            $name = $uploadedImage->getClientOriginalName();
            $extension = $uploadedImage->getClientOriginalExtension();
        } else {
            $extension = last(explode('.', $uploadedImage));
            $name = last(str_replace($extension, '', explode('/', $uploadedImage)));
        }

        $path = 'images/'. $this->baseFolder;

        if (Str::contains($this->baseFolder, 'posts')) {
            $this->withWatermark = true;
        }
        $name = Str::replaceLast('.', '', $name);

        if (!Storage::disk('public')->exists($path)) Storage::disk('public')->makeDirectory($path);
        if ((isset($width) && $width != '') && (isset($height) && $height != '')) {
            $imageName = $this->makeImageName($name, $extension, $width, $height);
            $image = Image::make($uploadedImage)
                ->orientate()
                ->resize((int)$width, (int)$height);
        } elseif (isset($width) && $width != '') {
            $imageName = $this->makeImageName($name, $extension, $width);
            $image = Image::make($uploadedImage)
                ->orientate()
                ->widen((int)$width);
        } else {
            $imageName = $this->makeImageName($name, $extension);
            $image = Image::make($uploadedImage)->orientate();
        }

        if ($this->withWatermark) {
            $watermarkImage = Setting::getSetting('default_watermark') ?: Setting::getSetting('site_logo');
            if ($watermarkImage) {
                $watermark = storage_path(str_replace('/storage', '/app/public', $watermarkImage));
                $watermarkInstance = Image::make($watermark);
                $watermarkInstance->orientate();
                $image->insert($watermarkInstance);
            }
        }

        $image->save(storage_path('app/public/' . $path . '/' . $imageName), 80);
        return $image ? '/storage/' . $path .'/' . $imageName : null;
    }

    public function deleteImage($path)
    {
        $path = str_replace('/storage', '', $path);
        return Storage::disk('public')->delete($path);
    }



    /**
     * @param $name
     * @param $extension
     * @param null $width
     * @param null $height
     * @return string
     */
    private function makeImageName($name, $extension, $width = null, $height = null): string
    {
        if ($width && $height) {
            return str_replace($extension, '', str_slug($name)) . '_' . time() . '_'. $width . 'x'. $height . '.' . $extension;
        } elseif ($width) {
            return str_replace($extension, '', str_slug($name)) . '_' . time() . '_'. $width . '.' . $extension;
        } else {
            return str_replace($extension, '', str_slug($name)) . '_' . time() . '_original.' . $extension;
        }
    }

}