<?php
namespace App\Services\PostsGrabber;

use App\Models\City;
use App\Models\Photo;
use App\Models\Post;
use App\Repositories\PostRepository;
use App\Services\ImageUpload;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Str;
use Storage;

class PostsGrabberService implements PostsGrabber
{
    /**
     * @var PostRepository
     */
    private $postRepository;
    private $httpClient;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
        $this->httpClient = new HttpClient();
    }

    public function runGrabber(int $offset = null, int $limit = null)
    {
        $posts = $this->getPosts();
        dump('Posts count:' . $posts->count());
        $counter = 0;

        $newPostsIds = [];

        foreach ($posts as $key => $post) {
            if ($offset && $key < $offset) {
                continue;
            }
            if ($limit && $counter >= $limit) {
                dump('limited!');
                break;
            }

            $city_id = 197053;
            $city = null;
            try {
                $city = $this->getCityRecordForPost($post[2] ?? '');
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
            try {
                if ($city) {
                    if ($city->name == 'Москва') {
                        $city_id = 197053;
                    } else {
                        $city_id = $city->id;
                    }
                }

                $breed = $this->getBreedType($post[4] ?? '');

                $creatingData = [
                    'user_id'        => 1,
                    'is_from_parser' => true,
                    'breed_type_id'  => $breed['breed_type_id'],
                    'breed_id'       => $breed['breed_id'],
                    'city_id'        => $city_id,
                    'title'          => $post[3] ?? null,
                    'description'    => $post[4] ?? null,
                    'status'         => false,
                    'post_type_id'   => 3,
                    'original_url'   => $post[0] ?? null,
                ];
                $validator = validator($creatingData, [
                    'title'        => 'required',
                    'description'  => 'required',
                    'original_url' => 'required',
                ]);

                if ($validator->fails()) {
                    dump('Validation error in parser', [$validator->getMessageBag()]);
                    continue;
                }

                $newPost = $this->savePost(['original_url' => $post[0] ?? null], $creatingData);

                if ($newPost && $newPost->wasRecentlyCreated) {
                    $newPostsIds[] = $newPost->id;
                    $counter++;
                    dump('New Post ID: ' . $newPost->id);
                    $this->savePostImage($newPost, $post[1] ?? null);
                }
            } catch (\Exception $exception) {
                dump('unknown error:', $exception->getMessage());
                info('Posts grabbing error: ', [$exception->getMessage()]);
            }
        }

        return ['new_posts_count' => $counter, 'new_posts_ids' => $newPostsIds];
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        $response = $this->httpClient->get('//mur.tv/assets/foundpets-history.dat');

        $posts = explode("\n", $response->getBody()->getContents());

        $postsCollection = collect($posts)
            ->mapWithKeys(function ($item, $key) {
                return [$key => explode('|', $item)];
            })
            ->reject(function ($item) {
                return !empty($item) && empty($item[0]);
            });

        return $postsCollection;
    }

    /**
     * @param Post $post
     * @param $url_or_image
     * @param bool $watermark
     * @return void
     */
    public function savePostImage(Post $post, $url_or_image, bool $watermark = true)
    {
        if (is_null($url_or_image)) {
            return null;
        }

        $this->postRepository->createImage($post, [$url_or_image]);
    }

    /**
     * @param string $city_name
     * @return mixed
     */
    public function getCityRecordForPost(string $city_name = null)
    {
        $city = new City();

        $city = $city
            ->where('name', $city_name)
            ->orWhere('name', 'like',  $city_name . '%')
            ->first();

        return $city;
    }

    /**
     * @param array $attributes
     * @param array $values
     * @return Post
     */
    public function savePost(array $attributes = [], array $values = [])
    {
        return $this->postRepository->firstOrCreate($attributes, $values);
    }

    /**
     * @param string $type
     * @return array
     */
    private function getBreedType($type = '')
    {
        $dogType = Str::contains($type, ['пес', 'собак', 'щенок']);
        $catType = Str::contains($type, ['кот', 'кошк']);

        if ($dogType) {
            return ['breed_type_id' => 2, 'breed_id' => 146];
        } elseif ($catType) {
            return ['breed_type_id' => 1, 'breed_id' => 145];
        }

        return ['breed_type_id' => 4, 'breed_id' => 144];
    }
}