<?php
namespace App\Services\PostsGrabber;


use App\Models\City;
use App\Models\Post;

interface PostsGrabber
{
    /**
     * @param int|null $offset
     * @param int|null $limit
     * @return mixed
     */
    public function runGrabber(int $offset = null, int $limit = null);

    /**
     * @return mixed
     */
    public function getPosts();

    /**
     * @param array $attributes
     * @param array $values
     * @return Post
     */
    public function savePost(array $attributes = [], array $values = []);

    /**
     * @param Post $post
     * @param $url_or_image
     * @param bool $watermark
     * @return string|void
     */
    public function savePostImage(Post $post, $url_or_image, bool $watermark = true);

    /**
     * @param string $city_name
     * @return City|null
     */
    public function getCityRecordForPost(string $city_name = null);
}