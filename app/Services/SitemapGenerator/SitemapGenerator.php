<?php
namespace App\Services\SitemapGenerator;


interface SitemapGenerator
{
    /**
     * @return mixed
     */
    public function generate();

    /**
     * @return mixed
     */
    public function getLinks();
}