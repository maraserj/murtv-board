<?php
namespace App\Services\SitemapGenerator;

use App\Models\Organization;
use Watson\Sitemap\Facades\Sitemap;
use App\Repositories\PostRepository;
use Storage;

class SitemapGeneratorService implements SitemapGenerator
{
    private $repository;
    private $urls = [];

    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    public function generate()
    {
        $i = 0;
        foreach ($this->urls as $url) {
            Sitemap::addTag($url['link'], $url['last_modify'], $url['change_frequency'], $url['priority']);
            $i++;
        }

        $sitemap = Sitemap::xml();

        Storage::disk('public')->put('sitemap.xml', $sitemap, 'public');
        Sitemap::clear();

        return $i;
    }

    public function getLinks()
    {
        $organizations = Organization::active()->get();
        $posts = $this->repository->active()->get();
        $url = [];
        $this->urls[] = [
            'link' => route('index'),
            'last_modify' => today(),
            'change_frequency' => 'daily',
            'priority' => '1',
        ];
        $this->urls[] = [
            'link' => route('page.show', ['slug' => 'terms']),
            'last_modify' => today(),
            'change_frequency' => 'daily',
            'priority' => '1',
        ];

        $this->urls[] = [
            'link' => route('posts.index'),
            'last_modify' => today(),
            'change_frequency' => 'daily',
            'priority' => '0.8',
        ];

        $this->urls[] = [
            'link' => route('organizations.index'),
            'last_modify' => today(),
            'change_frequency' => 'daily',
            'priority' => '0.8',
        ];

        foreach ($posts as $post) {
            $url['link'] = route('posts.show', ['slug' => $post->slug]);
            $url['last_modify'] = $post->updated_at;
            $url['change_frequency'] = 'daily';
            $url['priority'] = '0.7';
            $this->urls[] = $url;
            $url = null;
        }

        foreach ($organizations as $organization) {
            $url['link'] = route('organizations.show', ['slug' => $organization->slug]);
            $url['last_modify'] = $organization->updated_at;
            $url['change_frequency'] = 'daily';
            $url['priority'] = '0.7';
            $this->urls[] = $url;
            $url = null;
        }
    }
}