<?php

namespace App\Repositories;

use App\Http\Controllers\Admin\PostController;
use App\Models\City;
use App\Models\Photo;
use App\Models\Post;
use App\Models\PostType;
use App\Services\ImageUpload;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class PostRepository extends BaseRepository
{
    protected $model;
    protected $role;
    protected $photo;
    private $filterCollection;
    /**
     * @var ImageUpload
     */
    private $imageUpload;

    public function __construct(Post $model, Photo $photo, ImageUpload $imageUpload)
    {
        $this->model = $model;
        $this->photo = $photo;
        parent::__construct($model);
        $this->imageUpload = $imageUpload;
    }

    public function active()
    {
        return $this->model->active();
    }

    public function getAllPostTypes()
    {
        return PostType::orderBy('title')->get();
    }

    public function findBy($column, $value, $relations = [])
    {
        return $this->model->where($column, $value)->with($relations)->firstOrFail();
    }

    public function filter(array $data = [], $need_paginate = false, $active = true)
    {
        $sortBy = $data['sortBy'] ?? 'created_at';
        $sortOrder = $data['order'] ?? 'desc';

        if ($active) {
            $this->filterCollection = $this->model->active()->with('photos');
        } else {
            $this->filterCollection = $this->model->with('photos');
        }

        $this->_fillable[] = 'created_at';
        $whereParams = $this->addWhereParams($data);
        $this->filterCollection->where($whereParams);

        if (!empty($data['age'])) {
            $datesArray = $this->getAgeBetweenArray($data['age']);
            $this->filterCollection->whereBetween('birthday', $datesArray);
        }

        if (!empty($sortBy) && !empty($sortOrder) && in_array($sortBy, $this->_fillable)) {
            $this->filterCollection->orderBy($sortBy, $sortOrder);
        }

        return !$need_paginate ? $this->filterCollection : $this->filterCollection->paginate($data['perPage']);
    }

    private function addWhereParams(array $data = [])
    {
        if (empty($data)) {
            return [];
        }
        $params = [];

        $data = array_only($data, $this->_fillable);
        foreach ($data as $key => $fillParam) {
            if ($key == 'organization_id' && $fillParam == '9999') {
                $params[] = ['organization_id', '=', null];
            } else {
                $params[] = [$key, '=', $fillParam];
            }
        }

        return $params;
    }

    private function getAgeBetweenArray(string $age = '')
    {
        $firstDate = null;
        $secondDate = null;
        $ageParam = !empty($age) ? $age : null;

        if (!empty($ageParam)) {
            switch ($ageParam) {
                case '0-0.5':
                    $age = [0, 0.5];
                    break;
                case '0.6-1':
                    $age = [0.6, 1];
                    break;
                case '1.1-2':
                    $age = [1.1, 2];
                    break;
                case '2.1-3':
                    $age = [2.1, 3];
                    break;
                case '3.1-4':
                    $age = [3.1, 4];
                    break;
                case '4.1-5':
                    $age = [4.1, 5];
                    break;
                case '5+':
                    $age = [5.1, 99999];
                    break;
                case '5':
                    $age = [5.1, 99999];
                    break;
                default:
                    $age = null;
            }
            if ($age) {
                if ($age[1] < 1) {
                    $firstDate = Carbon::today()->addMonth(-($age[1] * 10));
                    $secondDate = Carbon::today()->addMonth(-($age[0] * 10));
                } else {
                    $firstDate = Carbon::today()->addYears(-$age[1]);
                    $secondDate = Carbon::today()->addYears(-$age[0]);
                }
            }
        }

        return [$firstDate, $secondDate];
    }

    public function firstOrCreate(array $attributes, array $values = [])
    {
        if (! is_null($instance = $this->model->where($attributes)->first())) {
            return $instance;
        }

        return tap($this->create($attributes + $values), function ($instance) {
            $instance->save();
        });
    }

    public function create(array $data = [])
    {
        $data['slug'] = str_slug($data['title']);
        $data = $this->checkEmpty($data);

        $model = parent::create($data);

        if ($model) {
            $update = [];
            if ($model->animal_id && Str::contains($model->animal_id, 'pet_id')) {
                $animal_id = str_replace('pet_id', $model->id, $model->animal_id);
                $update['animal_id'] = $animal_id;
            }
            $update['slug'] = $data['slug'] . '-' . $model->id;
            $model->update($update);
        }

        return $model;
    }

    public function update($id, array $data = [])
    {
        if (!empty($data['title'])) {
            $data['slug'] = str_slug($data['title']) . '-' .  $id;
        }
        $data = $this->checkEmpty($data);

        $model = parent::update($id, $data);

        if ($model) {
            if ($model->animal_id && Str::contains($model->animal_id, 'pet_id')) {
                $animal_id = str_replace('pet_id', $model->id, $model->animal_id);
                $model->update(['animal_id' => $animal_id]);
            }
        }

        return $model;
    }

    public function createImage(Post $post, $images = [])
    {
        $this->imageUpload->baseFolder = "posts/{$post->id}";

        foreach ($images as $image) {
            $imagePath = $this->imageUpload->save($image, 800, null);

            if (!empty($imagePath)) {
                $post->photos()->create([
                    'file_name' => last(explode('/', $imagePath)),
                    'path'      => $imagePath,
                ]);
            }
        }
    }

    private function checkEmpty(array $data = [])
    {
        if (!empty($data['city_id'])) {
            $city = City::findOrFail($data['city_id']);

            if (empty($data['region_id'])) {
                $data['region_id'] = $city->region_id;
            }
            if (empty($data['country_id'])) {
                $data['country_id'] = $city->region->country_id;
            }
        }

        if (!empty($data['category_id']) && !empty($data['is_for_sale'])) {
            $data['sale_type'] = Post::SALE_TYPE;
        } else if (empty($data['category_id'])) {
            $data['category_id'] = null;
            $data['price'] = null;
            $data['sale_type'] = null;
        }

        if (empty($data['sterilized'])) {
            $data['sterilized'] = false;
        }
        if (empty($data['sterilized'])) {
            $data['sterilized'] = false;
        }
        if (empty($data['ill'])) {
            $data['ill'] = false;
        }
        if (empty($data['guarded'])) {
            $data['guarded'] = false;
        }
        if (empty($data['deadly'])) {
            $data['deadly'] = false;
        }
        if (empty($data['featured'])) {
            $data['featured'] = false;
        }
        if (empty($data['animal_id'])) {
            $data['animal_id'] = 'BM-';
            if (!empty($data['country_id'])) {
                $data['animal_id'] .= $data['country_id'];
            }
            if (!empty($data['region_id'])) {
                $data['animal_id'] .= '-' . $data['region_id'];
            }
            if (!empty($data['city_id'])) {
                $data['animal_id'] .= '-' . $data['city_id'];
            }
            $data['animal_id'] .= '-pet_id';
        }
        if (!empty($data['description'])) {
            $data['description'] = strip_tags($data['description'], '<span><p><br>');
        }

        return $data;
    }

    public function deletePhotos($post_or_id)
    {
        if (!($post_or_id instanceof Post)) {
            $post_or_id = $this->findOrFail($post_or_id);
        }

        if (!empty($post_or_id->photos()->count())) {
            foreach ($post_or_id->photos as $photo) {
                \Storage::delete($photo->path);
                $photo->delete();
            }
        }
    }

}