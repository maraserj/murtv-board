<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

abstract class BaseRepository
{
    protected $_fillable = [];
    protected $validationRules = [];
    protected $model;


    public function __construct(Model $model)
    {
        $this->_fillable = $model->getFillable();
        $this->model = $model;
    }


    public function validatorAttributeNames()
    {
        return [];
    }

    public function getModel()
    {
        return $this->model;
    }

    public function with($array)
    {
        return $this->model->with($array);
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findBy($column, $value)
    {
        return $this->model->where($column, $value)->first();
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function query()
    {
        return $this->model->query();
    }

    public function count()
    {
        return $this->model->count();
    }

    public function instance(array $attributes = [])
    {
        $model = $this->model;

        return new $model($attributes);
    }

    public function where($key, $where)
    {
        return $this->model->where($key, $where);
    }

    public function whereIn($key, $whereIn)
    {
        return $this->model->whereIn($key, $whereIn);
    }

    public function paginate($perPage = null)
    {
        return $this->model->paginate($perPage);
    }

    public function validator(array $data = [], $rules = null, array $messages = [], array $customAttributes = [])
    {
        if (is_null($rules)) {
            $rules = $this->validationRules;
        }

        return validator($data, $rules, $messages, $customAttributes);
    }

    public function validate(array $data = [], $rules = null, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->validator($data, $rules, $messages, $customAttributes);

        return $this->_validate($validator);
    }

    public function create(array $data = [])
    {
        return $this->model->create($data);
    }

    public function update($id, array $data = [])
    {
        $instance = $this->findOrFail($id);
        $instance->update($data);

        return $instance;
    }

    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->delete();

        return $model;
    }

    protected function _validate(Validator $validator)
    {
        if (!empty($attributeNames = $this->validatorAttributeNames())) {
            $validator->setAttributeNames($attributeNames);
        }

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return true;
    }

}