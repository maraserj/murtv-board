<?php

namespace App\Repositories;

use App\Http\Controllers\Admin\PostController;
use App\Models\Organization;
use App\Models\OrganizationType;
use App\Models\Photo;
use App\Models\Post;
use App\Models\PostType;
use App\Services\ImageUpload;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class OrganizationRepository extends BaseRepository
{
    protected $model;
    protected $role;
    protected $photo;
    private $filterCollection;
    /**
     * @var ImageUpload
     */
    private $imageUpload;

    public function __construct(Organization $model, Photo $photo, ImageUpload $imageUpload)
    {
        $this->model = $model;
        $this->photo = $photo;
        parent::__construct($model);
        $this->imageUpload = $imageUpload;
    }

    public function active()
    {
        return $this->model->active();
    }

    public function getAllTypes()
    {
        return OrganizationType::orderBy('title')->get();
    }

    public function findBy($column, $value, $relations = [])
    {
        return $this->model->where($column, $value)->with($relations)->firstOrFail();
    }

    public function filter(array $data = [], $need_paginate = false)
    {
        $sortBy = $data['sortBy'] ?? 'created_at';
        $sortOrder = $data['order'] ?? 'desc';

        $this->filterCollection = $this->model->active();

        $whereParams = $this->addWhereParams($data);
        $this->filterCollection->where($whereParams);

        $this->_fillable[] = 'created_at';
        if (!empty($sortBy) && !empty($sortOrder) && in_array($sortBy, $this->_fillable)) {
            $this->filterCollection->orderBy($sortBy, $sortOrder);
        }

        return !$need_paginate ? $this->filterCollection : $this->filterCollection->paginate($data['perPage']);
    }

    private function addWhereParams(array $data = [])
    {
        if (empty($data)) {
            return [];
        }
        $params = [];

        $data = array_only($data, $this->model->getFillable());
        foreach ($data as $key => $fillParam) {
            $params[] = [$key, '=', $fillParam];
        }

        return $params;
    }

    public function create(array $data = [])
    {
        $data['slug'] = str_slug($data['title']);
        $data = $this->checkEmpty($data);

        $model = parent::create($data);

        return $model;
    }

    public function update($id, array $data = [])
    {
        if (!empty($data['title'])) {
            $data['slug'] = str_slug($data['title']);
        }
        $data = $this->checkEmpty($data);

        $model = parent::update($id, $data);

        return $model;
    }

    public function createImage(Organization $post, $images = [])
    {
        $this->imageUpload->baseFolder = "organizations/{$post->id}";

        foreach ($images as $image) {
            $imagePath = $this->imageUpload->save($image, 800, null);

            if (!empty($imagePath)) {
                $post->photos()->create([
                    'file_name' => last(explode('/', $imagePath)),
                    'path'      => $imagePath,
                ]);
            }
        }
    }

    private function checkEmpty(array $data = [])
    {

        if (!empty($data['description'])) {
            $data['description'] = strip_tags($data['description'], '<span><p><br>');
        }

        return $data;
    }

    public function deletePhotos($post_or_id)
    {
        if (!($post_or_id instanceof Organization)) {
            $post_or_id = $this->findOrFail($post_or_id);
        }

        if (!empty($post_or_id->photos()->count())) {
            foreach ($post_or_id->photos as $photo) {
                \Storage::delete($photo->path);
                $photo->delete();
            }
        }
    }

}