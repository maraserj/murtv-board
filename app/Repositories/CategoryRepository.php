<?php

namespace App\Repositories;

use DateHelper;
use App\Models\Category;

class CategoryRepository extends BaseRepository
{

    protected $model;
    public $active = 1;
    public $main = 0;
    protected $fillable = [];

    public function __construct(Category $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    public function active()
    {
        return $this->model->active();
    }

    public function getMain()
    {
        return $this->where('parent_id', $this->main)->where('active', $this->active)->get();
    }

    public function all()
    {
        $categories = $this->model->all();

        return $categories;
    }

    public function getForMainPageBig()
    {
        return $this->model->active()->main()->where('big_caption', true)->get();
    }

    public function getForMainPageSmall()
    {
        return $this->model->active()->main()->where('big_caption', false)->get();
    }

    public function paginate($perPage = 15)
    {
        $categories = $this->model->paginate($perPage);

        return $categories;
    }

    public function limit($limit = 15)
    {
        $categories = $this->model->limit($limit);

        return $categories;
    }

    public function getActive()
    {
        return $this->model->active()->get();
    }

    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)->with(['channels' => function ($query) {
            $query->active();
        }])->first();
    }

    public function changeStatus(array $data = [])
    {
        return parent::update($data['id'], array_only($data, $this->fillable));
    }

    public function create(array $data = [])
    {
        $data = $this->checkEmpty($data);
        return parent::create(array_only($data, $this->fillable));
    }

    public function update($id, array $data = [])
    {
        if (!empty($data['title'])) {
            $data = $this->checkEmpty($data);
        }
        return parent::update($id, array_only($data, $this->fillable));
    }

    public function delete($id)
    {
        return parent::delete($id);
    }

    private function checkEmpty(array $data = [])
    {
        if (empty($data['slug'])) {
            $data['slug'] = str_slug($data['title']);
        }
        if (empty($data['seo_h1'])) {
            $data['seo_h1'] = $data['title'];
        }
        if (empty($data['seo_title'])) {
            $data['seo_title'] = $data['title'];
        }
        if (empty($data['seo_description'])) {
            $data['seo_description'] = $data['title'];
        }
        if (empty($data['show_on_main'])) {
            $data['show_on_main'] = 0;
        }
        if (empty($data['show_in_menu'])) {
            $data['show_in_menu'] = 0;
        }
        if (empty($data['parent_id'])) {
            $data['parent_id'] = null;
        }
        if (empty($data['is_for_sale'])) {
            $data['is_for_sale'] = false;
        }

        return $data;
    }
}
