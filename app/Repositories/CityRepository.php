<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class CityRepository extends BaseRepository
{
    protected $model;
    private $filterCollection;

    public function __construct(City $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    public function active()
    {
        return $this->model->active();
    }

    public function paginate($perPage = 20)
    {
        return $this->model->with(['region', 'region.country'])->paginate($perPage);
    }

    public function filter(array $params = [], $need_paginate = false)
    {
        $relations = ['region', 'region.country'];
        $sortBy = $params['sortBy'] ?? 'created_at';
        $sortOrder = $params['order'] ?? 'desc';
        $params['perPage'] = $params['perPage'] ?? 60;

        $this->filterCollection = $this->model->active()->with($relations);

        $whereParams = $this->addWhereParams($params);
        $this->filterCollection->where($whereParams);

        if (!empty($params['q'])) {
            $this->filterCollection->where('name', 'like', '%' . $params['q'] . '%');
        }

        $this->_fillable[] = 'created_at';
        if (!empty($sortBy) && !empty($sortOrder) && in_array($sortBy, $this->_fillable)) {
            $this->filterCollection->orderBy($sortBy, $sortOrder);
        }

        return $this->filterCollection->paginate($params['perPage']);
    }

    public function create(array $data = [])
    {
        $data['slug'] = str_slug($data['name']);

        return parent::create($data);
    }

    public function update($id, array $data = [])
    {
        if (!empty($data['name'])) {
            $data['slug'] = str_slug($data['name']);
        }

        return parent::update($id, $data);
    }

    private function addWhereParams(array $data = [])
    {
        if (empty($data)) {
            return [];
        }
        $params = [];
        $this->_fillable[] = 'created_at';

        $data = array_only($data, $this->_fillable);
        foreach ($data as $key => $fillParam) {
            if ($key == 'region_id' && empty($fillParam)) {
                continue;
            }
            $params[] = [$key, '=', $fillParam];
        }

        return $params;
    }


}