<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Models\SettingGroup;
use App\Services\ImageUpload;
use Illuminate\Support\Collection;

class SettingsRepository extends BaseRepository
{
    protected $imageRepository;
    /**
     * @var Setting
     */
    private $settings;
    /**
     * @var ImageUpload
     */
    private $imageUpload;
    /**
     * @var SettingGroup
     */
    private $settingGroup;

    /**
     * SettingsRepository constructor.
     * @param Setting $settings
     * @param ImageUpload $imageUpload
     * @param SettingGroup $settingGroup
     */
    public function __construct(Setting $settings, ImageUpload $imageUpload, SettingGroup $settingGroup)
    {
        $this->_fillable = $settings->getFillable();
        $this->settings = $settings;
        $this->imageUpload = $imageUpload;
        $this->imageUpload->baseFolder = 'settings';
        $this->settingGroup = $settingGroup;
        parent::__construct($settings);
    }

    public function getSeo(): Collection
    {
        return $this->getSettingsByGroup('Seo');
    }
    public function getMain(): Collection
    {
        return $this->getSettingsByGroup();
    }

    public function getSettingsByGroup($name = 'main')
    {
        return $this->settingGroup->where('name', $name)->first()->settings()->get();
    }

    public function create(array $data = [])
    {
        $newData = [];
        $type = $data['type'];
        $newData['name'] = $data['name'];
        $newData['code'] = $data['code'];
        $newData['group_id'] = $data['group_id'];

        if ($type && $type == 'text') {
            $newData['type'] = 'text';

            if (isset($data['text'])) {
                if ($newData['group_id'] != 'other' || !$newData['group_id']) {
                    $newData['value'] = $data['text'];
                } else {
                    $newData['value'] = strip_tags($data['text']);
                }
            }
        } elseif ($type && $type == 'textarea') {
            $newData['type'] = 'textarea';
            if (isset($data['textarea'])) {
                if ($newData['group_id'] != 'other' || !$newData['group_id']) {
                    $newData['value'] = $data['textarea'];
                } else {
                    $newData['value'] = strip_tags($data['textarea']);
                }
            }
        } elseif ($type && $type == 'file') {
            $newData['type'] = 'file';
            if (isset($data['text'])) {
                $newData['value'] = $data['text'];
            }
        } elseif ($type && $type == 'image') {
            $newData['type'] = 'image';
            if (isset($data['image'])) {
                $newData['value'] = $data['image'];
            }
        }
        return parent::create(array_only($newData, $this->_fillable));
    }


    public function update($id, array $data = [])
    {
        $newData = [];
        $type = $data['type'];
        $newData['name'] = $data['name'];
        $newData['code'] = $data['code'];
        $newData['group_id'] = $data['group_id'];

        if ($type && $type == 'text') {
            $newData['type'] = 'text';
            if (isset($data['text'])) {
                if ($newData['group_id'] != 'other' || !$newData['group_id']) {
                    $newData['value'] = $data['text'];
                } else {
                    $newData['value'] = strip_tags($data['text']);
                }
            }
        } elseif ($type && $type == 'textarea') {
            $newData['type'] = 'textarea';
            if (isset($data['textarea'])) {
                if ($newData['group_id'] != 'other' || !$newData['group_id']) {
                    $newData['value'] = $data['textarea'];
                } else {
                    $newData['value'] = strip_tags($data['textarea']);
                }
            }
        } elseif ($type && $type == 'file') {
            $newData['type'] = 'file';
            if (isset($data['text'])) {
                $newData['value'] = $data['text'];
            }
        } elseif ($type && $type == 'image') {
            $newData['type'] = 'image';
            if (isset($data['image'])) {
                $newData['value'] = $data['image'];
            }
        }
        return parent::update($id, array_only($newData, $this->_fillable));
    }

    public function uploadSettingImage($setting, $image, $width, $heigth, $method)
    {
        $this->imageUpload->baseFolder = 'settings';

        if ((isset($width) && $width != '') && (isset($heigth) && $heigth != '')) {
            $imagePath = $this->imageUpload->save($image, (int) $width, (int) $heigth, $method, $setting->value);
        } elseif (isset($width) && $width != '') {
            $imagePath = $this->imageUpload->save($image, (int) $width, null, $method, $setting->value);
        } else {
            $imagePath = $this->imageUpload->save($image,null, null, $method, $setting->value);
        }

        return $imagePath;
    }


    public function delete($id)
    {
        $model = $this->settings->find($id);
        if ($model) {
            \Storage::disk('public')->delete(str_replace('/storage', '', $model->value));
            return $model->delete();
        } else {
            return null;
        }
    }

}