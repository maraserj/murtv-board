<?php

namespace App\Repositories;

use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class CountryRepository extends BaseRepository
{
    protected $model;

    public function __construct(Country $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    public function active()
    {
        return $this->model->active();
    }

    public function create(array $data = [])
    {
        if (!empty($data['name'])) {
            $data['slug'] = str_slug($data['name']);
        }
        if (empty($data['status'])) {
            $data['status'] = 0;
        }
        return parent::create($data);
    }

    public function update($id, array $data = [])
    {
        if (!empty($data['name'])) {
            $data['slug'] = str_slug($data['name']);
        }
        if (empty($data['status'])) {
            $data['status'] = 0;
        }

        return parent::update($id, $data);
    }

}