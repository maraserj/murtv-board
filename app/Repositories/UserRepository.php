<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository
{
    protected $model;
    protected $role;
    private $filterCollection;

    public function __construct(User $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    public function all(array $data = [])
    {
        $page = $data['perPage'] ?? 25;
        $this->filterCollection = $this->model;

        if (!empty($data)) {
            $this->filterCollection->where(array_only($data, $this->_fillable));

            if (!empty($data['sortBy']) && !empty($data['sortTo'])) {
                $this->filterCollection->orderBy($data['sortBy'], $data['sortTo']);
            }
        }

        return $this->filterCollection->paginate($page);
    }

    public function getById($id)
    {
        $user = $this->model->where('id', $id)->first();

        return $user;
    }

    public function findByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }

    public function create(array $data = [])
    {
        if (isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }

        return parent::create(array_only($data, $this->_fillable));
    }

    public function update($id, array $data = [])
    {
        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::update($id, array_only($data, $this->_fillable));
    }


    public function delete($id)
    {
        return parent::delete($id);
    }

}