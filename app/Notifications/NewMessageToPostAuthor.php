<?php

namespace App\Notifications;

use App\Models\Post;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewMessageToPostAuthor extends Notification
{
    use Queueable;
    private $user;
    private $post;
    private $mail_data;

    public function __construct(User $user, Post $post, array $mail_data = [])
    {
        $this->user = $user;
        $this->post = $post;
        $this->mail_data = $mail_data;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Новый ответ на Ваше объявление')
            ->greeting('Здравствуйте, ' . $this->user->name . '!')
            ->line('Вы получили новый ответ на ваше объявление: "' . $this->post->title . '", от ' . ($this->mail_data['name'] ?? 'Не указано') . ', почта: ' . $this->mail_data['email'])
            ->line($this->mail_data['text'])
            ->line('Спасибо, за пользование нашим сервисом.');
    }

    public function toArray($notifiable)
    {
        return [
            'to_user'   => $this->user->id,
            'from_mail' => $this->mail_data['email'],
            'text'      => $this->mail_data['text'],
        ];
    }
}
