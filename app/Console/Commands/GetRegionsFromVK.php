<?php

namespace App\Console\Commands;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Repositories\RegionRepository;
use Illuminate\Console\Command;
use GuzzleHttp\Client as HttpClient;

class GetRegionsFromVK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse_vk:regions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get regions from API VK';
    /**
     * @var HttpClient
     */
    private $client;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new HttpClient(['base_uri' => 'https://api.vk.com/method/']);
    }

    /**
     * Execute the console command.
     *
     * @param RegionRepository $regionRepository
     * @return mixed
     */
    public function handle(RegionRepository $regionRepository)
    {
        $countries = Country::all();
        $i = 0;
//        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        City::truncate();
//        Region::truncate();
//        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach ($countries as $country) {
            $response = $this->client->post('database.getRegions', [
                'form_params' => [
                    'country_id' => $country->vk_country_id,
                    'need_all'   => 1,
                    'count'      => 1000,
                ],
            ]);

            $collection = collect(json_decode($response->getBody()->getContents())->response);
            $regions = $collection->sortBy('title')->all();
            $newRegion = null;
            foreach ($regions as $region) {
                $newRegion = $regionRepository->create([
                    'vk_region_id' => (int) $region->region_id,
                    'name' => $region->title,
                    'country_id' => $country->id,
                ]);
                $i++;
            }
            $this->info($i);

        }
        dd($i);
    }
}
