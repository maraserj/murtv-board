<?php

namespace App\Console\Commands;

use App\Services\SitemapGenerator\SitemapGenerator;
use Illuminate\Console\Command;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new sitemap.xml file';
    /**
     * @var SitemapGenerator
     */
    private $sitemapGenerator;

    /**
     * Create a new command instance.
     *
     * @param SitemapGenerator $sitemapGenerator
     */
    public function __construct(SitemapGenerator $sitemapGenerator)
    {
        parent::__construct();
        $this->sitemapGenerator = $sitemapGenerator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        info('Scheduled');

        $this->sitemapGenerator->getLinks();
        $this->info('Links in sitemap: ' . $this->sitemapGenerator->generate());
    }
}
