<?php

namespace App\Console\Commands;

use App\Models\City;
use App\Models\Region;
use App\Repositories\CityRepository;
use Illuminate\Console\Command;
use GuzzleHttp\Client as HttpClient;

class GetCitiesFromVK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse_vk:cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get cities from API VK';
    /**
     * @var HttpClient
     */
    private $client;
    private $counter;
    private $offset = 0;

    /**
     * Create a new command instance.
     *
     * @param HttpClient $client
     */
    public function __construct(HttpClient $client)
    {
        parent::__construct();
        $this->client = new HttpClient(['base_uri' => 'https://api.vk.com/method/']);
    }

    /**
     * Execute the console command.
     *
     * @param CityRepository $cityRepository
     * @return mixed
     */
    public function handle(CityRepository $cityRepository)
    {
        $regions = Region::whereNotIn('id', [0])->get();
        $this->counter = 0;

//        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        City::truncate();
//        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach ($regions as $region) {
            $this->offset = 0;
            $this->info("Region: $region->name");
            $this->makeRequest($region, $cityRepository);
        }
        $this->warn('Done!');
    }

    private function makeRequest(Region $region, CityRepository $cityRepository)
    {
        $response = $this->client->post('database.getCities', [
            'form_params' => [
                'country_id' => $region->country->vk_country_id,
                'region_id'  => $region->vk_region_id,
                'need_all'   => 1,
                'offset'     => $this->offset,
                'count'      => 1000,
            ],
        ]);

        $collection = collect(json_decode($response->getBody()->getContents())->response);
        $cities = $collection->sortBy('title')->all();
        $this->saveCities($region, $cityRepository, $cities);

        if ($collection->count() > 999) {
            $this->info('Count > 1000. Request again');
            $this->offset += 999;
            return $this->makeRequest($region, $cityRepository);
        }

        return true;
    }

    private function saveCities(Region $region, CityRepository $cityRepository, $cities)
    {
        $newCity = null;
        $this->info($region->name);
        foreach ($cities as $city) {
            $createData = [
                'name'       => $city->title,
                'region_id'  => $region->id,
                'vk_city_id' => $city->cid,
            ];
            if (isset($city->area)) {
                $createData['area'] = $city->area;
            }
            $newCity = $cityRepository->create($createData);
            $this->counter++;
        }
//            dd($newCity);
        $this->info($this->counter);
    }
}
