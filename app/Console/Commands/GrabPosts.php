<?php

namespace App\Console\Commands;

use App\Services\PostsGrabber\PostsGrabber;
use Illuminate\Console\Command;

class GrabPosts extends Command
{
    protected $signature = 'grab_posts:run {--offset= : Number of post when need start} {--limit= : Limit of posts need to grab}';

    protected $description = 'Grabbing posts command';
    /**
     * @var PostsGrabber
     */
    private $postsGrabber;

    public function __construct(PostsGrabber $postsGrabber)
    {
        parent::__construct();
        $this->postsGrabber = $postsGrabber;
    }

    public function handle()
    {
        $offset = $this->option('offset');
        $limit = $this->option('limit');

        if (empty($offset)) {
            $offset = null;
        }
        if (empty($limit)) {
            $limit = null;
        }

        try {
            $this->postsGrabber->runGrabber($offset, $limit);
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
            info('Posts grabbing error: ', [$exception->getMessage()]);
        }
    }
}
