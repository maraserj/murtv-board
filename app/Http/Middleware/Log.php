<?php

namespace App\Http\Middleware;

use Closure;

class Log
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!request()->is('*admin*') && request()->ip() != '127.0.0.1') {
            $user_info = '';
            if (auth()->check()) {
                $user_info .= ' | {{ user_id: ' . auth()->id() . ' }}';
            }
            info('url: ' . request()->getRequestUri() .
                ' || ip: ' . request()->ip() .
                ' || UA: '.request()->userAgent() . $user_info);
        }
        return $next($request);
    }
}
