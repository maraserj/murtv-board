<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Organization;
use App\Models\Region;
use App\Repositories\OrganizationRepository;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    private $model;

    /**
     * PostController constructor.
     * @param OrganizationRepository $model
     */
    public function __construct(OrganizationRepository $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $user = \Auth::user();
        $perPage = $request->get('perPage', 20);

        $this->viewData['organizations'] = $user->organizations()->with('photos')->paginate($perPage);

        return view('profile.organizations.index', $this->viewData);
    }

    public function create()
    {
        $this->viewData['organization_types'] = $this->model->getAllTypes();
        $this->viewData['organization'] = $this->model->instance();
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['regions'] = Region::active()->get();

        return view('profile.organizations.create-edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'organization_type_id' => 'required|not_in:0',
            'country_id'           => 'required|not_in:0',
            'region_id'            => 'required|not_in:0',
            'city_id'              => 'required|not_in:0',
            'title'                => 'required|min:10|max:90',
            'contact_name'         => 'required|min:3|max:90',
            'contact_surname'      => 'required|min:3|max:90',
            'contact_email'        => 'required|min:3|max:90',
            'contact_phone'        => 'required|min:5|max:90',
            'description'          => 'required|min:20|max:2500',
            'accept_terms'         => 'accepted',
        ]);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $organization = $this->model->create($data);
        if ($organization) {
            if (!empty($request->allFiles()) && !empty($request->allFiles()['images'])) {
                $this->model->createImage($organization, $request->allFiles()['images']);
            }

            return redirect()->route('profile.organizations.index')->with('success', __('flash.created'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }

    public function edit($id)
    {
        $this->viewData['organization_types'] = $this->model->getAllTypes();
        $this->viewData['organization'] = $this->model->findOrFail($id);
        if ($this->viewData['organization']->user_id != auth()->id() && !auth()->user()->can('admin')) {
            abort(404);
        }
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['regions'] = Region::active()->get();

        return view('profile.organizations.create-edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'organization_type_id' => 'required|not_in:0',
            'country_id'           => 'required|not_in:0',
            'region_id'            => 'required|not_in:0',
            'city_id'              => 'required|not_in:0',
            'title'                => 'required|min:10|max:90',
            'contact_name'         => 'required|min:3|max:90',
            'contact_surname'      => 'required|min:3|max:90',
            'contact_email'        => 'required|min:3|max:90',
            'contact_phone'        => 'required|min:5|max:90',
            'description'          => 'required|min:20|max:2500',
            'accept_terms'         => 'accepted',
        ]);
        $this->viewData['organization'] = $this->model->findOrFail($id);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        if ($this->viewData['organization']->user_id != auth()->id()) {
            abort(404);
        }
        $organization = $this->model->update($id, $data);
        if (!empty($request->allFiles()) && !empty($request->allFiles()['images'])) {
            $this->model->createImage($organization, $request->allFiles()['images']);
        }

        if ($organization) {
            return redirect()->route('profile.organizations.index')->with('success', __('flash.updated'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }


    public function destroy(Organization $organization)
    {
        if (auth()->id() != $organization->user_id && !auth()->user()->can('admin')) {
            return back()->with('error', __('flash.not_found'));
        } else {
            $this->model->deletePhotos($organization);
            if ($organization->delete()) {
                return back()->with('success', __('flash.deleted'));
            } else {
                return back()->with('error', __('flash.error'));
            }
        }
    }

    public function addToFavourites(Request $request)
    {
        $organization = $this->model->findOrFail($request->get('post_id', 0));

        $favourite = $organization->favourites()->firstOrCreate(['user_id' => auth()->id()]);

        if ($favourite) {
            return response()->json(['status' => 'ok', 'message' => __('flash.created')]);
        }

        return response()->json(['status' => 'fail'], 400);
    }

    public function removeFromFavourites(Request $request)
    {
        $organization = $this->model->findOrFail($request->get('post_id', 0));

        $favourite = $organization->favourites()->where(['user_id' => auth()->id()])->firstOrFail();

        if ($favourite->delete()) {
            if (!$request->ajax()) {
                return back()->with('success', __('flash.deleted'));
            }

            return response()->json(['status' => 'ok', 'message' => __('flash.deleted')]);
        }

        if (!$request->ajax()) {
            return back()->with('error', __('flash.error'));
        }

        return response()->json(['status' => 'fail'], 400);
    }

    public function deleteImage(Request $request)
    {
        $photo_id = $request->get('key');
        $organization_id = $request->get('id');
        if (empty($photo_id) || empty($organization_id)) {
            return response()->json(['status' => 'fail', 'message' => 'Empty photo or organization'], 404);
        }

        $organization = $this->model->findOrFail($organization_id);
        if (auth()->id() != $organization->user_id) {
            if (!auth()->user()->can('admin')) {
                return response()->json(['status' => 'fail', 'message' => 'Aborted'], 403);
            }
        }
        $photo = $organization->photos()->where('id', $photo_id)->first();

        if (!$photo) {
            return response()->json(['status' => 'fail', 'message' => 'Empty photo or organization'], 404);
        }

        \Storage::delete($photo->path);
        if ($photo->delete()) {
            return response()->json(['status' => 'success', 'message' => 'Успешно удалено']);
        }

        return response()->json(['status' => 'fail', 'message' => 'Aborted'], 500);
    }
}
