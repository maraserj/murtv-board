<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\BreedType;
use App\Models\Category;
use App\Models\Color;
use App\Models\Country;
use App\Models\Post;
use App\Models\Region;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $model;

    /**
     * PostController constructor.
     * @param PostRepository $model
     */
    public function __construct(PostRepository $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $user = \Auth::user();
        $perPage = $request->get('perPage', 20);

        $this->viewData['posts'] = $user->posts()->where('has_home', 0)->with('photos')->paginate($perPage);
        $this->viewData['posts_has_home'] = $user->posts()->where('has_home', 1)->with('photos')->paginate($perPage);

        return view('profile.posts.index', $this->viewData);
    }

    public function create()
    {
        $user = \Auth::user();
        $this->viewData['post_types'] = $this->model->getAllPostTypes();
        $this->viewData['post'] = $this->model->instance();
        $this->viewData['organizations'] = $user->organizations()->active()->orderBy('title')->get();
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['regions'] = Region::active()->get();
        $this->viewData['breed_types'] = BreedType::ordered()->get();
        $this->viewData['colors'] = Color::orderby('title')->get();
        $this->viewData['categories'] = Category::active()->forSale()->orderby('title')->get();

        return view('profile.posts.create-edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $not_in = '';
        $category_not_in = '';
        if (!$request->get('is_for_sale')) {
            $not_in = '|not_in:0';
        } else {
            $category_not_in = '|not_in:0';
        }
        $rules = [
            'post_type_id'  => 'required_without:is_for_sale' . $not_in,
            'gender'        => 'required_without:is_for_sale' . $not_in,
            'breed_type_id' => 'required_without:is_for_sale' . $not_in,
            'breed_id'      => 'required_without:is_for_sale' . $not_in,
            'category_id'   => 'required_with:is_for_sale' . $category_not_in,
//            'country_id'    => 'required|not_in:0',
//            'region_id'     => 'required|not_in:0',
            'city_id'       => 'required|not_in:0',
            'title'         => 'required|min:10|max:90',
            'description'   => 'required|min:20|max:2500',
            'accept_terms'  => 'accepted',
        ];
        if ($request->ajax() && $request->get('validate')) {
            return $this->validate($request, $rules);
        }
        $this->validate($request, $rules);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $post = $this->model->create($data);
        if ($post) {
            if (!empty($request->allFiles()) && !empty($request->allFiles()['images'])) {
                $this->model->createImage($post, $request->allFiles()['images']);
            }

            return redirect()->route('profile.posts.index')->with('success', __('flash.created'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }

    public function changeHomeStatus(Request $request, $id)
    {
        $this->viewData['post'] = $this->model->findOrFail($id);
        $data['has_home'] = $request->get('has_home');
        $data['user_id'] = auth()->id();
        if ($this->viewData['post']->user_id != auth()->id()) {
            abort(404);
        }
        $post = $this->model->update($id, $data);

        if ($post) {
            return redirect()->route('profile.posts.index')->with('success', __('flash.updated'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }

    public function show(Post $post)
    {
        //
    }

    public function edit($id)
    {
        $user = \Auth::user();
        $this->viewData['post_types'] = $this->model->getAllPostTypes();
        $this->viewData['post'] = $this->model->findOrFail($id);
        $this->viewData['organizations'] = $user->organizations()->active()->orderBy('title')->get();
        if ($this->viewData['post']->user_id != auth()->id()) {
            abort(404);
        }
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['regions'] = Region::active()->get();
        $this->viewData['breed_types'] = BreedType::ordered()->get();
        $this->viewData['colors'] = Color::orderby('title')->get();
        $this->viewData['categories'] = Category::active()->forSale()->orderby('title')->get();

        return view('profile.posts.create-edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $not_in = '';
        $category_not_in = '';
        if (!$request->get('is_for_sale')) {
            $not_in = '|not_in:0';
        } else {
            $category_not_in = '|not_in:0';
        }
        $rules = [
            'post_type_id'  => 'required_without:is_for_sale' . $not_in,
            'gender'        => 'required_without:is_for_sale' . $not_in,
            'breed_type_id' => 'required_without:is_for_sale' . $not_in,
            'breed_id'      => 'required_without:is_for_sale' . $not_in,
            'category_id'   => 'required_with:is_for_sale' . $category_not_in,
            //'country_id'    => 'required|not_in:0',
            //'region_id'     => 'required|not_in:0',
            'city_id'       => 'required|not_in:0',
            'title'         => 'required|min:10|max:90',
            'description'   => 'required|min:20|max:2500',
            'accept_terms'  => 'accepted',
        ];
        if ($request->ajax() && $request->get('validate')) {
            return $this->validate($request, $rules);
        }
        $this->validate($request, $rules);
        $this->viewData['post'] = $this->model->findOrFail($id);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        if ($this->viewData['post']->user_id != auth()->id()) {
            abort(404);
        }
        $post = $this->model->update($id, $data);
        if (!empty($request->allFiles()) && !empty($request->allFiles()['images'])) {
            $this->model->createImage($post, $request->allFiles()['images']);
        }

        if ($post) {
            return redirect()->route('profile.posts.index')->with('success', __('flash.updated'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }


    public function destroy(Post $post)
    {
        if (auth()->id() != $post->user_id) {
            return back()->with('error', __('flash.not_found'));
        } else {
            $this->model->deletePhotos($post);
            if ($post->delete()) {
                return back()->with('success', __('flash.deleted'));
            } else {
                return back()->with('error', __('flash.error'));
            }
        }
    }

    public function addToFavourites(Request $request)
    {
        $post = $this->model->findOrFail($request->get('post_id', 0));

        $favourite = $post->favourites()->firstOrCreate(['user_id' => auth()->id()]);

        if ($favourite) {
            return response()->json(['status' => 'ok', 'message' => __('flash.created')]);
        }

        return response()->json(['status' => 'fail'], 400);
    }

    public function removeFromFavourites(Request $request)
    {
        $post = $this->model->findOrFail($request->get('post_id', 0));

        $favourite = $post->favourites()->where(['user_id' => auth()->id()])->firstOrFail();

        if ($favourite->delete()) {
            if (!$request->ajax()) {
                return back()->with('success', __('flash.delete'));
            }

            return response()->json(['status' => 'ok', 'message' => __('flash.delete')]);
        }

        if (!$request->ajax()) {
            return back()->with('error', __('flash.error'));
        }

        return response()->json(['status' => 'fail'], 400);
    }

    public function markPhotoAsMain(Request $request)
    {
        $post_id = $request->get('post_id');
        $id = $request->get('id');

        if (empty($post_id) || empty($id)) {
            return response()->json(['status' => 'fail', 'message' => 'Bad request'], 400);
        }
        $post = $this->model->findOrFail($post_id);

        if (auth()->id() != $post->user_id) {
            if (!auth()->user()->can('admin')) {
                return response()->json(['status' => 'fail', 'message' => 'Aborted'], 403);
            }
        }

        $photo = $post->photos()->findOrFail($id);
        if (!$photo) {
            return response()->json(['status' => 'fail', 'message' => 'Empty photo or post'], 404);
        }

        \DB::beginTransaction();
        try {
            $main_photo = $post->photos()->where('is_main', true)->update(['is_main' => false]);
            $photo->update(['is_main' => true]);
            \DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Фото установлено как главное'], 200);
        } catch (\Exception $exception) {
            \DB::rollBack();
        }

        return response()->json(['status' => 'fail', 'message' => 'Aborted'], 500);
    }

    public function deleteImage(Request $request)
    {
        $photo_id = $request->get('key');
        $post_id = $request->get('id');
        if (empty($photo_id) || empty($post_id)) {
            return response()->json(['status' => 'fail', 'message' => 'Empty photo or post'], 404);
        }

        $post = $this->model->findOrFail($post_id);
        if (auth()->id() != $post->user_id) {
            if (!auth()->user()->can('admin')) {
                return response()->json(['status' => 'fail', 'message' => 'Aborted'], 403);
            }
        }
        $photo = $post->photos()->where('id', $photo_id)->first();

        if (!$photo) {
            return response()->json(['status' => 'fail', 'message' => 'Empty photo or post'], 404);
        }

        \Storage::delete($photo->path);
        if ($photo->delete()) {
            return response()->json(['status' => 'success', 'message' => 'Успешно удалено']);
        }

        return response()->json(['status' => 'fail', 'message' => 'Aborted'], 500);
    }
}
