<?php

namespace App\Http\Controllers\Profile;

use App\Repositories\UserRepository;
use App\Services\ImageUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class IndexController extends Controller
{
    private $user;
    /**
     * @var UserRepository
     */
    private $repository;
    private $imageUpload;

    public function __construct(UserRepository $repository, ImageUpload $imageUpload)
    {
        $this->repository = $repository;
        $this->imageUpload = $imageUpload;
        $this->imageUpload->baseFolder = 'avatars';
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('profile.index', $this->viewData);
    }
    public function update(Request $request)
    {
        $user = $this->user;
        $flashStatus = 'error';
        $flashMessage = __('flash.error');
        $avatar = $request->file('avatar');


        $this->validate($request, [
            'name'  => 'required',
        ]);
        $exist = $this->repository->findByEmail($request->get('email'));
        if ($exist && $user->id != $exist->id) {
            $this->validate($request, [
                'email' => 'unique:users',
            ]);
        }
        $data = $request->only(['name', 'phone']);

        if (!empty($avatar)) {
            if (!empty($user->avatar)) {
                $data['avatar'] = $this->imageUpload->save($request->file('avatar'), 300, null, 'update', $user->avatar);
            } else {
                $data['avatar'] = $this->imageUpload->save($request->file('avatar'), 300);
            }
        }

        if ($user->update($data)) {
            $flashStatus = 'success';
            $flashMessage = __('flash.updated');
        }
        $password = $request->get('current_password');
        $newPassword = $request->get('password');
        if (!empty($request->get('current_password'))) {
            $messages = [
                'current_password.required' => 'Пожалуйста, введите текущий пароль',
                'password.required'         => 'Пожалуйста, введите пароль',
            ];
            $this->validate($request, [
                'current_password' => 'required|min:6',
                'password'         => 'required|confirmed|min:6',
            ], $messages);

            $checked = \Hash::check($password, $user->password);

            if (!$checked) {

                $flashStatus = 'error';
                $flashMessage = __('flash.wrong_old_pass');

                return back()->with($flashStatus, $flashMessage);
            } else {
                $user->update(['password' => bcrypt($newPassword)]);

                $flashStatus = 'success';
                $flashMessage = __('flash.updated');
            }

        }

        return back()->with($flashStatus, $flashMessage);
    }

    public function destroy($id)
    {
        //
    }
}
