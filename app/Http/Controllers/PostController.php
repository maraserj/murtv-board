<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Favourite;
use App\Models\Organization;
use App\Models\Post;
use App\Models\User;
use App\Notifications\NewMessageToPostAuthor;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @var PostRepository
     */
    private $model;

    public function __construct(PostRepository $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 15);
        $data = $request->all();
        $this->viewData['favourites_array'] = [];
        $this->viewData['post_types'] = $this->model->getAllPostTypes();
        $this->viewData['organizations'] = Organization::active()->orderBy('title')->get();
        $this->viewData['categories'] = Category::active()->forSale()->orderby('title')->get();

        if (auth()->check()) {
            $this->viewData['favourites_array'] = Favourite::getFavouritePostIdsForUser(auth()->id());
        }
        if (!empty($data)) {
            $data['perPage'] = $perPage;
            $posts = $this->model->filter($data);
        } else {
            $posts = $this->model->active()->orderBy('created_at', 'desc');
        }
        $this->viewData['posts_count'] = $posts->count();

        $this->viewData['posts'] = $posts->paginate($perPage);

        return view('posts.index', $this->viewData);
    }

    public function show($slug)
    {
        $model = $this->model->findBy('slug', $slug);

        if (!$model) {
            return redirect()->route('posts.index')->with('error', 'Объявление удалено или не активно');
        }

        if (!session()->has("post_view_$model->id")) {
            session()->put("post_view_$model->id", 1);
            $model->increment('unique_view_count');
        }
        $model->increment('view_count');

        $this->viewData['post'] = $model;
        $this->viewData['favourites_array'] = [];
        if (auth()->check()) {
            $this->viewData['favourites_array'] = Favourite::getFavouritePostIdsForUser(auth()->id());
        }

        return view('posts.show', $this->viewData);
    }

    public function submitMessage(Request $request)
    {
        $this->validate($request, [
            'text'  => 'required|min:20|max:500',
            'email' => 'required|min:5|max:255|email',
            'name'  => 'required|min:2|max:255',
        ]);
        $response = [
            'message' => 'Author undefined',
            'data'    => $this->viewData,
            'status'  => 500,
            'type'    => 'error',
        ];
        $post_id = $request->get('post_id', null);
        $post = $this->model->findOrFail($post_id);

        $post_author = $post->user;

        if (!$post_author) {
            if ($request->ajax()) {
                return response($response);
            }

            return back()->with($response['type'], $response['message']);
        }

        $post->increment('respond_count');

        $mail_data = [
            'text'  => $request->get('text', null),
            'email' => $request->get('email', null),
            'name'  => $request->get('name', null),
        ];

        $post_author->notify(new NewMessageToPostAuthor($post_author, $post, $mail_data));

        $response = [
            'message' => 'Сообщение успешно отправлено. Ожидайте ответа',
            'data'    => $this->viewData,
            'status'  => 200,
            'type'    => 'success',
        ];

        if ($request->ajax()) {
            return response($response);
        }

        return back()->with($response['type'], $response['message']);
    }
}
