<?php

namespace App\Http\Controllers;

use App\Models\Breed;
use Illuminate\Http\Request;

class BreedController extends Controller
{
    /**
     * @var Breed
     */
    private $model;

    public function __construct(Breed $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $region_id = $request->get('breed_type_id', null);
        $this->viewData['status'] = 'ok';
        $this->viewData['data']['breeds'] = $this->model->where('breed_type_id', $region_id)->ordered()->get();

        return response()->json($this->viewData);
    }
}
