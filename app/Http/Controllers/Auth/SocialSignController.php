<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserInfo;
use App\Models\UserSocial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Auth;

class SocialSignController extends Controller
{

    /**
     * @var User
     */
    private $user;
    public $redirectTo = 'profile';

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @param $provider
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        $socialite = Socialite::driver($provider);

//        if ($provider == 'facebook') {
//            $socialite = $socialite->setScopes('picture');
//        }
//        dd($socialite);

        return $socialite->redirect();
    }

    /**
     * Obtain the user information from Social provider.
     *
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            info($e->getMessage());
            return back()->with('error', 'Ошибка.Что-то пошло не так.');
        }
        $u_id = $user->getId();

        if(empty($user->getName())) {
            $providerName = explode('@', $user->getEmail())[0];
        } else {
            $providerName = $user->getName();
        }

        $providerEmail = $user->getEmail();
        $providerAvatar = $user->getAvatar();


        if (isset($providerEmail) && $providerEmail != '') {

            $userSocial = UserSocial::where([['provider', '=', $provider], ['social_id', '=', $u_id]])->first();
            if(auth()->check()) {
                $checkUser = auth()->user();
                if(!empty($userSocial) && $userSocial->user_id != $checkUser->id) {
                    return redirect()->route('profile.index')->withError('Социальная сеть уже привязана к другой учётной записи.');
                }
            } else {
                if(empty($userSocial)) {
                    $checkUser = User::where('email', $providerEmail)->first();
                } else {
                    $checkUser = $userSocial->user;
                }
            }

            if($checkUser) {
                $userSocial = $checkUser->socials()->firstOrNew(['provider' => $provider]);
                if(empty($userSocial->id)) {
                    $userSocial->social_id = $u_id;
                    if ($provider == 'facebook') {
                        $userSocial->profile_link = 'https://facebook.com/' . $user->id;
                    }
                    $userSocial->save();
                } elseif(Auth::user()) {
                    return redirect()->route('profile.index')->withError('Для привязки новой соцсети, нужно отвязать старую.');
                }

                $checkUser->update();
                Auth::login($checkUser);

                if(Auth::user()) {
                    if(empty($userSocial->id)) {
                        return redirect()->route('profile.index')->withSuccess('Соцсеть успешно добавлена');
                    } else {
                        return redirect()->route('profile.index');
                    }
                }

                return redirect()->to($this->redirectTo)->withError('Ошибка. Попробуйте позже.');
            } else {

                $data['name'] = $providerName ?? '';
                $data['email'] = $providerEmail;
                $data['phone'] = null;
                $data['avatar'] = $providerAvatar ?? null;
                $data['token'] = str_random(32);
                $pass = str_random(25);
                $data['password'] = $pass;

                $regged_user = User::create($data);

                $info = UserInfo::firstOrCreate([
                    'user_id' => $regged_user->id,
                ]);
                $info->update([
                    'last_login' => Carbon::now(),
                    'user_agent' => request()->server('HTTP_USER_AGENT'),
                    'ip_address' => request()->ip(),
                ]);

                if($regged_user) {
                    $userSocial = $regged_user->socials()->firstOrNew(['provider' => $provider]);
                    $userSocial->social_id = $u_id;
                    if ($provider == 'facebook') {
                        $userSocial->profile_link = 'https://facebook.com/' . $user->id;
                    }
                    $userSocial->save();


                    return redirect()->route('profile.index')->withSuccess('Спасибо за регистрацию');
                } else {
                    return redirect()->to($this->redirectTo)->withError('Ошибка. Попробуйте позже.');
                }
            }

        } else {
            $userSocial = UserSocial::where([['provider', '=', $provider], ['social_id', '=', $u_id]])->first();

            $checkUser = auth()->user();
            if ($userSocial && $userSocial->user) {
                $checkUser = $userSocial->user;
            }

            if(!empty($checkUser)) {
                $userSocial = $checkUser->socials()->firstOrNew(['provider' => $provider]);
                if(empty($userSocial->id)) {
                    $userSocial->social_id = $u_id;
                    if ($provider == 'facebook') {
                        $userSocial->profile_link = 'https://facebook.com/' . $user->id;
                    }
                    $userSocial->save();
                    return redirect()->route('profile.index')->withSuccess('Соцсеть успешно добавлена');
                } elseif(!auth()->user() && !empty($userSocial->id)) {
                    Auth::login($checkUser);

                    if(Auth::user()) {
                        return redirect()->route('profile.index');
                    }
                } else {
                    return back()->withError('Для привязки новой соцсети, нужно отвязать старую.');
                }
            }

            return redirect('login')->withError('В Вашем аккаунте нет Email адреса. К сожалению, email адрес обязателен. Рекомендуем пройти обычную регистрацию или заполнить email в профиле соцсети.');
        }
    }
}
