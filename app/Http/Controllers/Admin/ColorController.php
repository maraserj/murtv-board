<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    private $model;

    public function __construct(Color $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $this->viewData['colors'] = $this->model->orderBy('title')->get();

        return view('admin.colors.index', $this->viewData);
    }

    public function create()
    {
        $this->viewData['color'] = $this->model;

        return view('admin.colors.create_edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'unique:colors,title']);
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);

        $model = $this->model->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.colors.index')->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->viewData['color'] = $this->model->findOrFail($id);

        return view('admin.colors.create_edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);
        $breed = $this->model->findOrFail($id);
        $model = $breed->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.colors.index')->with('success', __('flash.updated'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function destroy($id)
    {
        $breed = $this->model->findOrFail($id);
        if ($breed->delete($id)) {
            return back()->with('success', __('flash.deleted'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }
}
