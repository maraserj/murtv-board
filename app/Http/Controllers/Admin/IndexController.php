<?php

namespace App\Http\Controllers\Admin;

use App\Services\PostsGrabber\PostsGrabber;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;

class IndexController extends Controller
{
    /**
     * @var PostsGrabber
     */
    private $postsGrabber;

    public function __construct(PostsGrabber $postsGrabber)
    {
        $this->postsGrabber = $postsGrabber;
    }

    public function index()
    {
        return redirect()->route('admin.users.index');
    }

    public function robots(Filesystem $filesystem)
    {
        $path = 'robots.txt';
        $this->viewData['robots'] = '';

        if($filesystem->exists($path)) {
            try {
                $this->viewData['robots'] = $filesystem->get($path);
            } catch (FileNotFoundException $e) {
                return back()->with('error', $e->getMessage());
            }
        }

        return view('admin.robots', $this->viewData);
    }

    public function storeRobots(Request $request, Filesystem $filesystem)
    {
        $path = 'robots.txt';

        if($filesystem->put($path, $request->get('robots', ''))) {
            return back()->with('success', __('flash.updated'));
        } else {
            return back()->withInput($request->all())->with('error', __('flash.error'));
        }

    }

    public function grabber()
    {
        return view('admin.grabber', $this->viewData);
    }

    public function grabberRun(Request $request)
    {
        $offset = $request->get('offset', null);
        $limit = $request->get('limit', null);
        try {
            $data = $this->postsGrabber->runGrabber($offset, $limit);
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
            info('Posts grabbing error: ', [$exception->getMessage()]);
        }

        return response()->json(['data' => $data ?? []]);
    }
}
