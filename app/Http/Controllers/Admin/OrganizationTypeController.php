<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use App\Models\OrganizationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationTypeController extends Controller
{
    private $model;

    public function __construct(OrganizationType $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $this->viewData['organizationTypes'] = $this->model->orderBy('title')->get();

        return view('admin.organization-types.index', $this->viewData);
    }

    public function create()
    {
        $this->viewData['organizationType'] = $this->model;

        return view('admin.organization-types.create_edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'unique:organization_types,title']);
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);

        $model = $this->model->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.organization-types.index')->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->viewData['organizationType'] = $this->model->findOrFail($id);

        return view('admin.organization-types.create_edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);
        $breed = $this->model->findOrFail($id);
        $model = $breed->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.organization-types.index')->with('success', __('flash.updated'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function destroy($id)
    {
        $breed = $this->model->findOrFail($id);
        if ($breed->delete($id)) {
            return back()->with('success', __('flash.deleted'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }
}
