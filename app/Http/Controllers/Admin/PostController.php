<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BreedType;
use App\Models\Category;
use App\Models\Color;
use App\Models\Country;
use App\Models\Organization;
use App\Models\Post;
use App\Models\Region;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $model;

    /**
     * PostController constructor.
     * @param PostRepository $model
     */
    public function __construct(PostRepository $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 20);
        $params = $request->except('perPage');


//        $this->viewData['posts'] = $this->model->with('photos')->orderBy('created_at', 'desc')->paginate($perPage);
        $this->viewData['posts'] = $this->model->filter($params, false, false)->paginate($perPage);

        return view('admin.posts.list', $this->viewData);
    }

    public function create()
    {
        $this->viewData['organizations'] = Organization::active()->orderBy('title')->get();
        $this->viewData['post_types'] = $this->model->getAllPostTypes();
        $this->viewData['post'] = $this->model->instance();
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['regions'] = Region::active()->get();
        $this->viewData['breed_types'] = BreedType::orderby('title')->get();
        $this->viewData['colors'] = Color::orderby('title')->get();
        $this->viewData['categories'] = Category::active()->forSale()->orderby('title')->get();

        return view('admin.posts.create_edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $not_in = '';
        $category_not_in = '';
        if (!$request->get('is_for_sale')) {
            $not_in = '|not_in:0';
        } else {
            $category_not_in = '|not_in:0';
        }
        $rules = [
            'post_type_id'  => 'required_without:is_for_sale' . $not_in,
            'gender'        => 'required_without:is_for_sale' . $not_in,
            'breed_type_id' => 'required_without:is_for_sale' . $not_in,
            'breed_id'      => 'required_without:is_for_sale' . $not_in,
            'category_id'   => 'required_with:is_for_sale' . $category_not_in,
//            'country_id'    => 'required|not_in:0',
//            'region_id'     => 'required|not_in:0',
            'city_id'       => 'required|not_in:0',
            'title'         => 'required|min:10|max:90',
            'description'   => 'required|min:20|max:2500',
        ];

        if ($request->ajax() && $request->get('validate')) {
            return $this->validate($request, $rules);
        }
        $this->validate($request, $rules);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $post = $this->model->create($data);
        if ($post) {
            if (!empty($request->allFiles()) && !empty($request->allFiles()['images'])) {
                $this->model->createImage($post, $request->allFiles()['images']);
            }
            return redirect()->route('admin.posts.index')->with('success', __('flash.created'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }

    public function show(Post $post)
    {
        //
    }

    public function edit($id)
    {
        $this->viewData['organizations'] = Organization::active()->orderBy('title')->get();
        $this->viewData['post_types'] = $this->model->getAllPostTypes();
        $this->viewData['post'] = $this->model->findOrFail($id);
        $this->viewData['categories'] = Category::active()->forSale()->orderby('title')->get();
        if ($this->viewData['post']->user_id != auth()->id() && !auth()->user()->can('admin')) {
            abort(404);
        }
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['regions'] = Region::active()->get();
        $this->viewData['breed_types'] = BreedType::orderby('title')->get();
        $this->viewData['colors'] = Color::orderby('title')->get();

        return view('admin.posts.create_edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax() && $request->get('changeStatus')) {
            $post = $this->model->findOrFail($id)->update(['status' => $request->get('status')]);
            if ($post) {
                return response()->json(['status' => 'ok', 'message' => __('flash.updated')]);
            } else {
                return response()->json(['status' => 'fail']);
            }
        }
        $not_in = '';
        $category_not_in = '';
        if (!$request->get('is_for_sale')) {
            $not_in = '|not_in:0';
        } else {
            $category_not_in = '|not_in:0';
        }
        $rules = [
            'post_type_id'  => 'required_without:is_for_sale' . $not_in,
            'gender'        => 'required_without:is_for_sale' . $not_in,
            'breed_type_id' => 'required_without:is_for_sale' . $not_in,
            'breed_id'      => 'required_without:is_for_sale' . $not_in,
            'category_id'   => 'required_with:is_for_sale' . $category_not_in,
//            'country_id'    => 'required|not_in:0',
//            'region_id'     => 'required|not_in:0',
            'city_id'       => 'required|not_in:0',
            'title'         => 'required|min:10|max:90',
            'description'   => 'required|min:20|max:2500',
        ];

        if ($request->ajax() && $request->get('validate')) {
            return $this->validate($request, $rules);
        }
        $this->validate($request, $rules);
        $this->viewData['post'] = $this->model->findOrFail($id);
        $data = $request->all();
//        $data['user_id'] = auth()->id();
        if ($this->viewData['post']->user_id != auth()->id()) {
            if (!auth()->user()->can('admin')) {
                abort(404);
            }
        }
        $post = $this->model->update($id, $data);
        if (!empty($request->allFiles()) && !empty($request->allFiles()['images'])) {
            $this->model->createImage($post, $request->allFiles()['images']);
        }

        if ($post) {
            return redirect()->route('admin.posts.index')->with('success', __('flash.updated'));
        }

        return back()->withInput($data)->with('error', __('flash.error'));
    }


    public function destroy($id)
    {
        $post = $this->model->findOrFail($id);
        if (auth()->id() != $post->user_id && !auth()->user()->can('admin')) {
            return back()->with('error', __('flash.not_found'));
        } else {
            $this->model->deletePhotos($post);
            if ($post->delete()) {
                return back()->with('success', __('flash.deleted'));
            } else {
                return back()->with('error', __('flash.error'));
            }
        }
    }

    public function addToFavourites(Request $request)
    {
        $post = $this->model->findOrFail($request->get('post_id', 0));

        $favourite = $post->favourites()->firstOrCreate(['user_id' => auth()->id()]);

        if ($favourite) {
            return response()->json(['status' => 'ok', 'message' => __('flash.created')]);
        }

        return response()->json(['status' => 'fail'], 400);
    }

    public function removeFromFavourites(Request $request)
    {
        $post = $this->model->findOrFail($request->get('post_id', 0));

        $favourite = $post->favourites()->where(['user_id' => auth()->id()])->firstOrFail();

        if ($favourite->delete()) {
            if (!$request->ajax()) {
                return back()->with('success', __('flash.delete'));
            }
            return response()->json(['status' => 'ok', 'message' => __('flash.delete')]);
        }

        if (!$request->ajax()) {
            return back()->with('error', __('flash.error'));
        }
        return response()->json(['status' => 'fail'], 400);
    }
}
