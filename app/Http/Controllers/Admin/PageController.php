<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Pages\StorePageRequest;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $model;
    public function __construct(Page $page)
    {
        $this->model = $page;
    }

    public function index()
    {
        $pages = $this->model->all();
        return view('admin.pages.index', ['pages' => $pages]);
    }

    public function create()
    {
        $page = new Page();
        return view('admin.pages.create_edit', ['page' => $page]);
    }

    public function store(StorePageRequest $request)
    {
        $data = $request->except(['_token', '_method']);
        if(!isset($data['footer'])) $data['footer'] = 0;
        if(empty($data['slug'])) $data['slug'] = str_slug($data['title']);
        if(empty($data['seo_title'])) $data['seo_title'] = $data['title'];
        if(empty($data['seo_h1'])) $data['seo_h1'] = $data['title'];
        if(empty($data['seo_description'])) $data['seo_description'] = $data['title'];

        $page = new Page();
        $page->fill($data);
        if ($page->save()) {
            return redirect()->route('admin.pages.index', ['id' => $page->id])->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $page = $this->model->find($id);
        return view('admin.pages.create_edit', ['page' => $page]);
    }

    public function update(Request $request, $id)
    {
        $page = $this->model->find($id);
        $data = $request->except(['_token', '_method']);
        if ($request->ajax() && isset($data['active'])) {
            $page->fill($data);
            if ($page->save()) {
                return response(['status' => 'ok', 'type' => 'success', 'message' => __('flash.updated')]);
            } else {
                return response(['status' => 'error', 'type' => 'error', 'message' => __('flash.error')]);
            }
        }
        if(!isset($data['footer'])) $data['footer'] = 0;
        if(empty($data['slug'])) $data['slug'] = str_slug($data['title']);
        if(empty($data['seo_title'])) $data['seo_title'] = $data['title'];
        if(empty($data['seo_h1'])) $data['seo_h1'] = $data['title'];
        if(empty($data['seo_description'])) $data['seo_description'] = $data['title'];
        $page->fill($data);
        if ($page->save()) {
            return back()->with('success', 'Страница успешно изменена');
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function delete($id)
    {
        $page = $this->model->find($id);
        if ($page->delete()) {
            return back()->with('success', __('flash.deleted'));
        } else {
            return back()->with('error', __('flash.error'));
        }

    }
}
