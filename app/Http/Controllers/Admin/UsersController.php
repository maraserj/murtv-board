<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Users\StoreUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Http\Requests\Users\UsersStoreRequest;
use App\Http\Requests\Users\UsersUpdateRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $this->viewData['users'] = $this->user->all($request->all());
        return view('admin.users.list', $this->viewData);
    }

    public function create()
    {
        $this->viewData['user'] = $this->user->instance();
        return view('admin.users.create_edit', $this->viewData);
    }

    public function edit($id)
    {
        $this->viewData['user'] = $this->user->getById($id);
        return view('admin.users.create_edit', $this->viewData);
    }

    public function store(StoreUserRequest $request)
    {
        $user = $this->user->create($request->except(['_token']));
        if ($user) {
            if(!$request->ajax()) {
                return back()->with('success', __('flash.created'));
            }
        } else if(!$request->ajax()) {
            return back()->with('error', __('flash.error'));
        }

        return back();
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $data = [];
        if ($request->exists('password') && !$request->get('password')) {
            $request->except(['_token', 'password']);
        } else {
            $data = $request->except(['_token']);
        }
        $user = $this->user->update($id, $data);
        if ($user) {
            if(!$request->ajax()) {
                return back()->with('success', __('flash.updated'));
            }
        } else if(!$request->ajax()) {
            return back()->with('error', __('flash.error'));
        }
    }

    public function destroy($id)
    {
        if ($this->user->delete($id)) return back()->with('success', __('flash.deleted'));
        else return back()->with('error', __('flash.error'));
    }
}
