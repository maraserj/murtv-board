<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Repositories\RegionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    /**
     * @var RegionRepository
     */
    private $model;

    public function __construct(RegionRepository $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $params['perPage'] = 50;

        $this->viewData['countries'] = Country::all();
        $this->viewData['regions'] = $this->model->filter($params);


        return view('admin.regions.index', $this->viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['region'] = $this->model->instance();

        return view('admin.regions.create-edit', $this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
        ]);

        $model = $this->model->create($request->all());

        if ($model) {
            return redirect()->route('admin.regions.index')->with('success', __('flash.created'));
        }

        return back()->with('error', __('flash.error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->viewData['countries'] = Country::active()->get();
        $this->viewData['region'] = $this->model->findOrFail($id);

        return view('admin.regions.create-edit', $this->viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($request->get('changeStatus'))) {
            $this->validate($request, [
                'name' => 'required',
                'country_id' => 'required',
            ]);
        }

        $model = $this->model->update($id, $request->all());

        if ($model) {
            if ($request->ajax()) {
                return response()->json(['status' => 'success', 'message' => __('flash.updated')]);
            }
            return redirect()->route('admin.regions.index')->with('success', __('flash.updated'));
        }

        if ($request->ajax()) {
            return response()->json(['status' => 'error', 'message' => __('flash.error')]);
        }

        return back()->with('error', __('flash.error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->delete($id);

        return back()->with('success', __('flash.deleted'));
    }
}
