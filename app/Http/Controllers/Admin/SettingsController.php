<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\StoreSettingRequest;
use App\Models\SettingGroup;
use App\Repositories\SettingsRepository;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    protected $settings;

    public function __construct(SettingsRepository $settingsRepository)
    {
        $this->settings = $settingsRepository;
    }

    public function index()
    {
        $groups = SettingGroup::with('settings')->orderBy('created_at', 'desc')->get();
        return view('admin.settings.index', [
            'groups' => $groups,
        ]);
    }

    public function create()
    {
        $groups = SettingGroup::with('settings')->orderBy('created_at', 'desc')->get();
        return view('admin.settings.create_edit', [
            'groups' => $groups,
            'setting' => $this->settings->getModel(),
        ]);
    }

    public function store(StoreSettingRequest $request)
    {
        $setting = $this->settings->create($request->except(['_token']));
        if ($request->file('image')) {
            $image = $this->settings->uploadSettingImage($setting, $request->file('image'), $request->get('width', null), $request->get('height', null), 'save');
            if ($image) {
                $setting->update(['value' => $image]);
            }
        }
        if ($setting) {
            return redirect()->route('admin.settings.index')->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function edit($id)
    {
        $groups = SettingGroup::with('settings')->orderBy('created_at', 'desc')->get();
        return view('admin.settings.create_edit', [
            'groups' => $groups,
            'setting' => $this->settings->find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $response = [];
            $setting = $this->settings->find($id);
            $setting->value = $request->value;
            if ($setting->save()) {
                $response['status'] = 'ok';
                $response['message'] = __('flash.updated');
                $response['type'] = 'success';
            } else {
                $response['status'] = 'fail';
                $response['message'] = __('flash.error');
                $response['type'] = 'error';
            }
            return response($response);
        } else {
            $setting = $this->settings->update($id, $request->except(['_token']));
            if ($request->file('image')) {
                $image = $this->settings->uploadSettingImage($setting, $request->file('image'), $request->get('width'), $request->get('height'), 'update');
                if ($image) {
                    $setting->update(['value' => $image]);
                }
            }
            if ($setting) {
                return redirect()->route('admin.settings.index')->with('success', __('flash.updated'));
            } else {
                return back()->with('error', __('flash.error'));
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            if ($this->settings->delete($id)) {
                $response['status'] = 'ok';
                $response['message'] = __('flash.deleted');
                $response['type'] = 'success';
            } else {
                $response['status'] = 'fail';
                $response['message'] = __('flash.error');
                $response['type'] = 'error';
            }

            return response($response);
        } else {
            if ($this->settings->delete($id)) {
                return back()->with('success', __('flash.deleted'));
            } else {
                return back()->with('error', __('flash.error'));
            }
        }
    }
}
