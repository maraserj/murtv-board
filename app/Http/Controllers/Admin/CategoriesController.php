<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Categories\DeleteCategoryRequest;
use App\Http\Requests\Categories\StoreCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use App\Http\Controllers\Controller;
use App\Services\ImageUpload;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public $viewData = [];
    protected $repo;
    /**
     * @var ImageUpload
     */
    private $imageUpload;

    /**
     * CategoriesController constructor.
     * @param CategoryRepository $repo
     * @param ImageUpload $imageUpload
     */
    public function __construct(CategoryRepository $repo, ImageUpload $imageUpload)
    {
        $this->repo = $repo;
        $this->imageUpload = $imageUpload;
    }

    public function index()
    {

        $this->viewData['categories'] = $this->repo->all();

        return view('admin.categories.list', $this->viewData);
    }

    public function edit($id)
    {
        $this->viewData['item'] = $this->repo->findOrFail($id);
        if (!$this->viewData['item']) abort(404);
        $this->viewData['categories'] = $this->repo->getMain();

        return view('admin.categories.item', $this->viewData);
    }

    public function create()
    {
        $this->viewData['item'] = $this->repo->getModel();
        $this->viewData['categories'] = $this->repo->all();

        return view('admin.categories.item', $this->viewData);
    }

    public function store(StoreCategoryRequest $request)
    {
        //todo add base image width for categories image
        $category = $this->repo->create($request->except(['_token', 'image']));
        if ($category) {
            if ($request->file('image')) {
                $this->imageUpload->baseFolder = 'categories';
                $imagePath = $this->imageUpload->save($request->file('image'), 800);
                if ($imagePath) {
                    $this->repo->update($category->id, ['image' => $imagePath]);
                }
            } else if ($request->get('image')) {
                $this->imageUpload->baseFolder = 'categories';
                $imagePath = $this->imageUpload->save($request->get('image'), 800);
                if ($imagePath) {
                    $this->repo->update($category->id, ['image' => $imagePath]);
                }
            }

            return redirect()->route('admin.categories.index')->with('success', __('flash.created'));
        } else {
            return back()->withInput()->with('error', __('flash.error'));
        }
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $data = $request->except(['_token', 'image']);
        $category = $this->repo->find($id);

        if ($request->file('image')) {
            $this->imageUpload->baseFolder = 'categories';
            $imagePath = $this->imageUpload->save($request->file('image'), 800, null, 'update', $category ? $category->image : '');
            if ($imagePath) {
                $this->repo->update($category->id, ['image' => $imagePath]);
            }
        } else if ($request->get('image')) {
            $this->imageUpload->baseFolder = 'categories';
            $imagePath = $this->imageUpload->save($request->get('image'), 800, null, 'update', $category ? $category->image : '');
            if ($imagePath) {
                $this->repo->update($category->id, ['image' => $imagePath]);
            }
        }

        $category = $this->repo->update($id, $data);
        if ($category) {
            if ($request->ajax()) {
                return response(['status' => 'success', 'message' => __('flash.updated')]);
            }

            return redirect()->route('admin.categories.index')->with('success', __('flash.updated'));
        } else {
            if ($request->ajax()) {
                return response(['status' => 'danger', 'message' => __('flash.error')]);
            }

            return back()->withInput($request->all())->with('error', __('flash.error'));
        }
    }

    public function destroy(DeleteCategoryRequest $request, $id)
    {
        $category = $this->repo->find($id);
        if ($category) {
            $this->imageUpload->deleteImage($category->image);
        }
        if ($category->delete()) {
            return redirect()->route('admin.categories.index')->with('success', __('flash.deleted'));
        } else {
            return back()->withInput()->with('error', __('flash.error'));
        }
    }

    public function deleteImg(Request $request)
    {
        $category = $this->repo->find($request->get('key', 0));
        $category->update(['image' => null]);
        if ($category) {
            $deleted = $this->imageUpload->deleteImage($category->image);
            if ($deleted) {
                return response(['status' => 'success', 'message' => __('flash.deleted')]);
            } else {
                return response(['status' => 'danger', 'message' => __('flash.error')]);
            }
        } else {
            return response(['status' => 'error', 'message' => __('flash.not_found')], 404);
        }

    }

    public function active(UpdateCategoryRequest $request)
    {
        $response = [];

        if ($this->repo->changeStatus($request->except(['_token']))) {
            $response['status'] = 'success';
            $response['message'] = __('flash.updated');
        } else {
            $response['status'] = 'danger';
            $response['message'] = __('flash.error');
        }

        return response()->json($response);
    }
}
