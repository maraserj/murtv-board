<?php

namespace App\Http\Controllers\Admin;

use App\Models\Breed;
use App\Models\BreedType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BreedController extends Controller
{
    private $breed;
    private $breedType;

    public function __construct(Breed $breed, BreedType $breedType)
    {
        $this->breed = $breed;
        $this->breedType = $breedType;
    }

    public function index()
    {
        $this->viewData['breedTypes'] = $this->breedType->with('breeds')->get();

        return view('admin.breeds.index', $this->viewData);
    }

    public function create()
    {
        $this->viewData['breedTypes'] = $this->breedType->all();
        $this->viewData['breed'] = $this->breed;

        return view('admin.breeds.create_edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);

        $model = $this->breed->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.breeds.index')->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->viewData['breedTypes'] = $this->breedType->all();
        $this->viewData['breed'] = $this->breed->findOrFail($id);

        return view('admin.breeds.create_edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);
        $breed = $this->breed->findOrFail($id);
        $model = $breed->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.breeds.index')->with('success', __('flash.updated'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function destroy($id)
    {
        $breed = $this->breed->findOrFail($id);
        if ($breed->delete($id)) {
            return back()->with('success', __('flash.deleted'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }
}
