<?php

namespace App\Http\Controllers\Admin;

use App\Models\BreedType;
use App\Services\ImageUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BreedTypeController extends Controller
{
    private $breedType;
    /**
     * @var ImageUpload
     */
    private $imageUpload;

    public function __construct(BreedType $breedType, ImageUpload $imageUpload)
    {
        $this->breedType = $breedType;
        $this->imageUpload = $imageUpload;
        $this->imageUpload->baseFolder = 'breed-types';
    }

    public function index()
    {
        $this->viewData['breedTypes'] = $this->breedType->withCount('breeds')->orderBy('title')->get();

        return view('admin.breed-types.index', $this->viewData);
    }

    public function create()
    {
        $this->viewData['breedType'] = $this->breedType;

        return view('admin.breed-types.create_edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $data = $request->except('image');
        $data['slug'] = str_slug($data['title']);

        $model = $this->breedType->fill($data);
        if ($model->save()) {
            if ($request->file('image')) {
                $imagePath = $this->imageUpload->save($request->file('image'));
                if ($imagePath) {
                    $model->update(['image' => $imagePath]);
                }
            } else if ($request->get('image')) {
                $imagePath = $this->imageUpload->save($request->get('image'));
                if ($imagePath) {
                    $model->update(['image' => $imagePath]);
                }
            }
            return redirect()->route('admin.breed-types.index')->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->viewData['breedType'] = $this->breedType->findOrFail($id);

        return view('admin.breed-types.create_edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $data = $request->except('image');
        $data['slug'] = str_slug($data['title']);
        $breed = $this->breedType->findOrFail($id);
        $model = $breed->fill($data);
        if ($model->save()) {
            if ($request->file('image')) {
                $imagePath = $this->imageUpload->save($request->file('image'), null, null, 'update', $model ? $model->image : '');
                if ($imagePath) {
                    $model->update(['image' => $imagePath]);
                }
            } else if ($request->get('image')) {
                $imagePath = $this->imageUpload->save($request->get('image'), null, null, 'update', $model ? $model->image : '');
                if ($imagePath) {
                    $model->update(['image' => $imagePath]);
                }
            }

            return redirect()->route('admin.breed-types.index')->with('success', __('flash.updated'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function destroy($id)
    {
        $breed = $this->breedType->findOrFail($id);
        if ($breed->delete($id)) {
            return back()->with('success', __('flash.deleted'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }
}
