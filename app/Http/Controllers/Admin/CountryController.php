<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\CountryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    /**
     * @var CountryRepository
     */
    private $model;

    public function __construct(CountryRepository $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $this->viewData['countries'] = $this->model->paginate(60);

        return view('admin.countries.index', $this->viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->viewData['country'] = $this->model->instance();

        return view('admin.countries.create-edit', $this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $model = $this->model->create($request->all());

        if ($model) {
            return redirect()->route('admin.countries.index')->with('success', __('flash.created'));
        }

        return back()->with('error', __('flash.error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->viewData['country'] = $this->model->findOrFail($id);

        return view('admin.countries.create-edit', $this->viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($request->get('changeStatus'))) {
            $this->validate($request, [
                'name' => 'required',
            ]);
        }

        $model = $this->model->update($id, $request->all());

        if ($model) {
            if ($request->ajax()) {
                return response()->json(['status' => 'success', 'message' => __('flash.updated')]);
            }
            return redirect()->route('admin.countries.index')->with('success', __('flash.updated'));
        }

        if ($request->ajax()) {
            return response()->json(['status' => 'error', 'message' => __('flash.error')]);
        }

        return back()->with('error', __('flash.error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->delete($id);

        return back()->with('success', __('flash.deleted'));
    }
}
