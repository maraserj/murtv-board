<?php

namespace App\Http\Controllers\Admin;

use App\Models\Breed;
use App\Models\BreedType;
use App\Models\PostType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostTypeController extends Controller
{
    private $model;

    public function __construct(PostType $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $this->viewData['postTypes'] = $this->model->withCount('posts')->get();

        return view('admin.post-types.index', $this->viewData);
    }

    public function create()
    {
        $this->viewData['postType'] = $this->model;

        return view('admin.post-types.create_edit', $this->viewData);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);

        $model = $this->model->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.post-types.index')->with('success', __('flash.created'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->viewData['postType'] = $this->model->findOrFail($id);

        return view('admin.post-types.create_edit', $this->viewData);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);
        $breed = $this->model->findOrFail($id);
        $model = $breed->fill($data);
        if ($model->save()) {
            return redirect()->route('admin.post-types.index')->with('success', __('flash.updated'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }

    public function destroy($id)
    {
        $breed = $this->model->findOrFail($id);
        if ($breed->delete($id)) {
            return back()->with('success', __('flash.deleted'));
        } else {
            return back()->with('error', __('flash.error'));
        }
    }
}
