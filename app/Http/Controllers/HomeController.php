<?php

namespace App\Http\Controllers;

use App\Models\BreedType;
use App\Models\Country;
use App\Models\Favourite;
use App\Models\Page;
use App\Models\Post;
use App\Models\PostType;
use App\Models\Region;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Storage;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->viewData['posts'] = Post::active()->with('photos')->orderBy('created_at', 'desc')->limit(5)->get();
        $this->viewData['post_types'] = PostType::orderBy('title')->get();
        $this->viewData['favourites_array'] = [];
        if (auth()->check()) {
            $this->viewData['favourites_array'] = Favourite::getFavouritePostIdsForUser(auth()->id());
        }
        return view('index', $this->viewData);
    }

    public function showStaticPage($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();

        return view('static', ['page' => $page]);
    }

    public function sitemap()
    {
        $file = null;

        try {
            $file = Storage::disk('public')->get('sitemap.xml');
        } catch (FileNotFoundException $exception) {
            abort(404, $exception->getMessage());
        }
        $type = Storage::disk('public')->mimeType('sitemap.xml');

        return response()->make($file, 200, ['Content-Type' => $type]);
    }
}
