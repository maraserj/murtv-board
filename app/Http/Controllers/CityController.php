<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * @var City
     */
    private $model;

    public function __construct(City $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $this->viewData['status'] = 'ok';
        $cities = [];
        if ($search) {
            $cities = $this->model->active()->where('name', 'like',  $search . '%')->orderBy('name')->get();
        }
        if (is_array($cities)) {
            $cities = collect($cities);
        }

        $this->viewData['data']['cities'] = $cities->each(function ($item) {
            $item->label = $item->name . ($item->area ? ', ' . $item->area : '');
        })->all();

        return response()->json($this->viewData);
    }

    public function getRegionsByCountry()
    {
        $region_id = request('region_id', null);
        $search = request('search', null);
        $this->viewData['status'] = 'ok';
        $cities = [];
        if ($search) {
            $cities = $this->model->active()->where('region_id', $region_id)->where('name', 'like',  '%' . $search . '%')->orderBy('name')->get();
        } elseif ($region_id) {
            $cities = [];
//            $cities = $this->model->active()->where('region_id', $region_id)->orderBy('name')->limit(50)->get();
        }
        if (is_array($cities)) {
            $cities = collect($cities);
        }

        $this->viewData['data']['cities'] = $cities->each(function ($item) {
            $item->label = $item->name . ($item->area ? ', ' . $item->area : '');
        })->all();

        return response()->json($this->viewData);
    }

    public function show($id)
    {
        $this->viewData['status'] = 'ok';
        $this->viewData['city'] = $this->model->findOrFail($id);
        $this->viewData['city']->label = $this->viewData['city']->name . ($this->viewData['city']->area ? ', ' . $this->viewData['city']->area : '');

        return response()->json($this->viewData);
    }
}
