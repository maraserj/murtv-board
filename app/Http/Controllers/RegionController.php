<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * @var Region
     */
    private $model;

    public function __construct(Region $model)
    {
        $this->model = $model;
    }

    public function getRegionsByCountry()
    {
        $country_id = request('country_id', null);
        $this->viewData['status'] = 'ok';
        $this->viewData['data']['regions'] = $this->model->where('country_id', $country_id)->orderBy('name')->get();

        return response()->json($this->viewData);
    }
}
