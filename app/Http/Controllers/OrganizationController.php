<?php

namespace App\Http\Controllers;

use App\Models\Favourite;
use App\Notifications\NewMessageToOrganization;
use App\Repositories\OrganizationRepository;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * @var OrganizationRepository
     */
    private $model;

    public function __construct(OrganizationRepository $model)
    {
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 15);
        $data = $request->all();
        $this->viewData['organization_types'] = $this->model->getAllTypes();

        if (!empty($data)) {
            $data['perPage'] = $perPage;
            $organizations = $this->model->filter($data);
        } else {
            $organizations = $this->model->active()->orderBy('created_at', 'desc');
        }
        $this->viewData['organizations_count'] = $organizations->count();
        $this->viewData['organizations'] = $organizations->paginate($perPage);

        return view('organizations.index', $this->viewData);
    }

    public function show(Request $request, $slug)
    {
        $organization = $this->model->findBy('slug', $slug);
        $this->viewData['organization'] = $organization;
        $this->viewData['organization_posts_count'] = $organization->posts()->count();
        $this->viewData['organization_posts'] = $organization->posts()->limit(9)->get();

        $this->viewData['favourites_array'] = [];
        if (auth()->check()) {
            $this->viewData['favourites_array'] = Favourite::getFavouriteOrganizationIdsForUser(auth()->id());
        }

        return view('organizations.show', $this->viewData);
    }

    public function submitMessage(Request $request)
    {
        $this->validate($request, [
            'text'  => 'required|min:20|max:500',
            'email' => 'required|min:5|max:255|email',
            'name'  => 'required|min:2|max:255',
        ]);
        $response = [
            'message' => 'Author undefined',
            'data'    => $this->viewData,
            'status'  => 500,
            'type'    => 'error',
        ];
        $organization_id = $request->get('organization_id', null);
        $organization = $this->model->findOrFail($organization_id);

        $organization_author = $organization->user;

        if (!$organization_author) {
            if ($request->ajax()) {
                return response($response);
            }

            return back()->with($response['type'], $response['message']);
        }

        $mail_data = [
            'text'  => $request->get('text', null),
            'email' => $request->get('email', null),
            'name'  => $request->get('name', null),
        ];

        $organization_author->notify(new NewMessageToOrganization($organization_author, $organization, $mail_data));

        $response = [
            'message' => 'Сообщение успешно отправлено. Ожидайте ответа',
            'data'    => $this->viewData,
            'status'  => 200,
            'type'    => 'success',
        ];

        if ($request->ajax()) {
            return response($response);
        }

        return back()->with($response['type'], $response['message']);
    }
}
