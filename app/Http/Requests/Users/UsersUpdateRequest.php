<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsersUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->route()->parameter('user');
        if ($this->request->get('from', 'web') == 'ajax') {
            return [];
        }
        $rules = [
            'name'     => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($userId),
            ],
//            'phone'    => 'required|min:5|string|max:255',
//            'site_url' => 'required|min:3|string|max:255',
        ];

        if (!empty($this->request->get('password', ''))) {
            $rules['password'] = 'required|string|min:6|confirmed';
        }

        return $rules;
    }
}
