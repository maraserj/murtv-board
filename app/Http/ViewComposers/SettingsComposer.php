<?php
namespace App\Http\ViewComposers;

use App\Models\Site;
use App\Repositories\SettingsRepository;
use App\Repositories\SiteRepository;
use App\Repositories\UserRepository;
use Illuminate\View\View;
use Auth;

class SettingsComposer
{
    protected $settings;

    public function __construct(SettingsRepository $settingsRepository)
    {
        $this->settings = $settingsRepository;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $settings = [];

        $seo = $this->settings->getSeo();
        $seo = $seo->mapWithKeys(function ($item) {
            return [$item->code => $item->value];
        })->all();

        $main = $this->settings->getMain();
        $main = $main->mapWithKeys(function ($item) {
            return [$item->code => $item->value];
        })->all();

        $settings['seo_settings'] = $seo;
        $settings['main_settings'] = $main;

        $view->with($settings);
    }
}
