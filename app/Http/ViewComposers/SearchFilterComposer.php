<?php
namespace App\Http\ViewComposers;

use App\Models\{
    BreedType, Color, Country
};
use Illuminate\View\View;

class SearchFilterComposer
{
    protected $data;

    public function __construct(Country $country, BreedType $breedType, Color $color)
    {
        $this->data['breed_types'] = $breedType->orderBy('title')->get();
        $this->data['countries'] = $country->active()->get();
        $this->data['colors'] = $color->orderBy('title')->get();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with($this->data);
    }
}
