<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BreedType
 *
 * @property int $id
 * @property string $title
 * @property string|null $slug
 * @property string|null $image
 * @property int|null $order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Breed[] $breeds
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BreedType ordered()
 */
class BreedType extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'image',
        'order',
    ];

    public function breeds()
    {
        return $this->hasMany(Breed::class, 'breed_type_id', 'id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderby('order', 'desc')->orderBy('title');
    }
}
