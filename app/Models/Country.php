<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $code
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Region[] $regions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $vk_country_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereVkCountryId($value)
 */
class Country extends Model
{
    protected $fillable = [
        'vk_country_id',
        'name',
        'slug',
        'code',
        'status',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', true)->orderBy('name');
    }

    public function regions()
    {
        return $this->hasMany(Region::class);
    }
}
