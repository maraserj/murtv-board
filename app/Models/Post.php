<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $name
 * @property string|null $type
 * @property string|null $gender
 * @property int|null $size
 * @property int|null $color_id
 * @property int|null $breed_type_id
 * @property int|null $breed_id
 * @property string|null $birthday
 * @property string|null $animal_id
 * @property int $sterilized
 * @property int $guarded
 * @property int $ill
 * @property int $deadly
 * @property int $featured
 * @property int|null $country_id
 * @property int|null $region_id
 * @property int|null $city_id
 * @property string|null $address
 * @property string|null $coordinates
 * @property string|null $title
 * @property string|null $slug
 * @property string|null $description
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Breed|null $breed
 * @property-read \App\Models\BreedType|null $breedType
 * @property-read \App\Models\City|null $city
 * @property-read \App\Models\Color|null $color
 * @property-read \App\Models\Country|null $country
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @property-read \App\Models\Region|null $region
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereAnimalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereBreedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereBreedTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCoordinates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereDeadly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereGuarded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereIll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSterilized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $organization_id
 * @property int|null $category_id
 * @property string|null $sale_type
 * @property int|null $post_type_id
 * @property int $respond_count
 * @property int $unique_view_count
 * @property int $view_count
 * @property int|null $price
 * @property int $has_home
 * @property-read \App\Models\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Favourite[] $favourites
 * @property-read \App\Models\Organization|null $organization
 * @property-read \App\Models\PostType|null $postType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereHasHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePostTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereRespondCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSaleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUniqueViewCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereViewCount($value)
 */
class Post extends Model
{
    protected $fillable = [
        'user_id',
        'organization_id',
        'name',
        'type',
        'gender',
        'size',
        'color_id',
        'breed_type_id',
        'breed_id',
        'birthday',
        'animal_id',
        'sterilized',
        'guarded',
        'ill',
        'deadly',
        'featured',
        'country_id',
        'region_id',
        'city_id',
        'address',
        'coordinates',
        'title',
        'slug',
        'description',
        'status',
        'post_type_id',
        'has_home',
        'price',
        'sale_type',
        'view_count',
        'unique_view_count',
        'respond_count',
        'category_id',
        'original_url',
        'is_from_parser',
    ];

    const SALE_TYPE = 'sale';

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function breedType()
    {
        return $this->belongsTo(BreedType::class);
    }

    public function breed()
    {
        return $this->belongsTo(Breed::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoble');
    }

    public function favourites()
    {
        return $this->morphMany(Favourite::class, 'favourited');
    }

    public function postType()
    {
        return $this->belongsTo(PostType::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


}
