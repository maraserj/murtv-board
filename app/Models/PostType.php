<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PostType
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $slug
 * @property string|null $code
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PostType extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'code',
        'description',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
