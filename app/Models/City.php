<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\City
 *
 * @property int $id
 * @property int|null $region_id
 * @property string $name
 * @property string $slug
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $vk_city_id
 * @property-read \App\Models\Region|null $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\City whereVkCityId($value)
 */
class City extends Model
{
    protected $fillable = [
        'vk_city_id',
        'region_id',
        'name',
        'area',
        'slug',
        'status',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
