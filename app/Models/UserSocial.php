<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserSocial
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $provider
 * @property string|null $profile_link
 * @property string|null $social_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereProfileLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereSocialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSocial whereUserId($value)
 * @mixin \Eloquent
 */
class UserSocial extends Model
{
    protected $fillable = [
        'user_id',
        'provider',
        'social_id',
        'profile_link',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
