<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Favourite
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $favourited_id
 * @property string $favourited_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $favourited
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favourite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favourite whereFavouritedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favourite whereFavouritedType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favourite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favourite whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favourite whereUserId($value)
 * @mixin \Eloquent
 */
class Favourite extends Model
{
    protected $fillable = [
        'user_id',
        'favourited_id',
    ];

    public function favourited()
    {
        return $this->morphTo();
    }

    public static function getFavouritePostIdsForUser($user_id)
    {
        return self::whereFavouritedType(Post::class)->where('user_id', $user_id)->pluck('favourited_id')->all();
    }

    public static function getFavouriteOrganizationIdsForUser($user_id)
    {
        return self::whereFavouritedType(Organization::class)->where('user_id', $user_id)->pluck('favourited_id')->all();
    }
}
