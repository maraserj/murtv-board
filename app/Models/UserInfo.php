<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserInfo
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $last_login
 * @property string|null $user_agent
 * @property string|null $ip_address
 * @property mixed|null $additional_info
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereAdditionalInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereUserId($value)
 * @mixin \Eloquent
 */
class UserInfo extends Model
{
    protected $fillable = [
        'user_id',
        'last_login',
        'user_agent',
        'ip_address',
        'additional_info',
    ];
}
