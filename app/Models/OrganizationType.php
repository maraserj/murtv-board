<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrganizationType
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Organization[] $organizations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrganizationType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrganizationType extends Model
{
    protected $fillable = [
        'title',
        'slug',
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
