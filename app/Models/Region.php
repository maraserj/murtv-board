<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Region
 *
 * @property int $id
 * @property int|null $country_id
 * @property string $name
 * @property string $slug
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\City[] $cities
 * @property-read \App\Models\Country|null $country
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $vk_region_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereVkRegionId($value)
 */
class Region extends Model
{
    protected $fillable = [
        'vk_region_id',
        'country_id',
        'name',
        'slug',
        'status',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
