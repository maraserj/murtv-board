<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property string|null $title
 * @property string $slug
 * @property string|null $description
 * @property string|null $seo_title
 * @property string|null $seo_h1
 * @property string|null $seo_description
 * @property string|null $seo_keywords
 * @property int $active
 * @property int $footer
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page footer()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereFooter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'description',
        'seo_title',
        'seo_h1',
        'seo_description',
        'seo_keywords',
        'active',
        'footer',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeFooter($query)
    {
        return $query->where('footer', true);
    }
}
