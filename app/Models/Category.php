<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $slug
 * @property string $title
 * @property string|null $image
 * @property string|null $description
 * @property string|null $seo_title
 * @property string|null $seo_h1
 * @property string|null $seo_description
 * @property string|null $seo_keywords
 * @property int $active
 * @property int $show_on_main
 * @property int $show_in_menu
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $child
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Favourite[] $favourites
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category main()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category menu()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereShowInMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereShowOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_for_sale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category forSale()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereIsForSale($value)
 */
class Category extends Model
{
    protected $fillable = [
        'parent_id',
        'slug',
        'title',
        'image',
        'description',
        'seo_title',
        'seo_h1',
        'seo_description',
        'seo_keywords',
        'active',
        'show_on_main',
        'show_in_menu',
        'big_caption',
        'is_for_sale',
    ];

    public function child()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function favourites()
    {
        return $this->morphMany(Favourite::class, 'favourited');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeForSale($query)
    {
        return $query->where('is_for_sale', true);
    }

    public function scopeMain($query)
    {
        return $query->where('show_on_main', true);
    }

    public function scopeMenu($query)
    {
        return $query->where('show_in_menu', true);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }
}
