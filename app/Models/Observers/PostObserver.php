<?php
namespace App\Models\Observers;

use App\Models\Post;

class PostObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * Listen to the Post deleting event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleting(Post $post)
    {
        $post->photos()->delete();
    }
}