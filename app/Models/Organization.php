<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Organization
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $organization_type_id
 * @property string|null $title
 * @property string|null $slug
 * @property int|null $country_id
 * @property int|null $region_id
 * @property int|null $city_id
 * @property string|null $address
 * @property string|null $description
 * @property string|null $youtube_link
 * @property string|null $contact_name
 * @property string|null $contact_surname
 * @property string|null $contact_email
 * @property string|null $contact_phone
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\City|null $city
 * @property-read \App\Models\Country|null $country
 * @property-read \App\Models\OrganizationType|null $organizationType
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read \App\Models\Region|null $region
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereContactSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereOrganizationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Organization whereYoutubeLink($value)
 * @mixin \Eloquent
 */
class Organization extends Model
{
    protected $fillable = [
        'user_id',
        'organization_type_id',
        'title',
        'slug',
        'country_id',
        'region_id',
        'city_id',
        'address',
        'description',
        'youtube_link',
        'contact_name',
        'contact_surname',
        'contact_email',
        'contact_phone',
        'status',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoble');
    }
}
