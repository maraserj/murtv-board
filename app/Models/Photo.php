<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Photo
 *
 * @property int $id
 * @property int $photoble_id
 * @property string $photoble_type
 * @property string $file_name
 * @property string $path
 * @property int $is_main
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $photoble
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereIsMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePhotobleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePhotobleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo main()
 */
class Photo extends Model
{
    protected $fillable = [
        'photoble_id',
        'photoble_type',
        'file_name',
        'path',
        'is_main',
    ];

    public function photoble()
    {
        return $this->morphTo();
    }

    public function scopeMain($query)
    {
        return $query->where('is_main', true);
    }
}
