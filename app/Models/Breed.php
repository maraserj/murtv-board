<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Breed
 *
 * @property int $id
 * @property int|null $breed_type_id
 * @property string $title
 * @property string|null $slug
 * @property int|null $order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\BreedType|null $breedType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereBreedTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Breed ordered()
 */
class Breed extends Model
{
    protected $fillable = [
        'breed_type_id',
        'title',
        'slug',
        'order',
    ];

    public function breedType()
    {
        return $this->belongsTo(BreedType::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function scopeOrdered($query)
    {
        return $query->orderby('order', 'desc')->orderBy('title');
    }
}
