<?php

namespace App\Providers;

use App\Http\ViewComposers\SearchFilterComposer;
use App\Http\ViewComposers\SettingsComposer;
use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer(
            '*', SettingsComposer::class
        );
        View::composer(
            ['partials/top-filter-banner', 'partials/posts-search-filter'], SearchFilterComposer::class
        );
    }

    public function register()
    {
        //
    }
}