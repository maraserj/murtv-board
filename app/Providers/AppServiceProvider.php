<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\Observers\PostObserver;
use App\Services\PostsGrabber\PostsGrabber;
use App\Services\PostsGrabber\PostsGrabberService;
use App\Services\SitemapGenerator\SitemapGenerator;
use App\Services\SitemapGenerator\SitemapGeneratorService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Post::observe(PostObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SitemapGenerator::class, SitemapGeneratorService::class);
        $this->app->bind(PostsGrabber::class, PostsGrabberService::class);
    }
}
