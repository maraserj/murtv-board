User-agent: *
Disallow: /cgi-bin/
Disallow: /countries/regions
Disallow: /regions/cities
Disallow: /cities*
Disallow: /breeds

Host: https://board.mur.tv/
Sitemap: https://board.mur.tv/sitemap.xml