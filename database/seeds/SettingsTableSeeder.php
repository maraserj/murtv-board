<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seoSettings = [
            'index_page_title'       => 'MUR TV',
            'index_page_keywords'    => 'MUR TV',
            'index_page_description' => 'MUR TV',

            'search_page_title'       => 'MUR TV',
            'search_page_keywords'    => 'MUR TV',
            'search_page_description' => 'MUR TV',
        ];

        foreach ($seoSettings as $code => $setting) {
            \App\Models\Setting::create([
                'group_id' => 2,
                'name'     => $code,
                'code'     => $code,
                'value'    => $setting,
                'type'     => 'text',
            ]);
        }
    }
}
