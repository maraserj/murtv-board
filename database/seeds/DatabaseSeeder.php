<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            \App\Models\Post::truncate();
            $posts = factory(\App\Models\Post::class, 15)->create();
            dd($posts[0]);
        } catch (Exception $e) {
            dump($e->getMessage());
        }

//         try {
//             $this->call(SettingsGroupTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(UsersTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(CountriesTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(RegionsTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(CitiesTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(ColorsTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(BreedTypesTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}
//
//         try {
//             $this->call(BreedsTableSeeder::class);
//         } catch (\Exception $e) {dump($e->getMessage());}

    }
}
