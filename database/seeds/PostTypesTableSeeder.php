<?php

use Illuminate\Database\Seeder;

class PostTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'household' => 'Мой домашний',
            'founded' => 'Найденный',
            'need_home' => 'Ищет дом (потерялся)',
        ];

        foreach ($types as $code => $type) {
            \App\Models\PostType::create([
                'title' => $type,
                'slug' => str_slug($type),
                'code' => $code,
            ]);
        }
    }
}
