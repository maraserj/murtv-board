<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
            'Двухцветный',
            'Другой',
            'Коричневый',
            'Кремовый (палевый)',
            'Многоцветный',
            'Рыжий (желтый, красный)',
            'Рябой',
            'Серый (дымчатый)',
            'Черный',
        ];

        foreach ($colors as $color) {
            \App\Models\Color::create(['title' => $color]);
        }
    }
}