<?php

use Illuminate\Database\Seeder;

class BreedTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $breedTypes = [
            'Кошка',
            'Собака',
        ];

        foreach ($breedTypes as $breedType) {
            \App\Models\BreedType::create([
                'title' => $breedType,
                'slug'  => str_slug($breedType),
            ]);
        }
    }
}
