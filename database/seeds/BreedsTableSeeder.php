<?php

use Illuminate\Database\Seeder;

class BreedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $breeds = [
            'cats' => [
                'Мэйн кун',
                'Наполеон',
                'Нибелунг',
            ],
            'dogs' => [
                'Бультерьер',
                'Динго',
                'Доберман',
                'Мастиф',
            ],
        ];

        foreach ($breeds as $key => $breed) {
            foreach ($breed as $value) {
                \App\Models\Breed::create([
                    'breed_type_id' => $key == 'cats' ? 1 : 2,
                    'title'         => $value,
                    'slug'          => str_slug($value),
                ]);
            }
        }
    }
}
