<?php

use Illuminate\Database\Seeder;

class OrganizationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Приют',
            'Ветеринарная клиника',
            'Зоозащитная организация',
            'Волонтерская группа',
            'Кинологический клуб',
        ];

        foreach ($types as $type) {
            \App\Models\OrganizationType::create([
                'title' => $type,
                'slug' => str_slug($type),
            ]);
        }
    }
}
