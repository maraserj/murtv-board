<?php

use Illuminate\Database\Seeder;

class SettingsGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            'main',
            'Seo',
            'Sitemap',
        ];

        foreach ($settings as $setting) {
            \App\Models\SettingGroup::create(['name' => $setting]);
        }
    }
}
