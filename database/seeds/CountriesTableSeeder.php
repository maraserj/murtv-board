<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Country::create([
            'name' => 'Украина',
            'slug' => 'ukraine',
            'code' => 'ua',
        ]);
        \App\Models\Country::create([
            'name' => 'Россия',
            'slug' => 'russia',
            'code' => 'ru',
        ]);
        \App\Models\Country::create([
            'name' => 'Белоруссия',
            'slug' => 'belarus',
            'code' => 'by',
        ]);


    }
}
