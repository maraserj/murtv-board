<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'role'     => 'admin',
            'name'     => 'Admin',
            'email'    => 'admin@murtv.info',
            'password' => bcrypt('307v0nf48aw'),
            'token' => str_random(32),
        ];

        \App\Models\User::create($user);
    }
}
