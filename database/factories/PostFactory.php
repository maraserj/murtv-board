<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Post::class, function (Faker $faker) {
    $color = \App\Models\Color::inRandomOrder()->first();
    $breed = \App\Models\Breed::inRandomOrder()->first();
    $city = \App\Models\City::inRandomOrder()->first();

    $title = $faker->text(70);

    return [
        'user_id'       => 1,
        'name'          => $faker->userName,
        'type'          => 'household',
        'gender'        => 'male',
        'size'          => rand(20, 100),
        'color_id'      => $color->id,
        'breed_type_id' => $breed->breed_type_id,
        'breed_id'      => $breed->id,
        'birthday'      => $faker->date(),
        'animal_id'     => null,
        'sterilized'    => rand(0, 1),
        'guarded'       => rand(0, 1),
        'ill'           => rand(0, 1),
        'deadly'        => rand(0, 1),
        'featured'      => rand(0, 1),
        'country_id'    => $city->region->country_id,
        'region_id'     => $city->region_id,
        'city_id'       => $city->id,
        'address'       => $faker->address,
        'coordinates'   => $faker->latitude . ',' . $faker->longitude,
        'title'         => $title,
        'slug'          => str_slug($title),
        'description'   => $faker->text(1000),
        'status'        => true,
    ];
});
