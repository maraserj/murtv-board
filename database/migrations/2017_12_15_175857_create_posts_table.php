<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name', 60)->nullable();
            $table->string('type', 40)->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('size')->nullable();
            $table->unsignedInteger('color_id')->nullable();
            $table->unsignedInteger('breed_type_id')->nullable();
            $table->unsignedInteger('breed_id')->nullable();
            $table->string('birthday')->nullable();
            $table->string('animal_id')->nullable();
            $table->boolean('sterilized')->default(false);
            $table->boolean('guarded')->default(false);
            $table->boolean('ill')->default(false);
            $table->boolean('deadly')->default(false);
            $table->boolean('featured')->default(false);

            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('address')->nullable();
            $table->string('coordinates')->nullable();

            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_user_id_foreign');
        });
        Schema::dropIfExists('posts');
    }
}
