<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderColumnToBreedsAndBreedTypesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('breed_types', function (Blueprint $table) {
            $table->integer('order')->default(0)->after('image');
        });
        Schema::table('breeds', function (Blueprint $table) {
            $table->integer('order')->default(0)->after('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('breed_types', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('breeds', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
