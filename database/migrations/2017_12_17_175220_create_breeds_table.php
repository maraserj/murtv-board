<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breeds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('breed_type_id')->nullable();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->timestamps();
        });
        Schema::table('breeds', function (Blueprint $table) {
            $table->foreign('breed_type_id')->references('id')->on('breed_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('breeds', function (Blueprint $table) {
            $table->dropForeign('breeds_breed_type_id_foreign');
        });
        Schema::dropIfExists('breeds');
    }
}
