<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVkLocationIdToLocationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->unsignedInteger('vk_country_id')->nullable()->after('id');
        });
        Schema::table('regions', function (Blueprint $table) {
            $table->unsignedInteger('vk_region_id')->nullable()->after('id');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->unsignedInteger('vk_city_id')->nullable()->after('id');
            $table->string('area')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('vk_country_id');
        });
        Schema::table('regions', function (Blueprint $table) {
            $table->dropColumn('vk_region_id');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('vk_city_id');
            $table->dropColumn('area');
        });
    }
}
