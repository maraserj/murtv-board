<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedInteger('post_type_id')->nullable()->after('type');
            $table->boolean('has_home')->default(false)->after('status');
            $table->string('price')->nullable()->after('status');
            $table->string('sale_type')->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('post_type_id');
            $table->dropColumn('has_home');
            $table->dropColumn('price');
            $table->dropColumn('sale_type');
        });
    }
}
