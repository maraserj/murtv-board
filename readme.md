## About Laravel
![Laravel logo](https://laravel.com/assets/img/components/logo-laravel.svg)
###
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## [Docs](https://laravel.com/docs)

### Install

1. Clone git repository
2. Run `composer install` or `php composer.phar install`
3. Create your own `.env` file with command: `cp .env.example .env`
4. Run `npm install`
5. Run `php artisan key:generate `, for generation session key
6. Run `php artisan storage:link`, for saving public files in storage folder
7. In `.env` file change DB settings to your local.
8. Run `php artisan migrate --seed` for creating DB schemas
9. Optionally. For PHPStorm autocomplete run `php artisan ide-helper:generate`
